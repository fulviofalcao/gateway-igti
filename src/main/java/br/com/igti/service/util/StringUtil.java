package br.com.igti.service.util;

import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    public static String objectAttributes(Object obj) throws IllegalAccessException {
        String result = "";
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            Object value = field.get(obj);
            result += name + ": " + value + ", ";
        }
        return result.substring(0, result.length()-2);
    }

    public static boolean hasCharsOrNumbersInString(String field) {

        String regex = "[A-Z0-9]*";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(field);
        if(matcher.matches()){
            return true;
        }
        return false;
    }

    public static boolean hasOnlyNumbersInString(String field) {

        String regex = "^\\d+$";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(field);
        if(matcher.matches()){
            return true;
        }
        return false;
    }

    public static boolean hasOnlyCharsInString(String field) {
        String regex = "[A-Z\\s]+";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(field);
        if(matcher.matches()){
            return true;
        }
        return false;
    }

    public static boolean hasCharsAndNumbersInString(String field) {

        String regex = "[A-Z0-9\\s]+";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(field);
        if(matcher.matches()){
            return true;
        }
        return false;
    }
}
