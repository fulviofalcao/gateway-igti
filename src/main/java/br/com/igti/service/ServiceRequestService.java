package br.com.igti.service;

import br.com.igti.domain.ServiceRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ServiceRequest.
 */
public interface ServiceRequestService {

    /**
     * Save a serviceRequest.
     *
     * @param serviceRequest the entity to save
     * @return the persisted entity
     */
    ServiceRequest save(ServiceRequest serviceRequest);

    /**
     *  Get all the serviceRequests.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ServiceRequest> findAll(Pageable pageable);

    /**
     *  Get the "id" serviceRequest.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ServiceRequest findOne(Long id);

    /**
     *  Delete the "id" serviceRequest.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
