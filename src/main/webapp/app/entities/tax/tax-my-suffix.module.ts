import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    TaxMySuffixService,
    TaxMySuffixPopupService,
    TaxMySuffixComponent,
    TaxMySuffixDetailComponent,
    TaxMySuffixDialogComponent,
    TaxMySuffixPopupComponent,
    TaxMySuffixDeletePopupComponent,
    TaxMySuffixDeleteDialogComponent,
    taxRoute,
    taxPopupRoute,
    TaxMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...taxRoute,
    ...taxPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TaxMySuffixComponent,
        TaxMySuffixDetailComponent,
        TaxMySuffixDialogComponent,
        TaxMySuffixDeleteDialogComponent,
        TaxMySuffixPopupComponent,
        TaxMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        TaxMySuffixComponent,
        TaxMySuffixDialogComponent,
        TaxMySuffixPopupComponent,
        TaxMySuffixDeleteDialogComponent,
        TaxMySuffixDeletePopupComponent,
    ],
    providers: [
        TaxMySuffixService,
        TaxMySuffixPopupService,
        TaxMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayTaxMySuffixModule {}
