import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GatewayPayReferenceMySuffixModule } from './pay-reference/pay-reference-my-suffix.module';
import { GatewayPaymentOriginMySuffixModule } from './payment-origin/payment-origin-my-suffix.module';
import { GatewayCurrencyMySuffixModule } from './currency/currency-my-suffix.module';
import { GatewayMessageMySuffixModule } from './message/message-my-suffix.module';
import { GatewaySituationMySuffixModule } from './situation/situation-my-suffix.module';
import { GatewayBankMySuffixModule } from './bank/bank-my-suffix.module';
import { GatewayServiceRequestMySuffixModule } from './service-request/service-request-my-suffix.module';
import { GatewayServiceResponseMySuffixModule } from './service-response/service-response-my-suffix.module';
import { GatewayTaxMySuffixModule } from './tax/tax-my-suffix.module';
import { GatewayLogServiceMySuffixModule } from './log-service/log-service-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        GatewayPayReferenceMySuffixModule,
        GatewayPaymentOriginMySuffixModule,
        GatewayCurrencyMySuffixModule,
        GatewayMessageMySuffixModule,
        GatewaySituationMySuffixModule,
        GatewayBankMySuffixModule,
        GatewayServiceRequestMySuffixModule,
        GatewayServiceResponseMySuffixModule,
        GatewayTaxMySuffixModule,
        GatewayLogServiceMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayEntityModule {}
