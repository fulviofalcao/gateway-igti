package br.com.igti.web.rest;

import br.com.igti.GatewayApp;

import br.com.igti.domain.LogService;
import br.com.igti.repository.LogServiceRepository;
import br.com.igti.service.LogServiceService;
import br.com.igti.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import static br.com.igti.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LogServiceResource REST controller.
 *
 * @see LogServiceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class LogServiceResourceIntTest {

    private static final String DEFAULT_IMPUT = "AAAAAAAAAA";
    private static final String UPDATED_IMPUT = "BBBBBBBBBB";

    private static final String DEFAULT_OUTPUT = "AAAAA";
    private static final String UPDATED_OUTPUT = "BBBBB";

    private static final LocalDateTime DEFAULT_LOG_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_LOG_DATE = LocalDateTime.now(ZoneId.systemDefault());

    @Autowired
    private LogServiceRepository logServiceRepository;

    @Autowired
    private LogServiceService logServiceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLogServiceMockMvc;

    private LogService logService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LogServiceResource logServiceResource = new LogServiceResource(logServiceService);
        this.restLogServiceMockMvc = MockMvcBuilders.standaloneSetup(logServiceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LogService createEntity(EntityManager em) {
        LogService logService = new LogService()
            .imput(DEFAULT_IMPUT)
            .output(DEFAULT_OUTPUT)
            .logDate(DEFAULT_LOG_DATE);
        return logService;
    }

    @Before
    public void initTest() {
        logService = createEntity(em);
    }

    @Test
    @Transactional
    public void createLogService() throws Exception {
        int databaseSizeBeforeCreate = logServiceRepository.findAll().size();

        // Create the LogService
        restLogServiceMockMvc.perform(post("/api/log-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(logService)))
            .andExpect(status().isCreated());

        // Validate the LogService in the database
        List<LogService> logServiceList = logServiceRepository.findAll();
        assertThat(logServiceList).hasSize(databaseSizeBeforeCreate + 1);
        LogService testLogService = logServiceList.get(logServiceList.size() - 1);
        assertThat(testLogService.getImput()).isEqualTo(DEFAULT_IMPUT);
        assertThat(testLogService.getOutput()).isEqualTo(DEFAULT_OUTPUT);
        assertThat(testLogService.getLogDate()).isEqualTo(DEFAULT_LOG_DATE);
    }

    @Test
    @Transactional
    public void createLogServiceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = logServiceRepository.findAll().size();

        // Create the LogService with an existing ID
        logService.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLogServiceMockMvc.perform(post("/api/log-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(logService)))
            .andExpect(status().isBadRequest());

        // Validate the LogService in the database
        List<LogService> logServiceList = logServiceRepository.findAll();
        assertThat(logServiceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkImputIsRequired() throws Exception {
        int databaseSizeBeforeTest = logServiceRepository.findAll().size();
        // set the field null
        logService.setImput(null);

        // Create the LogService, which fails.

        restLogServiceMockMvc.perform(post("/api/log-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(logService)))
            .andExpect(status().isBadRequest());

        List<LogService> logServiceList = logServiceRepository.findAll();
        assertThat(logServiceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLogDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = logServiceRepository.findAll().size();
        // set the field null
        logService.setLogDate(null);

        // Create the LogService, which fails.

        restLogServiceMockMvc.perform(post("/api/log-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(logService)))
            .andExpect(status().isBadRequest());

        List<LogService> logServiceList = logServiceRepository.findAll();
        assertThat(logServiceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLogServices() throws Exception {
        // Initialize the database
        logServiceRepository.saveAndFlush(logService);

        // Get all the logServiceList
        restLogServiceMockMvc.perform(get("/api/log-services?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(logService.getId().intValue())))
            .andExpect(jsonPath("$.[*].imput").value(hasItem(DEFAULT_IMPUT.toString())))
            .andExpect(jsonPath("$.[*].output").value(hasItem(DEFAULT_OUTPUT.toString())))
            .andExpect(jsonPath("$.[*].logDate").value(hasItem(DEFAULT_LOG_DATE.toString())));
    }

    @Test
    @Transactional
    public void getLogService() throws Exception {
        // Initialize the database
        logServiceRepository.saveAndFlush(logService);

        // Get the logService
        restLogServiceMockMvc.perform(get("/api/log-services/{id}", logService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(logService.getId().intValue()))
            .andExpect(jsonPath("$.imput").value(DEFAULT_IMPUT.toString()))
            .andExpect(jsonPath("$.output").value(DEFAULT_OUTPUT.toString()))
            .andExpect(jsonPath("$.logDate").value(DEFAULT_LOG_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLogService() throws Exception {
        // Get the logService
        restLogServiceMockMvc.perform(get("/api/log-services/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLogService() throws Exception {
        // Initialize the database
        logServiceService.save(logService);

        int databaseSizeBeforeUpdate = logServiceRepository.findAll().size();

        // Update the logService
        LogService updatedLogService = logServiceRepository.findOne(logService.getId());
        updatedLogService
            .imput(UPDATED_IMPUT)
            .output(UPDATED_OUTPUT)
            .logDate(UPDATED_LOG_DATE);

        restLogServiceMockMvc.perform(put("/api/log-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLogService)))
            .andExpect(status().isOk());

        // Validate the LogService in the database
        List<LogService> logServiceList = logServiceRepository.findAll();
        assertThat(logServiceList).hasSize(databaseSizeBeforeUpdate);
        LogService testLogService = logServiceList.get(logServiceList.size() - 1);
        assertThat(testLogService.getImput()).isEqualTo(UPDATED_IMPUT);
        assertThat(testLogService.getOutput()).isEqualTo(UPDATED_OUTPUT);
        assertThat(testLogService.getLogDate()).isEqualTo(UPDATED_LOG_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingLogService() throws Exception {
        int databaseSizeBeforeUpdate = logServiceRepository.findAll().size();

        // Create the LogService

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLogServiceMockMvc.perform(put("/api/log-services")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(logService)))
            .andExpect(status().isCreated());

        // Validate the LogService in the database
        List<LogService> logServiceList = logServiceRepository.findAll();
        assertThat(logServiceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLogService() throws Exception {
        // Initialize the database
        logServiceService.save(logService);

        int databaseSizeBeforeDelete = logServiceRepository.findAll().size();

        // Get the logService
        restLogServiceMockMvc.perform(delete("/api/log-services/{id}", logService.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LogService> logServiceList = logServiceRepository.findAll();
        assertThat(logServiceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LogService.class);
        LogService logService1 = new LogService();
        logService1.setId(1L);
        LogService logService2 = new LogService();
        logService2.setId(logService1.getId());
        assertThat(logService1).isEqualTo(logService2);
        logService2.setId(2L);
        assertThat(logService1).isNotEqualTo(logService2);
        logService1.setId(null);
        assertThat(logService1).isNotEqualTo(logService2);
    }
}
