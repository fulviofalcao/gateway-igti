import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SituationMySuffix } from './situation-my-suffix.model';
import { SituationMySuffixPopupService } from './situation-my-suffix-popup.service';
import { SituationMySuffixService } from './situation-my-suffix.service';

@Component({
    selector: 'jhi-situation-my-suffix-dialog',
    templateUrl: './situation-my-suffix-dialog.component.html'
})
export class SituationMySuffixDialogComponent implements OnInit {

    situation: SituationMySuffix;
    isSaving: boolean;
    creationDateDp: any;
    updateDateDp: any;
    deleteDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private situationService: SituationMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.situation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.situationService.update(this.situation));
        } else {
            this.subscribeToSaveResponse(
                this.situationService.create(this.situation));
        }
    }

    private subscribeToSaveResponse(result: Observable<SituationMySuffix>) {
        result.subscribe((res: SituationMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SituationMySuffix) {
        this.eventManager.broadcast({ name: 'situationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-situation-my-suffix-popup',
    template: ''
})
export class SituationMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private situationPopupService: SituationMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.situationPopupService
                    .open(SituationMySuffixDialogComponent as Component, params['id']);
            } else {
                this.situationPopupService
                    .open(SituationMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
