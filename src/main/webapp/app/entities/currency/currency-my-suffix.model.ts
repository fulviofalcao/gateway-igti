import { BaseEntity } from './../../shared';

export class CurrencyMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public acronymsCurrency?: string,
        public creationDate?: any,
        public updateDate?: any,
        public deleteDate?: any,
        public active?: number,
    ) {
    }
}
