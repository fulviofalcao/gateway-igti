package br.com.igti.web.rest;

import br.com.igti.domain.ServiceResponse;
import br.com.igti.service.LogServiceService;
import br.com.igti.service.constants.ServiceRequestConstants;
import br.com.igti.service.constants.ServiceResponseConstants;
import br.com.igti.service.util.Assert;
import br.com.igti.service.vo.CancelPaymentReference;
import br.com.igti.service.vo.CreateReference;
import br.com.igti.service.vo.PaymentReference;
import br.com.igti.service.vo.UpdatePayReference;
import com.codahale.metrics.annotation.Timed;
import br.com.igti.domain.PayReference;
import br.com.igti.service.PayReferenceService;
import br.com.igti.web.rest.errors.BadRequestAlertException;
import br.com.igti.web.rest.util.HeaderUtil;
import br.com.igti.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import static br.com.igti.domain.Tax_.payReference;

/**
 * REST controller for managing PayReference.
 */
@RestController
@RequestMapping("/api")
public class PayReferenceResource {

    private final Logger log = LoggerFactory.getLogger(PayReferenceResource.class);

    private static final String ENTITY_NAME = "payReference";

    private final PayReferenceService payReferenceService;
    private final LogServiceService logServiceService;

    public PayReferenceResource(PayReferenceService payReferenceService, LogServiceService logServiceService) {
        this.payReferenceService = payReferenceService;
        this.logServiceService = logServiceService;
    }


    /**
     * POST  /pay-references : Create a new payReference.
     *
     * @param payReference the payReference to create
     * @return the ResponseEntity with status 201 (Created) and with body the new payReference, or with status 400 (Bad Request) if the payReference has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pay-references")
    @Timed
    public ResponseEntity<PayReference> createPayReference(@Valid @RequestBody PayReference payReference) throws URISyntaxException {
        log.debug("REST request to save PayReference : {}", payReference);
        if (payReference.getId() != null) {
            throw new BadRequestAlertException("A new payReference cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PayReference result = payReferenceService.save(payReference);
        return ResponseEntity.created(new URI("/api/pay-references/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pay-references : Updates an existing payReference.
     *
     * @param payReference the payReference to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated payReference,
     * or with status 400 (Bad Request) if the payReference is not valid,
     * or with status 500 (Internal Server Error) if the payReference couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pay-references")
    @Timed
    public ResponseEntity<PayReference> updatePayReference(@Valid @RequestBody PayReference payReference) throws URISyntaxException {
        log.debug("REST request to update PayReference : {}", payReference);
        if (payReference.getId() == null) {
            return createPayReference(payReference);
        }
        PayReference result = payReferenceService.save(payReference);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, payReference.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pay-references : get all the payReferences.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of payReferences in body
     */
    @GetMapping("/pay-references")
    @Timed
    public ResponseEntity<List<PayReference>> getAllPayReferences(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PayReferences");
        Page<PayReference> page = payReferenceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pay-references");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pay-references/:id : get the "id" payReference.
     *
     * @param id the id of the payReference to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the payReference, or with status 404 (Not Found)
     */
    @GetMapping("/pay-references/{id}")
    @Timed
    public ResponseEntity<PayReference> getPayReference(@PathVariable Long id) {
        log.debug("REST request to get PayReference : {}", id);
        PayReference payReference = payReferenceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(payReference));
    }

    /**
     * DELETE  /pay-references/:id : delete the "id" payReference.
     *
     * @param id the id of the payReference to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pay-references/{id}")
    @Timed
    public ResponseEntity<Void> deletePayReference(@PathVariable Long id) {
        log.debug("REST request to delete PayReference : {}", id);
        payReferenceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST  /pay-references/payment : Create a new payReference.
     *
     * @param paymentReference the payReference to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentReference, or with status 400 (Bad Request) if the paymentReference has has an error.
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pay-references/payment")
    @Timed
    public ResponseEntity<ServiceResponse> paymentPayReference(@Valid @RequestBody PaymentReference paymentReference) throws URISyntaxException {
        log.debug("REST request to payment PayReference : {}", paymentReference);

            if (Assert.isNullOrEmpty(paymentReference.getReferenceNumber())) {
                logServiceService.saveLog(paymentReference, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.INVALID_REFERENCE);
                throw new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists");
            }
            PayReference payReference = payReferenceService.findOnePayReference(paymentReference.getReferenceNumber());

            payReferenceService.paymentReferenceValidations(payReference, paymentReference);
            payReferenceService.persistConfirmationPayment(payReference, paymentReference);

            logServiceService.saveLog(paymentReference, "statusCode: 201 -» paymentDone", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.PAID);

        return ResponseEntity.created(new URI("/api/pay-references/payment/" + paymentReference.getReferenceNumber()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, paymentReference.getReferenceNumber()))
            .body(ServiceResponseConstants.PAID);
    }

    /**
     * POST  /pay-references/payment-cancel : Create a new payReference.
     *
     * @param cancelPaymentReference the payReference to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentReference, or with status 400 (Bad Request) if the paymentCancelReference has has an error.
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pay-references/payment-cancel")
    @Timed
    public ResponseEntity<ServiceResponse> paymentCancelPayReference(@Valid @RequestBody CancelPaymentReference cancelPaymentReference) throws URISyntaxException {
        log.debug("REST request to cancel payment PayReference : {}", cancelPaymentReference);
        if (Assert.isNullOrEmpty(cancelPaymentReference.getReferenceNumber())) {
            logServiceService.saveLog(cancelPaymentReference, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.PAYMENT_CANCELLATION, ServiceResponseConstants.INVALID_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists");
        }
        PayReference payReference = payReferenceService.findOnePayReference(cancelPaymentReference.getReferenceNumber());

        payReferenceService.paymentCancelReferenceValidations(payReference, cancelPaymentReference);
        payReferenceService.persistCancelPayment(payReference, cancelPaymentReference);

        logServiceService.saveLog(cancelPaymentReference, "statusCode: 201 -» paymentCancel", ServiceRequestConstants.PAYMENT_CANCELLATION, ServiceResponseConstants.CANCELLED);

        return ResponseEntity.created(new URI("/api/pay-references/payment-cancel/" + cancelPaymentReference.getReferenceNumber()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, cancelPaymentReference.getReferenceNumber()))
            .body(ServiceResponseConstants.CANCELLED);
    }

    /**
     * PUT  /pay-references/update : Updates an existing payReference.
     *
     * @param updatePayReference the payReference to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated payReference,
     * or with status 400 (Bad Request) if the payReference is not valid,
     * or with status 500 (Internal Server Error) if the payReference couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pay-references/update")
    @Timed
    public ResponseEntity<?> updatePayReferenceExist(@Valid @RequestBody UpdatePayReference updatePayReference) throws URISyntaxException {
        log.debug("REST request to update PayReference : {}", payReference);
        if (Assert.isNullOrEmpty(updatePayReference.getReferenceNumber())) {
            logServiceService.saveLog(payReference, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.REFERENCE_UPDATE, ServiceResponseConstants.INVALID_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists");
        }
        PayReference payReference = payReferenceService.findOnePayReference(updatePayReference.getReferenceNumber());

        payReferenceService.paymentUpdateReferenceValidations(payReference, updatePayReference);
        payReferenceService.persistUpdatePayReference(payReference, updatePayReference);

        logServiceService.saveLog(updatePayReference, "statusCode: 201 -» updateReference", ServiceRequestConstants.REFERENCE_UPDATE, ServiceResponseConstants.UPDATE_REFERENCE);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, updatePayReference.getReferenceNumber()))
            .body(ServiceResponseConstants.UPDATE_REFERENCE);
    }

    /**
     * POST  /pay-references/create : Create a new payReference.
     *
     * @param createReference the payReference to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentReference, or with status 400 (Bad Request) if the paymentReference has has an error.
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pay-references/create")
    @Timed
    public ResponseEntity<ServiceResponse> createPayReference(@Valid @RequestBody CreateReference createReference) throws URISyntaxException {
        log.debug("REST request to create PayReference : {}", createReference);

        if (Assert.isNullOrEmpty(createReference.getReferenceNumber())) {
            logServiceService.saveLog(createReference, "statusCode: 400 -» referenceNumberInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.INVALID_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberInvalid");
        }

        payReferenceService.createReferenceValidations(payReferenceService.findOnePayReference(createReference.getReferenceNumber()), createReference);
        PayReference responsePayReference = payReferenceService.persistCreateReferencePayment(createReference);

        logServiceService.saveLog(responsePayReference, "statusCode: 201 -» createReferenceDone", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_REFERENCE);

        return ResponseEntity.created(new URI("/api/pay-references/create/" + responsePayReference.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, responsePayReference.getId().toString()))
            .body(ServiceResponseConstants.CREATE_REFERENCE);
    }

    /**
     * PUT  /pay-references/delete : Delete an existing payReference.
     *
     * @param payReference the payReference to delete
     * @return the ResponseEntity with status 200 (OK) and with body the updated payReference,
     * or with status 400 (Bad Request) if the payReference is not valid,
     * or with status 500 (Internal Server Error) if the payReference couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pay-references/delete")
    @Timed
    public ResponseEntity<?> updatePayReferenceExist(@Valid @RequestBody String payReference) throws URISyntaxException {
        log.debug("REST request to update PayReference : {}", payReference);
        if (Assert.isNullOrEmpty(payReference)) {
            logServiceService.saveLog(payReference, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.REFERENCE_DELETE, ServiceResponseConstants.INVALID_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists");
        }
        PayReference resultPayReference = payReferenceService.findOnePayReference(payReference);

        payReferenceService.paymentDeleteReferenceValidations(resultPayReference, payReference);
        payReferenceService.persistDeletePayReference(resultPayReference);

        logServiceService.saveLog(resultPayReference, "statusCode: 201 -» updateReference", ServiceRequestConstants.REFERENCE_DELETE, ServiceResponseConstants.REFERENCE_DELETE);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resultPayReference.getReferenceNumber()))
            .body(ServiceResponseConstants.REFERENCE_DELETE);
    }
}
