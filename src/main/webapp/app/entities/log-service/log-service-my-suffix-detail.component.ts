import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { LogServiceMySuffix } from './log-service-my-suffix.model';
import { LogServiceMySuffixService } from './log-service-my-suffix.service';

@Component({
    selector: 'jhi-log-service-my-suffix-detail',
    templateUrl: './log-service-my-suffix-detail.component.html'
})
export class LogServiceMySuffixDetailComponent implements OnInit, OnDestroy {

    logService: LogServiceMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private logServiceService: LogServiceMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLogServices();
    }

    load(id) {
        this.logServiceService.find(id).subscribe((logService) => {
            this.logService = logService;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLogServices() {
        this.eventSubscriber = this.eventManager.subscribe(
            'logServiceListModification',
            (response) => this.load(this.logService.id)
        );
    }
}
