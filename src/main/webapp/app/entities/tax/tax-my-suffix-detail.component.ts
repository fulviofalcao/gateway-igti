import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TaxMySuffix } from './tax-my-suffix.model';
import { TaxMySuffixService } from './tax-my-suffix.service';

@Component({
    selector: 'jhi-tax-my-suffix-detail',
    templateUrl: './tax-my-suffix-detail.component.html'
})
export class TaxMySuffixDetailComponent implements OnInit, OnDestroy {

    tax: TaxMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private taxService: TaxMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTaxes();
    }

    load(id) {
        this.taxService.find(id).subscribe((tax) => {
            this.tax = tax;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTaxes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'taxListModification',
            (response) => this.load(this.tax.id)
        );
    }
}
