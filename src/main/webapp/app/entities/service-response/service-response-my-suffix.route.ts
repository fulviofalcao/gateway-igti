import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ServiceResponseMySuffixComponent } from './service-response-my-suffix.component';
import { ServiceResponseMySuffixDetailComponent } from './service-response-my-suffix-detail.component';
import { ServiceResponseMySuffixPopupComponent } from './service-response-my-suffix-dialog.component';
import { ServiceResponseMySuffixDeletePopupComponent } from './service-response-my-suffix-delete-dialog.component';

@Injectable()
export class ServiceResponseMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const serviceResponseRoute: Routes = [
    {
        path: 'service-response-my-suffix',
        component: ServiceResponseMySuffixComponent,
        resolve: {
            'pagingParams': ServiceResponseMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceResponse.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'service-response-my-suffix/:id',
        component: ServiceResponseMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceResponse.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const serviceResponsePopupRoute: Routes = [
    {
        path: 'service-response-my-suffix-new',
        component: ServiceResponseMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceResponse.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-response-my-suffix/:id/edit',
        component: ServiceResponseMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceResponse.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-response-my-suffix/:id/delete',
        component: ServiceResponseMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceResponse.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
