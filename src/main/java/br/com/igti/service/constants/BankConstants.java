package br.com.igti.service.constants;

import br.com.igti.domain.Bank;
import br.com.igti.repository.BankRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class BankConstants {
    public static Bank BAI;
    public static Bank BIC;

    @Bean
    @DependsOn("liquibase")
    public BankConstants bankConstantsInitializer(BankRepository bankRepository) {
        BAI = bankRepository.findOne(1L);
        BIC = bankRepository.findOne(2L);
        return new BankConstants();
    }
}
