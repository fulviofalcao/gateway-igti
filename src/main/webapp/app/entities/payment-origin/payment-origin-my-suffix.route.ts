import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PaymentOriginMySuffixComponent } from './payment-origin-my-suffix.component';
import { PaymentOriginMySuffixDetailComponent } from './payment-origin-my-suffix-detail.component';
import { PaymentOriginMySuffixPopupComponent } from './payment-origin-my-suffix-dialog.component';
import { PaymentOriginMySuffixDeletePopupComponent } from './payment-origin-my-suffix-delete-dialog.component';

@Injectable()
export class PaymentOriginMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const paymentOriginRoute: Routes = [
    {
        path: 'payment-origin-my-suffix',
        component: PaymentOriginMySuffixComponent,
        resolve: {
            'pagingParams': PaymentOriginMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.paymentOrigin.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-origin-my-suffix/:id',
        component: PaymentOriginMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.paymentOrigin.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentOriginPopupRoute: Routes = [
    {
        path: 'payment-origin-my-suffix-new',
        component: PaymentOriginMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.paymentOrigin.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-origin-my-suffix/:id/edit',
        component: PaymentOriginMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.paymentOrigin.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-origin-my-suffix/:id/delete',
        component: PaymentOriginMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.paymentOrigin.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
