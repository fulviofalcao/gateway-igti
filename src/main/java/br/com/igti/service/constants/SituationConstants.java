package br.com.igti.service.constants;

import br.com.igti.domain.Situation;
import br.com.igti.repository.SituationRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class SituationConstants {

    public static Situation GENERATED;
    public static Situation PAID;
    public static Situation CANCELED;
    public static Situation CONCILIATED;
    public static Situation DELETED;


    @Bean
    @DependsOn("liquibase")
    public SituationConstants SituationConstantsInitializer(SituationRepository situationRepository) {
        GENERATED = situationRepository.findOne(1L);
        PAID = situationRepository.findOne(2L);
        CANCELED = situationRepository.findOne(3L);
        CONCILIATED = situationRepository.findOne(7L);
        DELETED = situationRepository.findOne(20L);

        return new SituationConstants();
    }
}
