import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PaymentOriginMySuffix } from './payment-origin-my-suffix.model';
import { PaymentOriginMySuffixService } from './payment-origin-my-suffix.service';

@Injectable()
export class PaymentOriginMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private paymentOriginService: PaymentOriginMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.paymentOriginService.find(id).subscribe((paymentOrigin) => {
                    if (paymentOrigin.creationDate) {
                        paymentOrigin.creationDate = {
                            year: paymentOrigin.creationDate.getFullYear(),
                            month: paymentOrigin.creationDate.getMonth() + 1,
                            day: paymentOrigin.creationDate.getDate()
                        };
                    }
                    if (paymentOrigin.updateDate) {
                        paymentOrigin.updateDate = {
                            year: paymentOrigin.updateDate.getFullYear(),
                            month: paymentOrigin.updateDate.getMonth() + 1,
                            day: paymentOrigin.updateDate.getDate()
                        };
                    }
                    if (paymentOrigin.deleteDate) {
                        paymentOrigin.deleteDate = {
                            year: paymentOrigin.deleteDate.getFullYear(),
                            month: paymentOrigin.deleteDate.getMonth() + 1,
                            day: paymentOrigin.deleteDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.paymentOriginModalRef(component, paymentOrigin);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.paymentOriginModalRef(component, new PaymentOriginMySuffix());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    paymentOriginModalRef(component: Component, paymentOrigin: PaymentOriginMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.paymentOrigin = paymentOrigin;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
