package br.com.igti.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.igti.domain.LogService;
import br.com.igti.service.LogServiceService;
import br.com.igti.web.rest.errors.BadRequestAlertException;
import br.com.igti.web.rest.util.HeaderUtil;
import br.com.igti.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LogService.
 */
@RestController
@RequestMapping("/api")
public class LogServiceResource {

    private final Logger log = LoggerFactory.getLogger(LogServiceResource.class);

    private static final String ENTITY_NAME = "logService";

    private final LogServiceService logServiceService;

    public LogServiceResource(LogServiceService logServiceService) {
        this.logServiceService = logServiceService;
    }

    /**
     * POST  /log-services : Create a new logService.
     *
     * @param logService the logService to create
     * @return the ResponseEntity with status 201 (Created) and with body the new logService, or with status 400 (Bad Request) if the logService has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/log-services")
    @Timed
    public ResponseEntity<LogService> createLogService(@Valid @RequestBody LogService logService) throws URISyntaxException {
        log.debug("REST request to save LogService : {}", logService);
        if (logService.getId() != null) {
            throw new BadRequestAlertException("A new logService cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LogService result = logServiceService.save(logService);
        return ResponseEntity.created(new URI("/api/log-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /log-services : Updates an existing logService.
     *
     * @param logService the logService to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated logService,
     * or with status 400 (Bad Request) if the logService is not valid,
     * or with status 500 (Internal Server Error) if the logService couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/log-services")
    @Timed
    public ResponseEntity<LogService> updateLogService(@Valid @RequestBody LogService logService) throws URISyntaxException {
        log.debug("REST request to update LogService : {}", logService);
        if (logService.getId() == null) {
            return createLogService(logService);
        }
        LogService result = logServiceService.save(logService);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, logService.getId().toString()))
            .body(result);
    }

    /**
     * GET  /log-services : get all the logServices.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of logServices in body
     */
    @GetMapping("/log-services")
    @Timed
    public ResponseEntity<List<LogService>> getAllLogServices(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of LogServices");
        Page<LogService> page = logServiceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/log-services");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /log-services/:id : get the "id" logService.
     *
     * @param id the id of the logService to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the logService, or with status 404 (Not Found)
     */
    @GetMapping("/log-services/{id}")
    @Timed
    public ResponseEntity<LogService> getLogService(@PathVariable Long id) {
        log.debug("REST request to get LogService : {}", id);
        LogService logService = logServiceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(logService));
    }

    /**
     * DELETE  /log-services/:id : delete the "id" logService.
     *
     * @param id the id of the logService to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/log-services/{id}")
    @Timed
    public ResponseEntity<Void> deleteLogService(@PathVariable Long id) {
        log.debug("REST request to delete LogService : {}", id);
        logServiceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
