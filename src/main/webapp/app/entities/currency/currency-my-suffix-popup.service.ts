import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyMySuffix } from './currency-my-suffix.model';
import { CurrencyMySuffixService } from './currency-my-suffix.service';

@Injectable()
export class CurrencyMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private currencyService: CurrencyMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.currencyService.find(id).subscribe((currency) => {
                    if (currency.creationDate) {
                        currency.creationDate = {
                            year: currency.creationDate.getFullYear(),
                            month: currency.creationDate.getMonth() + 1,
                            day: currency.creationDate.getDate()
                        };
                    }
                    if (currency.updateDate) {
                        currency.updateDate = {
                            year: currency.updateDate.getFullYear(),
                            month: currency.updateDate.getMonth() + 1,
                            day: currency.updateDate.getDate()
                        };
                    }
                    if (currency.deleteDate) {
                        currency.deleteDate = {
                            year: currency.deleteDate.getFullYear(),
                            month: currency.deleteDate.getMonth() + 1,
                            day: currency.deleteDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.currencyModalRef(component, currency);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.currencyModalRef(component, new CurrencyMySuffix());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    currencyModalRef(component: Component, currency: CurrencyMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.currency = currency;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
