import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ServiceRequestMySuffix } from './service-request-my-suffix.model';
import { ServiceRequestMySuffixPopupService } from './service-request-my-suffix-popup.service';
import { ServiceRequestMySuffixService } from './service-request-my-suffix.service';

@Component({
    selector: 'jhi-service-request-my-suffix-delete-dialog',
    templateUrl: './service-request-my-suffix-delete-dialog.component.html'
})
export class ServiceRequestMySuffixDeleteDialogComponent {

    serviceRequest: ServiceRequestMySuffix;

    constructor(
        private serviceRequestService: ServiceRequestMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.serviceRequestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'serviceRequestListModification',
                content: 'Deleted an serviceRequest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-service-request-my-suffix-delete-popup',
    template: ''
})
export class ServiceRequestMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private serviceRequestPopupService: ServiceRequestMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.serviceRequestPopupService
                .open(ServiceRequestMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
