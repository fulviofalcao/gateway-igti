package br.com.igti.service.impl;

import br.com.igti.service.ServiceRequestService;
import br.com.igti.domain.ServiceRequest;
import br.com.igti.repository.ServiceRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ServiceRequest.
 */
@Service
@Transactional
public class ServiceRequestServiceImpl implements ServiceRequestService{

    private final Logger log = LoggerFactory.getLogger(ServiceRequestServiceImpl.class);

    private final ServiceRequestRepository serviceRequestRepository;

    public ServiceRequestServiceImpl(ServiceRequestRepository serviceRequestRepository) {
        this.serviceRequestRepository = serviceRequestRepository;
    }

    /**
     * Save a serviceRequest.
     *
     * @param serviceRequest the entity to save
     * @return the persisted entity
     */
    @Override
    public ServiceRequest save(ServiceRequest serviceRequest) {
        log.debug("Request to save ServiceRequest : {}", serviceRequest);
        return serviceRequestRepository.save(serviceRequest);
    }

    /**
     *  Get all the serviceRequests.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ServiceRequest> findAll(Pageable pageable) {
        log.debug("Request to get all ServiceRequests");
        return serviceRequestRepository.findAll(pageable);
    }

    /**
     *  Get one serviceRequest by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ServiceRequest findOne(Long id) {
        log.debug("Request to get ServiceRequest : {}", id);
        return serviceRequestRepository.findOne(id);
    }

    /**
     *  Delete the  serviceRequest by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ServiceRequest : {}", id);
        serviceRequestRepository.delete(id);
    }
}
