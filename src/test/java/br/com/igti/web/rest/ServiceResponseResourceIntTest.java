package br.com.igti.web.rest;

import br.com.igti.GatewayApp;

import br.com.igti.domain.ServiceResponse;
import br.com.igti.repository.ServiceResponseRepository;
import br.com.igti.service.ServiceResponseService;
import br.com.igti.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static br.com.igti.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ServiceResponseResource REST controller.
 *
 * @see ServiceResponseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class ServiceResponseResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_RESPONSE_CODE = "AAAAA";
    private static final String UPDATED_RESPONSE_CODE = "BBBBB";

    private static final String DEFAULT_RETURN_CODE = "AAAAA";
    private static final String UPDATED_RETURN_CODE = "BBBBB";

    @Autowired
    private ServiceResponseRepository serviceResponseRepository;

    @Autowired
    private ServiceResponseService serviceResponseService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restServiceResponseMockMvc;

    private ServiceResponse serviceResponse;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ServiceResponseResource serviceResponseResource = new ServiceResponseResource(serviceResponseService);
        this.restServiceResponseMockMvc = MockMvcBuilders.standaloneSetup(serviceResponseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceResponse createEntity(EntityManager em) {
        ServiceResponse serviceResponse = new ServiceResponse()
            .description(DEFAULT_DESCRIPTION)
            .responseCode(DEFAULT_RESPONSE_CODE)
            .returnCode(DEFAULT_RETURN_CODE);
        return serviceResponse;
    }

    @Before
    public void initTest() {
        serviceResponse = createEntity(em);
    }

    @Test
    @Transactional
    public void createServiceResponse() throws Exception {
        int databaseSizeBeforeCreate = serviceResponseRepository.findAll().size();

        // Create the ServiceResponse
        restServiceResponseMockMvc.perform(post("/api/service-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceResponse)))
            .andExpect(status().isCreated());

        // Validate the ServiceResponse in the database
        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeCreate + 1);
        ServiceResponse testServiceResponse = serviceResponseList.get(serviceResponseList.size() - 1);
        assertThat(testServiceResponse.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testServiceResponse.getResponseCode()).isEqualTo(DEFAULT_RESPONSE_CODE);
        assertThat(testServiceResponse.getReturnCode()).isEqualTo(DEFAULT_RETURN_CODE);
    }

    @Test
    @Transactional
    public void createServiceResponseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = serviceResponseRepository.findAll().size();

        // Create the ServiceResponse with an existing ID
        serviceResponse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restServiceResponseMockMvc.perform(post("/api/service-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceResponse)))
            .andExpect(status().isBadRequest());

        // Validate the ServiceResponse in the database
        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceResponseRepository.findAll().size();
        // set the field null
        serviceResponse.setDescription(null);

        // Create the ServiceResponse, which fails.

        restServiceResponseMockMvc.perform(post("/api/service-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceResponse)))
            .andExpect(status().isBadRequest());

        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkResponseCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceResponseRepository.findAll().size();
        // set the field null
        serviceResponse.setResponseCode(null);

        // Create the ServiceResponse, which fails.

        restServiceResponseMockMvc.perform(post("/api/service-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceResponse)))
            .andExpect(status().isBadRequest());

        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReturnCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceResponseRepository.findAll().size();
        // set the field null
        serviceResponse.setReturnCode(null);

        // Create the ServiceResponse, which fails.

        restServiceResponseMockMvc.perform(post("/api/service-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceResponse)))
            .andExpect(status().isBadRequest());

        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllServiceResponses() throws Exception {
        // Initialize the database
        serviceResponseRepository.saveAndFlush(serviceResponse);

        // Get all the serviceResponseList
        restServiceResponseMockMvc.perform(get("/api/service-responses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(serviceResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].responseCode").value(hasItem(DEFAULT_RESPONSE_CODE.toString())))
            .andExpect(jsonPath("$.[*].returnCode").value(hasItem(DEFAULT_RETURN_CODE.toString())));
    }

    @Test
    @Transactional
    public void getServiceResponse() throws Exception {
        // Initialize the database
        serviceResponseRepository.saveAndFlush(serviceResponse);

        // Get the serviceResponse
        restServiceResponseMockMvc.perform(get("/api/service-responses/{id}", serviceResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(serviceResponse.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.responseCode").value(DEFAULT_RESPONSE_CODE.toString()))
            .andExpect(jsonPath("$.returnCode").value(DEFAULT_RETURN_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingServiceResponse() throws Exception {
        // Get the serviceResponse
        restServiceResponseMockMvc.perform(get("/api/service-responses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateServiceResponse() throws Exception {
        // Initialize the database
        serviceResponseService.save(serviceResponse);

        int databaseSizeBeforeUpdate = serviceResponseRepository.findAll().size();

        // Update the serviceResponse
        ServiceResponse updatedServiceResponse = serviceResponseRepository.findOne(serviceResponse.getId());
        updatedServiceResponse
            .description(UPDATED_DESCRIPTION)
            .responseCode(UPDATED_RESPONSE_CODE)
            .returnCode(UPDATED_RETURN_CODE);

        restServiceResponseMockMvc.perform(put("/api/service-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedServiceResponse)))
            .andExpect(status().isOk());

        // Validate the ServiceResponse in the database
        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeUpdate);
        ServiceResponse testServiceResponse = serviceResponseList.get(serviceResponseList.size() - 1);
        assertThat(testServiceResponse.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testServiceResponse.getResponseCode()).isEqualTo(UPDATED_RESPONSE_CODE);
        assertThat(testServiceResponse.getReturnCode()).isEqualTo(UPDATED_RETURN_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingServiceResponse() throws Exception {
        int databaseSizeBeforeUpdate = serviceResponseRepository.findAll().size();

        // Create the ServiceResponse

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restServiceResponseMockMvc.perform(put("/api/service-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceResponse)))
            .andExpect(status().isCreated());

        // Validate the ServiceResponse in the database
        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteServiceResponse() throws Exception {
        // Initialize the database
        serviceResponseService.save(serviceResponse);

        int databaseSizeBeforeDelete = serviceResponseRepository.findAll().size();

        // Get the serviceResponse
        restServiceResponseMockMvc.perform(delete("/api/service-responses/{id}", serviceResponse.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ServiceResponse> serviceResponseList = serviceResponseRepository.findAll();
        assertThat(serviceResponseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServiceResponse.class);
        ServiceResponse serviceResponse1 = new ServiceResponse();
        serviceResponse1.setId(1L);
        ServiceResponse serviceResponse2 = new ServiceResponse();
        serviceResponse2.setId(serviceResponse1.getId());
        assertThat(serviceResponse1).isEqualTo(serviceResponse2);
        serviceResponse2.setId(2L);
        assertThat(serviceResponse1).isNotEqualTo(serviceResponse2);
        serviceResponse1.setId(null);
        assertThat(serviceResponse1).isNotEqualTo(serviceResponse2);
    }
}
