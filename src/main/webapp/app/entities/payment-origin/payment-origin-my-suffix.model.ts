import { BaseEntity } from './../../shared';

export class PaymentOriginMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public codigo?: string,
        public description?: string,
        public creationDate?: any,
        public updateDate?: any,
        public deleteDate?: any,
    ) {
    }
}
