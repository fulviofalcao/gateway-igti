/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { BankMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/bank/bank-my-suffix-detail.component';
import { BankMySuffixService } from '../../../../../../main/webapp/app/entities/bank/bank-my-suffix.service';
import { BankMySuffix } from '../../../../../../main/webapp/app/entities/bank/bank-my-suffix.model';

describe('Component Tests', () => {

    describe('BankMySuffix Management Detail Component', () => {
        let comp: BankMySuffixDetailComponent;
        let fixture: ComponentFixture<BankMySuffixDetailComponent>;
        let service: BankMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [BankMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    BankMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(BankMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new BankMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.bank).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
