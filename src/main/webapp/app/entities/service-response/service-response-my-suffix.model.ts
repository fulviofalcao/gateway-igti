import { BaseEntity } from './../../shared';

export class ServiceResponseMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public responseCode?: string,
        public returnCode?: string,
    ) {
    }
}
