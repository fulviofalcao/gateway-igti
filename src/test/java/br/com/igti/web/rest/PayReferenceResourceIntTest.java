package br.com.igti.web.rest;

import br.com.igti.GatewayApp;

import br.com.igti.domain.PayReference;
import br.com.igti.repository.PayReferenceRepository;
import br.com.igti.service.LogServiceService;
import br.com.igti.service.PayReferenceService;
import br.com.igti.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import static br.com.igti.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PayReferenceResource REST controller.
 *
 * @see PayReferenceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class PayReferenceResourceIntTest {

    private static final String DEFAULT_REFERENCE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_REFERENCE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CPF = "AAAAAAAAAA";
    private static final String UPDATED_CPF = "BBBBBBBBBB";

    private static final String DEFAULT_TAXPAYER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TAXPAYER_NAME = "BBBBBBBBBB";

    private static final LocalDateTime DEFAULT_PAYMENT_LIMIT_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_PAYMENT_LIMIT_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final String DEFAULT_TAX_NUMBER = "AAAAA";
    private static final String UPDATED_TAX_NUMBER = "BBBBB";

    private static final String DEFAULT_TAX_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TAX_DESCRIPTION = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(2);
    private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(3);

    private static final String DEFAULT_ENTITY_PAYMENT_STATUS = "A";
    private static final String UPDATED_ENTITY_PAYMENT_STATUS = "B";

    private static final LocalDateTime DEFAULT_PAYMENT_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_PAYMENT_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final String DEFAULT_AUTHENTICATION_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_AUTHENTICATION_NUMBER = "BBBBBBBBBB";

    private static final LocalDateTime DEFAULT_CONCILIATION_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_CONCILIATION_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_INTEGRATION_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_INTEGRATION_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final String DEFAULT_CONCILIATED_SITUATION = "A";
    private static final String UPDATED_CONCILIATED_SITUATION = "B";

    private static final LocalDateTime DEFAULT_CONCILIATED_SITUATION_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_CONCILIATED_SITUATION_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_CREATION_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_CREATION_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_UPDATE_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_UPDATE_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_DELETE_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_DELETE_DATE = LocalDateTime.now(ZoneId.systemDefault());

    @Autowired
    private PayReferenceRepository payReferenceRepository;

    @Autowired
    private PayReferenceService payReferenceService;

    @Autowired
    private LogServiceService logServiceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPayReferenceMockMvc;

    private PayReference payReference;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PayReferenceResource payReferenceResource = new PayReferenceResource(payReferenceService, logServiceService);
        this.restPayReferenceMockMvc = MockMvcBuilders.standaloneSetup(payReferenceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PayReference createEntity(EntityManager em) {
        PayReference payReference = new PayReference()
            .referenceNumber(DEFAULT_REFERENCE_NUMBER)
            .cpf(DEFAULT_CPF)
            .taxpayerName(DEFAULT_TAXPAYER_NAME)
            .paymentLimitDate(DEFAULT_PAYMENT_LIMIT_DATE)
            .taxNumber(DEFAULT_TAX_NUMBER)
            .taxDescription(DEFAULT_TAX_DESCRIPTION)
            .amount(DEFAULT_AMOUNT)
            .entityPaymentStatus(DEFAULT_ENTITY_PAYMENT_STATUS)
            .paymentDate(DEFAULT_PAYMENT_DATE)
            .authenticationNumber(DEFAULT_AUTHENTICATION_NUMBER)
            .conciliationDate(DEFAULT_CONCILIATION_DATE)
            .integrationDate(DEFAULT_INTEGRATION_DATE)
            .conciliatedSituation(DEFAULT_CONCILIATED_SITUATION)
            .conciliatedSituationDate(DEFAULT_CONCILIATED_SITUATION_DATE)
            .creationDate(DEFAULT_CREATION_DATE)
            .updateDate(DEFAULT_UPDATE_DATE)
            .deleteDate(DEFAULT_DELETE_DATE);
        return payReference;
    }

    @Before
    public void initTest() {
        payReference = createEntity(em);
    }

    @Test
    @Transactional
    public void createPayReference() throws Exception {
        int databaseSizeBeforeCreate = payReferenceRepository.findAll().size();

        // Create the PayReference
        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isCreated());

        // Validate the PayReference in the database
        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeCreate + 1);
        PayReference testPayReference = payReferenceList.get(payReferenceList.size() - 1);
        assertThat(testPayReference.getReferenceNumber()).isEqualTo(DEFAULT_REFERENCE_NUMBER);
        assertThat(testPayReference.getCpf()).isEqualTo(DEFAULT_CPF);
        assertThat(testPayReference.getTaxpayerName()).isEqualTo(DEFAULT_TAXPAYER_NAME);
        assertThat(testPayReference.getPaymentLimitDate()).isEqualTo(DEFAULT_PAYMENT_LIMIT_DATE);
        assertThat(testPayReference.getTaxNumber()).isEqualTo(DEFAULT_TAX_NUMBER);
        assertThat(testPayReference.getTaxDescription()).isEqualTo(DEFAULT_TAX_DESCRIPTION);
        assertThat(testPayReference.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testPayReference.getEntityPaymentStatus()).isEqualTo(DEFAULT_ENTITY_PAYMENT_STATUS);
        assertThat(testPayReference.getPaymentDate()).isEqualTo(DEFAULT_PAYMENT_DATE);
        assertThat(testPayReference.getAuthenticationNumber()).isEqualTo(DEFAULT_AUTHENTICATION_NUMBER);
        assertThat(testPayReference.getConciliationDate()).isEqualTo(DEFAULT_CONCILIATION_DATE);
        assertThat(testPayReference.getIntegrationDate()).isEqualTo(DEFAULT_INTEGRATION_DATE);
        assertThat(testPayReference.getConciliatedSituation()).isEqualTo(DEFAULT_CONCILIATED_SITUATION);
        assertThat(testPayReference.getConciliatedSituationDate()).isEqualTo(DEFAULT_CONCILIATED_SITUATION_DATE);
        assertThat(testPayReference.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testPayReference.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testPayReference.getDeleteDate()).isEqualTo(DEFAULT_DELETE_DATE);
    }

    @Test
    @Transactional
    public void createPayReferenceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = payReferenceRepository.findAll().size();

        // Create the PayReference with an existing ID
        payReference.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        // Validate the PayReference in the database
        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkReferenceNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setReferenceNumber(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCpfIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setCpf(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTaxpayerNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setTaxpayerName(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaymentLimitDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setPaymentLimitDate(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTaxNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setTaxNumber(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTaxDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setTaxDescription(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setAmount(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEntityPaymentStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setEntityPaymentStatus(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = payReferenceRepository.findAll().size();
        // set the field null
        payReference.setCreationDate(null);

        // Create the PayReference, which fails.

        restPayReferenceMockMvc.perform(post("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isBadRequest());

        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPayReferences() throws Exception {
        // Initialize the database
        payReferenceRepository.saveAndFlush(payReference);

        // Get all the payReferenceList
        restPayReferenceMockMvc.perform(get("/api/pay-references?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payReference.getId().intValue())))
            .andExpect(jsonPath("$.[*].referenceNumber").value(hasItem(DEFAULT_REFERENCE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].cpf").value(hasItem(DEFAULT_CPF.toString())))
            .andExpect(jsonPath("$.[*].taxpayerName").value(hasItem(DEFAULT_TAXPAYER_NAME.toString())))
            .andExpect(jsonPath("$.[*].paymentLimitDate").value(hasItem(DEFAULT_PAYMENT_LIMIT_DATE.toString())))
            .andExpect(jsonPath("$.[*].taxNumber").value(hasItem(DEFAULT_TAX_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].taxDescription").value(hasItem(DEFAULT_TAX_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].entityPaymentStatus").value(hasItem(DEFAULT_ENTITY_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].paymentDate").value(hasItem(DEFAULT_PAYMENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].authenticationNumber").value(hasItem(DEFAULT_AUTHENTICATION_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].conciliationDate").value(hasItem(DEFAULT_CONCILIATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].integrationDate").value(hasItem(DEFAULT_INTEGRATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].conciliatedSituation").value(hasItem(DEFAULT_CONCILIATED_SITUATION.toString())))
            .andExpect(jsonPath("$.[*].conciliatedSituationDate").value(hasItem(DEFAULT_CONCILIATED_SITUATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].deleteDate").value(hasItem(DEFAULT_DELETE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPayReference() throws Exception {
        // Initialize the database
        payReferenceRepository.saveAndFlush(payReference);

        // Get the payReference
        restPayReferenceMockMvc.perform(get("/api/pay-references/{id}", payReference.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(payReference.getId().intValue()))
            .andExpect(jsonPath("$.referenceNumber").value(DEFAULT_REFERENCE_NUMBER.toString()))
            .andExpect(jsonPath("$.cpf").value(DEFAULT_CPF.toString()))
            .andExpect(jsonPath("$.taxpayerName").value(DEFAULT_TAXPAYER_NAME.toString()))
            .andExpect(jsonPath("$.paymentLimitDate").value(DEFAULT_PAYMENT_LIMIT_DATE.toString()))
            .andExpect(jsonPath("$.taxNumber").value(DEFAULT_TAX_NUMBER.toString()))
            .andExpect(jsonPath("$.taxDescription").value(DEFAULT_TAX_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()))
            .andExpect(jsonPath("$.entityPaymentStatus").value(DEFAULT_ENTITY_PAYMENT_STATUS.toString()))
            .andExpect(jsonPath("$.paymentDate").value(DEFAULT_PAYMENT_DATE.toString()))
            .andExpect(jsonPath("$.authenticationNumber").value(DEFAULT_AUTHENTICATION_NUMBER.toString()))
            .andExpect(jsonPath("$.conciliationDate").value(DEFAULT_CONCILIATION_DATE.toString()))
            .andExpect(jsonPath("$.integrationDate").value(DEFAULT_INTEGRATION_DATE.toString()))
            .andExpect(jsonPath("$.conciliatedSituation").value(DEFAULT_CONCILIATED_SITUATION.toString()))
            .andExpect(jsonPath("$.conciliatedSituationDate").value(DEFAULT_CONCILIATED_SITUATION_DATE.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.deleteDate").value(DEFAULT_DELETE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPayReference() throws Exception {
        // Get the payReference
        restPayReferenceMockMvc.perform(get("/api/pay-references/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePayReference() throws Exception {
        // Initialize the database
        payReferenceService.save(payReference);

        int databaseSizeBeforeUpdate = payReferenceRepository.findAll().size();

        // Update the payReference
        PayReference updatedPayReference = payReferenceRepository.findOne(payReference.getId());
        updatedPayReference
            .referenceNumber(UPDATED_REFERENCE_NUMBER)
            .cpf(UPDATED_CPF)
            .taxpayerName(UPDATED_TAXPAYER_NAME)
            .paymentLimitDate(UPDATED_PAYMENT_LIMIT_DATE)
            .taxNumber(UPDATED_TAX_NUMBER)
            .taxDescription(UPDATED_TAX_DESCRIPTION)
            .amount(UPDATED_AMOUNT)
            .entityPaymentStatus(UPDATED_ENTITY_PAYMENT_STATUS)
            .paymentDate(UPDATED_PAYMENT_DATE)
            .authenticationNumber(UPDATED_AUTHENTICATION_NUMBER)
            .conciliationDate(UPDATED_CONCILIATION_DATE)
            .integrationDate(UPDATED_INTEGRATION_DATE)
            .conciliatedSituation(UPDATED_CONCILIATED_SITUATION)
            .conciliatedSituationDate(UPDATED_CONCILIATED_SITUATION_DATE)
            .creationDate(UPDATED_CREATION_DATE)
            .updateDate(UPDATED_UPDATE_DATE)
            .deleteDate(UPDATED_DELETE_DATE);

        restPayReferenceMockMvc.perform(put("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPayReference)))
            .andExpect(status().isOk());

        // Validate the PayReference in the database
        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeUpdate);
        PayReference testPayReference = payReferenceList.get(payReferenceList.size() - 1);
        assertThat(testPayReference.getReferenceNumber()).isEqualTo(UPDATED_REFERENCE_NUMBER);
        assertThat(testPayReference.getCpf()).isEqualTo(UPDATED_CPF);
        assertThat(testPayReference.getTaxpayerName()).isEqualTo(UPDATED_TAXPAYER_NAME);
        assertThat(testPayReference.getPaymentLimitDate()).isEqualTo(UPDATED_PAYMENT_LIMIT_DATE);
        assertThat(testPayReference.getTaxNumber()).isEqualTo(UPDATED_TAX_NUMBER);
        assertThat(testPayReference.getTaxDescription()).isEqualTo(UPDATED_TAX_DESCRIPTION);
        assertThat(testPayReference.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPayReference.getEntityPaymentStatus()).isEqualTo(UPDATED_ENTITY_PAYMENT_STATUS);
        assertThat(testPayReference.getPaymentDate()).isEqualTo(UPDATED_PAYMENT_DATE);
        assertThat(testPayReference.getAuthenticationNumber()).isEqualTo(UPDATED_AUTHENTICATION_NUMBER);
        assertThat(testPayReference.getConciliationDate()).isEqualTo(UPDATED_CONCILIATION_DATE);
        assertThat(testPayReference.getIntegrationDate()).isEqualTo(UPDATED_INTEGRATION_DATE);
        assertThat(testPayReference.getConciliatedSituation()).isEqualTo(UPDATED_CONCILIATED_SITUATION);
        assertThat(testPayReference.getConciliatedSituationDate()).isEqualTo(UPDATED_CONCILIATED_SITUATION_DATE);
        assertThat(testPayReference.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testPayReference.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testPayReference.getDeleteDate()).isEqualTo(UPDATED_DELETE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPayReference() throws Exception {
        int databaseSizeBeforeUpdate = payReferenceRepository.findAll().size();

        // Create the PayReference

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPayReferenceMockMvc.perform(put("/api/pay-references")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payReference)))
            .andExpect(status().isCreated());

        // Validate the PayReference in the database
        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePayReference() throws Exception {
        // Initialize the database
        payReferenceService.save(payReference);

        int databaseSizeBeforeDelete = payReferenceRepository.findAll().size();

        // Get the payReference
        restPayReferenceMockMvc.perform(delete("/api/pay-references/{id}", payReference.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PayReference> payReferenceList = payReferenceRepository.findAll();
        assertThat(payReferenceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PayReference.class);
        PayReference payReference1 = new PayReference();
        payReference1.setId(1L);
        PayReference payReference2 = new PayReference();
        payReference2.setId(payReference1.getId());
        assertThat(payReference1).isEqualTo(payReference2);
        payReference2.setId(2L);
        assertThat(payReference1).isNotEqualTo(payReference2);
        payReference1.setId(null);
        assertThat(payReference1).isNotEqualTo(payReference2);
    }
}
