import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TaxMySuffixComponent } from './tax-my-suffix.component';
import { TaxMySuffixDetailComponent } from './tax-my-suffix-detail.component';
import { TaxMySuffixPopupComponent } from './tax-my-suffix-dialog.component';
import { TaxMySuffixDeletePopupComponent } from './tax-my-suffix-delete-dialog.component';

@Injectable()
export class TaxMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const taxRoute: Routes = [
    {
        path: 'tax-my-suffix',
        component: TaxMySuffixComponent,
        resolve: {
            'pagingParams': TaxMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.tax.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tax-my-suffix/:id',
        component: TaxMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.tax.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const taxPopupRoute: Routes = [
    {
        path: 'tax-my-suffix-new',
        component: TaxMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.tax.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-my-suffix/:id/edit',
        component: TaxMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.tax.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tax-my-suffix/:id/delete',
        component: TaxMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.tax.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
