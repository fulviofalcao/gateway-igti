import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ServiceResponseMySuffix } from './service-response-my-suffix.model';
import { ServiceResponseMySuffixPopupService } from './service-response-my-suffix-popup.service';
import { ServiceResponseMySuffixService } from './service-response-my-suffix.service';

@Component({
    selector: 'jhi-service-response-my-suffix-delete-dialog',
    templateUrl: './service-response-my-suffix-delete-dialog.component.html'
})
export class ServiceResponseMySuffixDeleteDialogComponent {

    serviceResponse: ServiceResponseMySuffix;

    constructor(
        private serviceResponseService: ServiceResponseMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.serviceResponseService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'serviceResponseListModification',
                content: 'Deleted an serviceResponse'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-service-response-my-suffix-delete-popup',
    template: ''
})
export class ServiceResponseMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private serviceResponsePopupService: ServiceResponseMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.serviceResponsePopupService
                .open(ServiceResponseMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
