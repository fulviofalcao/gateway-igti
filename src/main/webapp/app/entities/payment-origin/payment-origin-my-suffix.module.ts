import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    PaymentOriginMySuffixService,
    PaymentOriginMySuffixPopupService,
    PaymentOriginMySuffixComponent,
    PaymentOriginMySuffixDetailComponent,
    PaymentOriginMySuffixDialogComponent,
    PaymentOriginMySuffixPopupComponent,
    PaymentOriginMySuffixDeletePopupComponent,
    PaymentOriginMySuffixDeleteDialogComponent,
    paymentOriginRoute,
    paymentOriginPopupRoute,
    PaymentOriginMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...paymentOriginRoute,
    ...paymentOriginPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PaymentOriginMySuffixComponent,
        PaymentOriginMySuffixDetailComponent,
        PaymentOriginMySuffixDialogComponent,
        PaymentOriginMySuffixDeleteDialogComponent,
        PaymentOriginMySuffixPopupComponent,
        PaymentOriginMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        PaymentOriginMySuffixComponent,
        PaymentOriginMySuffixDialogComponent,
        PaymentOriginMySuffixPopupComponent,
        PaymentOriginMySuffixDeleteDialogComponent,
        PaymentOriginMySuffixDeletePopupComponent,
    ],
    providers: [
        PaymentOriginMySuffixService,
        PaymentOriginMySuffixPopupService,
        PaymentOriginMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayPaymentOriginMySuffixModule {}
