package br.com.igti.service;

import br.com.igti.domain.Situation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Situation.
 */
public interface SituationService {

    /**
     * Save a situation.
     *
     * @param situation the entity to save
     * @return the persisted entity
     */
    Situation save(Situation situation);

    /**
     *  Get all the situations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Situation> findAll(Pageable pageable);

    /**
     *  Get the "id" situation.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Situation findOne(Long id);

    /**
     *  Delete the "id" situation.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
