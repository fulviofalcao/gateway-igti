import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PayReferenceMySuffix } from './pay-reference-my-suffix.model';
import { PayReferenceMySuffixService } from './pay-reference-my-suffix.service';

@Component({
    selector: 'jhi-pay-reference-my-suffix-detail',
    templateUrl: './pay-reference-my-suffix-detail.component.html'
})
export class PayReferenceMySuffixDetailComponent implements OnInit, OnDestroy {

    payReference: PayReferenceMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private payReferenceService: PayReferenceMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPayReferences();
    }

    load(id) {
        this.payReferenceService.find(id).subscribe((payReference) => {
            this.payReference = payReference;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPayReferences() {
        this.eventSubscriber = this.eventManager.subscribe(
            'payReferenceListModification',
            (response) => this.load(this.payReference.id)
        );
    }
}
