import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { MessageMySuffix } from './message-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MessageMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/messages';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(message: MessageMySuffix): Observable<MessageMySuffix> {
        const copy = this.convert(message);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(message: MessageMySuffix): Observable<MessageMySuffix> {
        const copy = this.convert(message);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<MessageMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to MessageMySuffix.
     */
    private convertItemFromServer(json: any): MessageMySuffix {
        const entity: MessageMySuffix = Object.assign(new MessageMySuffix(), json);
        entity.creationDate = this.dateUtils
            .convertLocalDateFromServer(json.creationDate);
        entity.updateDate = this.dateUtils
            .convertLocalDateFromServer(json.updateDate);
        entity.deleteDate = this.dateUtils
            .convertLocalDateFromServer(json.deleteDate);
        return entity;
    }

    /**
     * Convert a MessageMySuffix to a JSON which can be sent to the server.
     */
    private convert(message: MessageMySuffix): MessageMySuffix {
        const copy: MessageMySuffix = Object.assign({}, message);
        copy.creationDate = this.dateUtils
            .convertLocalDateToServer(message.creationDate);
        copy.updateDate = this.dateUtils
            .convertLocalDateToServer(message.updateDate);
        copy.deleteDate = this.dateUtils
            .convertLocalDateToServer(message.deleteDate);
        return copy;
    }
}
