package br.com.igti.repository;

import br.com.igti.domain.ServiceResponse;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceResponse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceResponseRepository extends JpaRepository<ServiceResponse, Long> {

}
