package br.com.igti.service.vo;

import java.math.BigDecimal;

public class PaymentReference {

    private String referenceNumber;
    private BigDecimal amount;
    private String authenticationNumber;
    private String paymentOrigin;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public PaymentReference referenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        return this;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public PaymentReference amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAuthenticationNumber() {
        return authenticationNumber;
    }

    public PaymentReference authenticationNumber(String authenticationNumber) {
        this.authenticationNumber = authenticationNumber;
        return this;
    }

    public void setAuthenticationNumber(String authenticationNumber) {
        this.authenticationNumber = authenticationNumber;
    }

    public String getPaymentOrigin() {
        return paymentOrigin;
    }

    public PaymentReference paymentOrigin(String paymentOrigin) {
        this.paymentOrigin = paymentOrigin;
        return this;
    }

    public void setPaymentOrigin(String paymentOrigin) {
        this.paymentOrigin = paymentOrigin;
    }
}
