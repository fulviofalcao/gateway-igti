import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SituationMySuffix } from './situation-my-suffix.model';
import { SituationMySuffixService } from './situation-my-suffix.service';

@Component({
    selector: 'jhi-situation-my-suffix-detail',
    templateUrl: './situation-my-suffix-detail.component.html'
})
export class SituationMySuffixDetailComponent implements OnInit, OnDestroy {

    situation: SituationMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private situationService: SituationMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSituations();
    }

    load(id) {
        this.situationService.find(id).subscribe((situation) => {
            this.situation = situation;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSituations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'situationListModification',
            (response) => this.load(this.situation.id)
        );
    }
}
