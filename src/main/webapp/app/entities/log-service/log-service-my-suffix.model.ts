import { BaseEntity } from './../../shared';

export class LogServiceMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public imput?: string,
        public output?: string,
        public logDate?: any,
        public request?: BaseEntity,
        public response?: BaseEntity,
    ) {
    }
}
