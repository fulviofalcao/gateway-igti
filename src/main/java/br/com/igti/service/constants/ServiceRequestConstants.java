package br.com.igti.service.constants;

import br.com.igti.domain.ServiceRequest;
import br.com.igti.repository.ServiceRequestRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class ServiceRequestConstants {
    public static ServiceRequest VALIDATE_REFERENCE;
    public static ServiceRequest PAYMENT_CONFIRMATION;
    public static ServiceRequest PAYMENT_CANCELLATION;
    public static ServiceRequest PAYMENT_CONCILIATED;
    public static ServiceRequest REFERENCE_UPDATE;
    public static ServiceRequest REFERENCE_CREATE;
    public static ServiceRequest REFERENCE_DELETE;
    public static ServiceRequest JOB_PAYMENT_NOT_CONFIRMATION;

    @Bean
    @DependsOn("liquibase")
    public ServiceRequestConstants ServiceRequestConstantsInitializer(ServiceRequestRepository serviceRequestRepository) {
        VALIDATE_REFERENCE = serviceRequestRepository.findOne(1L);
        PAYMENT_CONFIRMATION = serviceRequestRepository.findOne(2L);
        PAYMENT_CANCELLATION = serviceRequestRepository.findOne(3L);
        PAYMENT_CONCILIATED = serviceRequestRepository.findOne(4L);
        REFERENCE_UPDATE = serviceRequestRepository.findOne(5L);
        REFERENCE_CREATE = serviceRequestRepository.findOne(6L);
        REFERENCE_DELETE = serviceRequestRepository.findOne(7L);
        JOB_PAYMENT_NOT_CONFIRMATION = serviceRequestRepository.findOne(8L);
        return new ServiceRequestConstants();
    }
}
