package br.com.igti.service;

import br.com.igti.domain.ServiceResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ServiceResponse.
 */
public interface ServiceResponseService {

    /**
     * Save a serviceResponse.
     *
     * @param serviceResponse the entity to save
     * @return the persisted entity
     */
    ServiceResponse save(ServiceResponse serviceResponse);

    /**
     *  Get all the serviceResponses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ServiceResponse> findAll(Pageable pageable);

    /**
     *  Get the "id" serviceResponse.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ServiceResponse findOne(Long id);

    /**
     *  Delete the "id" serviceResponse.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
