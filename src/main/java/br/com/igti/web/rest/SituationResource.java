package br.com.igti.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.igti.domain.Situation;
import br.com.igti.service.SituationService;
import br.com.igti.web.rest.errors.BadRequestAlertException;
import br.com.igti.web.rest.util.HeaderUtil;
import br.com.igti.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Situation.
 */
@RestController
@RequestMapping("/api")
public class SituationResource {

    private final Logger log = LoggerFactory.getLogger(SituationResource.class);

    private static final String ENTITY_NAME = "situation";

    private final SituationService situationService;

    public SituationResource(SituationService situationService) {
        this.situationService = situationService;
    }

    /**
     * POST  /situations : Create a new situation.
     *
     * @param situation the situation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new situation, or with status 400 (Bad Request) if the situation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/situations")
    @Timed
    public ResponseEntity<Situation> createSituation(@Valid @RequestBody Situation situation) throws URISyntaxException {
        log.debug("REST request to save Situation : {}", situation);
        if (situation.getId() != null) {
            throw new BadRequestAlertException("A new situation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Situation result = situationService.save(situation);
        return ResponseEntity.created(new URI("/api/situations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /situations : Updates an existing situation.
     *
     * @param situation the situation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated situation,
     * or with status 400 (Bad Request) if the situation is not valid,
     * or with status 500 (Internal Server Error) if the situation couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/situations")
    @Timed
    public ResponseEntity<Situation> updateSituation(@Valid @RequestBody Situation situation) throws URISyntaxException {
        log.debug("REST request to update Situation : {}", situation);
        if (situation.getId() == null) {
            return createSituation(situation);
        }
        Situation result = situationService.save(situation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, situation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /situations : get all the situations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of situations in body
     */
    @GetMapping("/situations")
    @Timed
    public ResponseEntity<List<Situation>> getAllSituations(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Situations");
        Page<Situation> page = situationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/situations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /situations/:id : get the "id" situation.
     *
     * @param id the id of the situation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the situation, or with status 404 (Not Found)
     */
    @GetMapping("/situations/{id}")
    @Timed
    public ResponseEntity<Situation> getSituation(@PathVariable Long id) {
        log.debug("REST request to get Situation : {}", id);
        Situation situation = situationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(situation));
    }

    /**
     * DELETE  /situations/:id : delete the "id" situation.
     *
     * @param id the id of the situation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/situations/{id}")
    @Timed
    public ResponseEntity<Void> deleteSituation(@PathVariable Long id) {
        log.debug("REST request to delete Situation : {}", id);
        situationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
