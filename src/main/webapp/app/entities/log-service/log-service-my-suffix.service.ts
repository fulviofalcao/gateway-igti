import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { LogServiceMySuffix } from './log-service-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class LogServiceMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/log-services';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(logService: LogServiceMySuffix): Observable<LogServiceMySuffix> {
        const copy = this.convert(logService);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(logService: LogServiceMySuffix): Observable<LogServiceMySuffix> {
        const copy = this.convert(logService);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<LogServiceMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to LogServiceMySuffix.
     */
    private convertItemFromServer(json: any): LogServiceMySuffix {
        const entity: LogServiceMySuffix = Object.assign(new LogServiceMySuffix(), json);
        entity.logDate = this.dateUtils
            .convertLocalDateFromServer(json.logDate);
        return entity;
    }

    /**
     * Convert a LogServiceMySuffix to a JSON which can be sent to the server.
     */
    private convert(logService: LogServiceMySuffix): LogServiceMySuffix {
        const copy: LogServiceMySuffix = Object.assign({}, logService);
        copy.logDate = this.dateUtils
            .convertLocalDateToServer(logService.logDate);
        return copy;
    }
}
