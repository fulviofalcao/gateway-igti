import { BaseEntity } from './../../shared';

export class PayReferenceMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public referenceNumber?: string,
        public cpf?: string,
        public taxpayerName?: string,
        public paymentLimitDate?: any,
        public taxNumber?: string,
        public taxDescription?: string,
        public amount?: number,
        public entityPaymentStatus?: string,
        public paymentDate?: any,
        public authenticationNumber?: string,
        public conciliationDate?: any,
        public integrationDate?: any,
        public conciliatedSituation?: string,
        public conciliatedSituationDate?: any,
        public creationDate?: any,
        public updateDate?: any,
        public deleteDate?: any,
        public situation?: BaseEntity,
        public currency?: BaseEntity,
        public bank?: BaseEntity,
        public paymentOrigin?: BaseEntity,
    ) {
    }
}
