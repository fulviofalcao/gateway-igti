package br.com.igti.service.impl;

import br.com.igti.service.ServiceResponseService;
import br.com.igti.domain.ServiceResponse;
import br.com.igti.repository.ServiceResponseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ServiceResponse.
 */
@Service
@Transactional
public class ServiceResponseServiceImpl implements ServiceResponseService{

    private final Logger log = LoggerFactory.getLogger(ServiceResponseServiceImpl.class);

    private final ServiceResponseRepository serviceResponseRepository;

    public ServiceResponseServiceImpl(ServiceResponseRepository serviceResponseRepository) {
        this.serviceResponseRepository = serviceResponseRepository;
    }

    /**
     * Save a serviceResponse.
     *
     * @param serviceResponse the entity to save
     * @return the persisted entity
     */
    @Override
    public ServiceResponse save(ServiceResponse serviceResponse) {
        log.debug("Request to save ServiceResponse : {}", serviceResponse);
        return serviceResponseRepository.save(serviceResponse);
    }

    /**
     *  Get all the serviceResponses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ServiceResponse> findAll(Pageable pageable) {
        log.debug("Request to get all ServiceResponses");
        return serviceResponseRepository.findAll(pageable);
    }

    /**
     *  Get one serviceResponse by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ServiceResponse findOne(Long id) {
        log.debug("Request to get ServiceResponse : {}", id);
        return serviceResponseRepository.findOne(id);
    }

    /**
     *  Delete the  serviceResponse by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ServiceResponse : {}", id);
        serviceResponseRepository.delete(id);
    }
}
