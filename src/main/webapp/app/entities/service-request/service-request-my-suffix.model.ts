import { BaseEntity } from './../../shared';

export class ServiceRequestMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public requestCode?: string,
    ) {
    }
}
