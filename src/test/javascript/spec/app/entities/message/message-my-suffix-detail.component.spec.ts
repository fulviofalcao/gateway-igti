/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { MessageMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/message/message-my-suffix-detail.component';
import { MessageMySuffixService } from '../../../../../../main/webapp/app/entities/message/message-my-suffix.service';
import { MessageMySuffix } from '../../../../../../main/webapp/app/entities/message/message-my-suffix.model';

describe('Component Tests', () => {

    describe('MessageMySuffix Management Detail Component', () => {
        let comp: MessageMySuffixDetailComponent;
        let fixture: ComponentFixture<MessageMySuffixDetailComponent>;
        let service: MessageMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [MessageMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    MessageMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(MessageMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MessageMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MessageMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new MessageMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.message).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
