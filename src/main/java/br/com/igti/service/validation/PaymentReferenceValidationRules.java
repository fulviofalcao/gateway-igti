package br.com.igti.service.validation;

import br.com.igti.domain.PayReference;
import br.com.igti.domain.PaymentOrigin;
import br.com.igti.service.LogServiceService;
import br.com.igti.service.constants.ServiceRequestConstants;
import br.com.igti.service.constants.ServiceResponseConstants;
import br.com.igti.service.constants.SituationConstants;
import br.com.igti.service.util.Assert;
import br.com.igti.service.util.ValidationCPF;
import br.com.igti.service.vo.CancelPaymentReference;
import br.com.igti.service.vo.CreateReference;
import br.com.igti.service.vo.PaymentReference;
import br.com.igti.service.vo.UpdatePayReference;
import br.com.igti.web.rest.errors.BadRequestAlertException;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Optional;

public class PaymentReferenceValidationRules {

    private static final String ENTITY_NAME = "payReference";

    private static LogServiceService logServiceService;

    public static void setLogServiceService(LogServiceService logServiceService) {
        PaymentReferenceValidationRules.logServiceService = logServiceService;
    }

    public static void validatePaymentReferenceExistence(Optional<PayReference> optionalPaymentReference, PaymentReference payment) {
        if (!optionalPaymentReference.isPresent())
            logServiceService.saveLog(payment, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.INVALID_REFERENCE);
        optionalPaymentReference.orElseThrow(() -> new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists"));
    }

    public static void validatePaymentReferenceExistence(Optional<PayReference> optionalPaymentReference, CancelPaymentReference cancelPaymentReference) {
        if (!optionalPaymentReference.isPresent())
            logServiceService.saveLog(cancelPaymentReference, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.PAYMENT_CANCELLATION, ServiceResponseConstants.INVALID_REFERENCE);
        optionalPaymentReference.orElseThrow(() -> new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists"));
    }

    public static void validatePaymentReferenceExistence(Optional<PayReference> optionalPaymentReference, UpdatePayReference updatePayReference) {
        if (!optionalPaymentReference.isPresent())
            logServiceService.saveLog(updatePayReference, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.REFERENCE_UPDATE, ServiceResponseConstants.INVALID_REFERENCE);
        optionalPaymentReference.orElseThrow(() -> new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists"));
    }

    public static void validateReferenceExistence(Optional<CreateReference> optionalPaymentReference) {
        if (optionalPaymentReference.isPresent())
            logServiceService.saveLog(optionalPaymentReference, "statusCode: 400 -» referenceNumberIsExists", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.INVALID_REFERENCE);
        optionalPaymentReference.orElseThrow(() -> new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberIsExists"));
    }

    public static void validateDeleteReferenceExistence(Optional<PayReference> optionalPaymentReference, String reference) {
        if (!optionalPaymentReference.isPresent())
            logServiceService.saveLog(reference, "statusCode: 400 -» referenceNumberNotExists", ServiceRequestConstants.REFERENCE_DELETE, ServiceResponseConstants.INVALID_REFERENCE);
        optionalPaymentReference.orElseThrow(() -> new BadRequestAlertException(ServiceResponseConstants.INVALID_REFERENCE.toString(), ENTITY_NAME, "referenceNumberNotExists"));
    }

    public static void validateAmount(PayReference payReference, BigDecimal amount) {
        if (payReference.getAmount().compareTo(amount) != 0) {
            logServiceService.saveLog(payReference, "statusCode: 400 -» paymentInvalidAmount", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.INVALID_AMOUNT);
            throw new BadRequestAlertException(ServiceResponseConstants.INVALID_AMOUNT.toString(), ENTITY_NAME, "paymentInvalidAmount");
        }
    }

    public static void validatePaymentReferenceIsAlreadyPaid(PayReference payReference) {
        if(payReference.getSituation().equals(SituationConstants.PAID)
            || payReference.getSituation().equals(SituationConstants.CONCILIATED)){
            logServiceService.saveLog(payReference, "statusCode: 400 -» referenceIsAlreadyPaid", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.REFERENCE_ALREADY_PAID);
            throw new BadRequestAlertException(ServiceResponseConstants.REFERENCE_ALREADY_PAID.toString(), ENTITY_NAME, "referenceIsAlreadyPaid");
        }
    }

    public static void validatePaymentReferenceIsCancelled(PayReference payReference){
        if (payReference.getSituation().equals(SituationConstants.CANCELED)){
            logServiceService.saveLog(payReference, "statusCode: 400 -» referenceIsCancelled", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.REFERENCE_ALREADY_CANCELLED);
            throw new BadRequestAlertException(ServiceResponseConstants.REFERENCE_ALREADY_CANCELLED.toString(), ENTITY_NAME, "referenceIsCancelled");
        }
    }

    public static void validatePaymentReferenceExpiration(PayReference payReference) {
        if(LocalDateTime.now(Clock.systemDefaultZone()).toLocalDate().isAfter(payReference.getPaymentLimitDate().toLocalDate())){
            logServiceService.saveLog(payReference, "statusCode: 400 -» referenceIsExpiration", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.EXPIRED_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.EXPIRED_REFERENCE.toString(), ENTITY_NAME, "referenceIsExpiration");
        }
    }

    public static void validatePaymentOrigin(Optional<PaymentOrigin> optionalPaymentReference, PaymentReference payment){
        if (!optionalPaymentReference.isPresent())
            logServiceService.saveLog(payment, "statusCode: 400 -» paymentOriginIsInvalid", ServiceRequestConstants.PAYMENT_CANCELLATION, ServiceResponseConstants.INVALID_ORIGEN_PAID);
        optionalPaymentReference.orElseThrow(() ->  new BadRequestAlertException(ServiceResponseConstants.INVALID_ORIGEN_PAID.toString(), ENTITY_NAME, "paymentOriginIsInvalid"));
    }

    public static void validateAuthenticationNumber(PayReference payReference, CancelPaymentReference cancelPaymentReference) {
        if(!payReference.getAuthenticationNumber().equals(cancelPaymentReference.getAuthenticationNumber())){
            logServiceService.saveLog(cancelPaymentReference, "statusCode: 400 -» authenticationNumberIsInvalid", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.INVALID_ERROR_TYPE);
            throw new BadRequestAlertException(ServiceResponseConstants.INVALID_ERROR_TYPE.toString(), ENTITY_NAME, "authenticationNumberIsInvalid");
        }
    }

    public static void validateSituationPayReference(PayReference payReference){
        if(!payReference.getSituation().equals(SituationConstants.PAID)
            && !payReference.getSituation().equals(SituationConstants.CONCILIATED)){
            logServiceService.saveLog(payReference, "statusCode: 400 -» situationIsInvalid", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.REFERENCE_NOT_PAID);
            throw new BadRequestAlertException(ServiceResponseConstants.REFERENCE_NOT_PAID.toString(), ENTITY_NAME, "situationIsInvalid");
        }
    }

    public static void validatePaymentReferenceLimitDate(LocalDateTime localDateTime) {
        if(LocalDateTime.now(Clock.systemDefaultZone()).toLocalDate().isAfter(localDateTime.toLocalDate())){
            logServiceService.saveLog(localDateTime, "statusCode: 400 -» paymentReferenceLimitDate", ServiceRequestConstants.REFERENCE_UPDATE, ServiceResponseConstants.UPDATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.UPDATE_NOT_REFERENCE.toString(), ENTITY_NAME, "paymentReferenceLimitDate");
        }
    }

    public static void validatePaymentReferenceTaxNumber(String taxNumber){
        if(Assert.isNullOrEmpty(taxNumber)){
            logServiceService.saveLog(taxNumber, "statusCode: 400 -» taxNumberIsInvalid", ServiceRequestConstants.REFERENCE_UPDATE, ServiceResponseConstants.UPDATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.UPDATE_NOT_REFERENCE.toString(), ENTITY_NAME, "taxNumberIsInvalid");
        }
    }

    public static void validatePaymentReferenceTaxDescription(String taxDescription){
        if(Assert.isNullOrEmpty(taxDescription)){
            logServiceService.saveLog(taxDescription, "statusCode: 400 -» taxDescriptionIsInvalid", ServiceRequestConstants.REFERENCE_UPDATE, ServiceResponseConstants.UPDATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.UPDATE_NOT_REFERENCE.toString(), ENTITY_NAME, "taxDescriptionIsInvalid");
        }
    }

    public static void validatePaymentReferenceAmount(BigDecimal amount){
        if(amount.compareTo(BigDecimal.ZERO) <= 0){
            logServiceService.saveLog(amount, "statusCode: 400 -» amountIsInvalid", ServiceRequestConstants.REFERENCE_UPDATE, ServiceResponseConstants.UPDATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.UPDATE_NOT_REFERENCE.toString(), ENTITY_NAME, "amountIsInvalid");
        }
    }

    public static void validateAuthenticationNumber(String authenticationNumber){
        if(Assert.isNullOrEmpty(authenticationNumber)){
            logServiceService.saveLog(authenticationNumber, "statusCode: 400 -» authenticationNumberIsInvalid", ServiceRequestConstants.PAYMENT_CONFIRMATION, ServiceResponseConstants.REFERENCE_NOT_PAID);
            throw new BadRequestAlertException(ServiceResponseConstants.UPDATE_NOT_REFERENCE.toString(), ENTITY_NAME, "authenticationNumberIsInvalid");
        }
    }

    public static void validateCpf (String cpf){
        if(Assert.isNullOrEmpty(cpf)|| !ValidationCPF.isCPF(cpf)){
            logServiceService.saveLog(cpf, "statusCode: 400 -» cpfIsInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.CREATE_NOT_REFERENCE.toString(), ENTITY_NAME, "cpfIsInvalid");
        }
    }

    public static void validateTaxpayerName (String taxpayerName){
        if(Assert.isNullOrEmpty(taxpayerName)){
            logServiceService.saveLog(taxpayerName, "statusCode: 400 -» taxpayerNameIsInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.CREATE_NOT_REFERENCE.toString(), ENTITY_NAME, "taxpayerNameIsInvalid");
        }
    }

    public static void validatePaymentLimitDate (LocalDateTime paymentLimitDate){
        if(LocalDateTime.now(Clock.systemDefaultZone()).toLocalDate().isAfter(paymentLimitDate.toLocalDate())){
            logServiceService.saveLog(paymentLimitDate, "statusCode: 400 -» paymentLimitDateIsExpiration", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.EXPIRED_REFERENCE.toString(), ENTITY_NAME, "paymentLimitDateIsExpiration");
        }
    }

    public static void validateTaxNumber (String taxNumber){
        if(Assert.isNullOrEmpty(taxNumber)){
            logServiceService.saveLog(taxNumber, "statusCode: 400 -» taxNumberIsInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.CREATE_NOT_REFERENCE.toString(), ENTITY_NAME, "taxNumberIsInvalid");
        }
    }

    public static void validateTaxDescription (String taxDescription){
        if(Assert.isNullOrEmpty(taxDescription)){
            logServiceService.saveLog(taxDescription, "statusCode: 400 -» taxDescriptionIsInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.CREATE_NOT_REFERENCE.toString(), ENTITY_NAME, "taxDescriptionIsInvalid");
        }
    }

    public static void validateAmount(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) != 1) {
            logServiceService.saveLog(amount, "statusCode: 400 -» amountIsInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.INVALID_AMOUNT.toString(), ENTITY_NAME, "amountIsInvalid");
        }
    }

    public static void validateCurrency (String currency){
        if(Assert.isNullOrEmpty(currency)){
            logServiceService.saveLog(currency, "statusCode: 400 -» currencyIsInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.CREATE_NOT_REFERENCE.toString(), ENTITY_NAME, "currencyIsInvalid");
        }
    }

    public static void validateBank(String bank){
        if(Assert.isNullOrEmpty(bank)){
            logServiceService.saveLog(bank, "statusCode: 400 -» bankIsInvalid", ServiceRequestConstants.REFERENCE_CREATE, ServiceResponseConstants.CREATE_NOT_REFERENCE);
            throw new BadRequestAlertException(ServiceResponseConstants.CREATE_NOT_REFERENCE.toString(), ENTITY_NAME, "bankIsInvalid");
        }
    }

    public static void validateDeleteReferenceIsAlreadyPaid(PayReference payReference) {
        if(payReference.getSituation().equals(SituationConstants.PAID)
            || payReference.getSituation().equals(SituationConstants.CONCILIATED)){
            logServiceService.saveLog(payReference, "statusCode: 400 -» referenceIsAlreadyPaid", ServiceRequestConstants.REFERENCE_DELETE, ServiceResponseConstants.REFERENCE_ALREADY_PAID);
            throw new BadRequestAlertException(ServiceResponseConstants.REFERENCE_ALREADY_PAID.toString(), ENTITY_NAME, "referenceIsAlreadyPaid");
        }
    }

}
