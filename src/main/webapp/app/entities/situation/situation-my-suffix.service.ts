import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { SituationMySuffix } from './situation-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SituationMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/situations';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(situation: SituationMySuffix): Observable<SituationMySuffix> {
        const copy = this.convert(situation);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(situation: SituationMySuffix): Observable<SituationMySuffix> {
        const copy = this.convert(situation);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<SituationMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to SituationMySuffix.
     */
    private convertItemFromServer(json: any): SituationMySuffix {
        const entity: SituationMySuffix = Object.assign(new SituationMySuffix(), json);
        entity.creationDate = this.dateUtils
            .convertLocalDateFromServer(json.creationDate);
        entity.updateDate = this.dateUtils
            .convertLocalDateFromServer(json.updateDate);
        entity.deleteDate = this.dateUtils
            .convertLocalDateFromServer(json.deleteDate);
        return entity;
    }

    /**
     * Convert a SituationMySuffix to a JSON which can be sent to the server.
     */
    private convert(situation: SituationMySuffix): SituationMySuffix {
        const copy: SituationMySuffix = Object.assign({}, situation);
        copy.creationDate = this.dateUtils
            .convertLocalDateToServer(situation.creationDate);
        copy.updateDate = this.dateUtils
            .convertLocalDateToServer(situation.updateDate);
        copy.deleteDate = this.dateUtils
            .convertLocalDateToServer(situation.deleteDate);
        return copy;
    }
}
