import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { ServiceResponseMySuffix } from './service-response-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ServiceResponseMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/service-responses';

    constructor(private http: Http) { }

    create(serviceResponse: ServiceResponseMySuffix): Observable<ServiceResponseMySuffix> {
        const copy = this.convert(serviceResponse);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(serviceResponse: ServiceResponseMySuffix): Observable<ServiceResponseMySuffix> {
        const copy = this.convert(serviceResponse);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ServiceResponseMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ServiceResponseMySuffix.
     */
    private convertItemFromServer(json: any): ServiceResponseMySuffix {
        const entity: ServiceResponseMySuffix = Object.assign(new ServiceResponseMySuffix(), json);
        return entity;
    }

    /**
     * Convert a ServiceResponseMySuffix to a JSON which can be sent to the server.
     */
    private convert(serviceResponse: ServiceResponseMySuffix): ServiceResponseMySuffix {
        const copy: ServiceResponseMySuffix = Object.assign({}, serviceResponse);
        return copy;
    }
}
