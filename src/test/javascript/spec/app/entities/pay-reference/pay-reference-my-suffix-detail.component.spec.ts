/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PayReferenceMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/pay-reference/pay-reference-my-suffix-detail.component';
import { PayReferenceMySuffixService } from '../../../../../../main/webapp/app/entities/pay-reference/pay-reference-my-suffix.service';
import { PayReferenceMySuffix } from '../../../../../../main/webapp/app/entities/pay-reference/pay-reference-my-suffix.model';

describe('Component Tests', () => {

    describe('PayReferenceMySuffix Management Detail Component', () => {
        let comp: PayReferenceMySuffixDetailComponent;
        let fixture: ComponentFixture<PayReferenceMySuffixDetailComponent>;
        let service: PayReferenceMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [PayReferenceMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PayReferenceMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(PayReferenceMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PayReferenceMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PayReferenceMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PayReferenceMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.payReference).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
