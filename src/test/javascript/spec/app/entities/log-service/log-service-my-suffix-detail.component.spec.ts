/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { LogServiceMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/log-service/log-service-my-suffix-detail.component';
import { LogServiceMySuffixService } from '../../../../../../main/webapp/app/entities/log-service/log-service-my-suffix.service';
import { LogServiceMySuffix } from '../../../../../../main/webapp/app/entities/log-service/log-service-my-suffix.model';

describe('Component Tests', () => {

    describe('LogServiceMySuffix Management Detail Component', () => {
        let comp: LogServiceMySuffixDetailComponent;
        let fixture: ComponentFixture<LogServiceMySuffixDetailComponent>;
        let service: LogServiceMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [LogServiceMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    LogServiceMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(LogServiceMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LogServiceMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LogServiceMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new LogServiceMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.logService).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
