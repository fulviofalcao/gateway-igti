package br.com.igti.service;

import br.com.igti.domain.PayReference;
import br.com.igti.service.vo.CancelPaymentReference;
import br.com.igti.service.vo.CreateReference;
import br.com.igti.service.vo.PaymentReference;
import br.com.igti.service.vo.UpdatePayReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PayReference.
 */
public interface PayReferenceService {

    /**
     * Save a payReference.
     *
     * @param payReference the entity to save
     * @return the persisted entity
     */
    PayReference save(PayReference payReference);

    /**
     *  Get all the payReferences.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PayReference> findAll(Pageable pageable);

    /**
     *  Get the "id" payReference.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PayReference findOne(Long id);

    /**
     *  Delete the "id" payReference.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     *  Get the "referenceNumber" payReference.
     *
     *  @param referenceNumber the id of the entity
     *  @return the entity
     */
    PayReference findOnePayReference(String referenceNumber);
    void paymentReferenceValidations(PayReference payReference, PaymentReference payment);
    void persistConfirmationPayment(PayReference payReference, PaymentReference paymentReference);
    void paymentCancelReferenceValidations(PayReference payReference, CancelPaymentReference cancelPaymentReference);
    void persistCancelPayment(PayReference payReference, CancelPaymentReference cancelPaymentReference);
    void paymentUpdateReferenceValidations(PayReference payReference, UpdatePayReference updatePayReference);
    void persistUpdatePayReference(PayReference payReferenceResponse, UpdatePayReference updatePayReference);
    void createReferenceValidations(PayReference payReference, CreateReference createReference);
    PayReference persistCreateReferencePayment(CreateReference createReference);
    void paymentDeleteReferenceValidations(PayReference payReference,String reference);
    void persistDeletePayReference(PayReference payReference);
    void cancelUnverifiedPayment();

}
