package br.com.igti.service;

import br.com.igti.domain.LogService;
import br.com.igti.domain.ServiceRequest;
import br.com.igti.domain.ServiceResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;

/**
 * Service Interface for managing LogService.
 */
public interface LogServiceService {

    /**
     * Save a logService.
     *
     * @param logService the entity to save
     * @return the persisted entity
     */
    LogService save(LogService logService);

    /**
     *  Get all the logServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<LogService> findAll(Pageable pageable);

    /**
     *  Get the "id" logService.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    LogService findOne(Long id);

    /**
     *  Delete the "id" logService.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    @Async
    void saveLog(Object imput, Object output, ServiceRequest serviceRequest, ServiceResponse serviceResponse);

}
