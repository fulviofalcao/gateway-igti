package br.com.igti.service;

import br.com.igti.domain.PaymentOrigin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PaymentOrigin.
 */
public interface PaymentOriginService {

    /**
     * Save a paymentOrigin.
     *
     * @param paymentOrigin the entity to save
     * @return the persisted entity
     */
    PaymentOrigin save(PaymentOrigin paymentOrigin);

    /**
     *  Get all the paymentOrigins.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PaymentOrigin> findAll(Pageable pageable);

    /**
     *  Get the "id" paymentOrigin.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PaymentOrigin findOne(Long id);

    /**
     *  Delete the "id" paymentOrigin.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
