import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentOriginMySuffix } from './payment-origin-my-suffix.model';
import { PaymentOriginMySuffixPopupService } from './payment-origin-my-suffix-popup.service';
import { PaymentOriginMySuffixService } from './payment-origin-my-suffix.service';

@Component({
    selector: 'jhi-payment-origin-my-suffix-delete-dialog',
    templateUrl: './payment-origin-my-suffix-delete-dialog.component.html'
})
export class PaymentOriginMySuffixDeleteDialogComponent {

    paymentOrigin: PaymentOriginMySuffix;

    constructor(
        private paymentOriginService: PaymentOriginMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.paymentOriginService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'paymentOriginListModification',
                content: 'Deleted an paymentOrigin'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-payment-origin-my-suffix-delete-popup',
    template: ''
})
export class PaymentOriginMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentOriginPopupService: PaymentOriginMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.paymentOriginPopupService
                .open(PaymentOriginMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
