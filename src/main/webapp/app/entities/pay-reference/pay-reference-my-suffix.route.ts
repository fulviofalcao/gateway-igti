import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PayReferenceMySuffixComponent } from './pay-reference-my-suffix.component';
import { PayReferenceMySuffixDetailComponent } from './pay-reference-my-suffix-detail.component';
import { PayReferenceMySuffixPopupComponent } from './pay-reference-my-suffix-dialog.component';
import { PayReferenceMySuffixDeletePopupComponent } from './pay-reference-my-suffix-delete-dialog.component';

@Injectable()
export class PayReferenceMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const payReferenceRoute: Routes = [
    {
        path: 'pay-reference-my-suffix',
        component: PayReferenceMySuffixComponent,
        resolve: {
            'pagingParams': PayReferenceMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.payReference.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'pay-reference-my-suffix/:id',
        component: PayReferenceMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.payReference.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const payReferencePopupRoute: Routes = [
    {
        path: 'pay-reference-my-suffix-new',
        component: PayReferenceMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.payReference.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pay-reference-my-suffix/:id/edit',
        component: PayReferenceMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.payReference.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pay-reference-my-suffix/:id/delete',
        component: PayReferenceMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.payReference.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
