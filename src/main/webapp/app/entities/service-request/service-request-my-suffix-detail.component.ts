import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ServiceRequestMySuffix } from './service-request-my-suffix.model';
import { ServiceRequestMySuffixService } from './service-request-my-suffix.service';

@Component({
    selector: 'jhi-service-request-my-suffix-detail',
    templateUrl: './service-request-my-suffix-detail.component.html'
})
export class ServiceRequestMySuffixDetailComponent implements OnInit, OnDestroy {

    serviceRequest: ServiceRequestMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private serviceRequestService: ServiceRequestMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInServiceRequests();
    }

    load(id) {
        this.serviceRequestService.find(id).subscribe((serviceRequest) => {
            this.serviceRequest = serviceRequest;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInServiceRequests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'serviceRequestListModification',
            (response) => this.load(this.serviceRequest.id)
        );
    }
}
