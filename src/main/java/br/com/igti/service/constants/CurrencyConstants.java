package br.com.igti.service.constants;

import br.com.igti.domain.Currency;
import br.com.igti.repository.CurrencyRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class CurrencyConstants {
    public static Currency KWANZAS;
    public static Currency REAL;
    public static Currency EURO;
    public static Currency DOLAR;

    @Bean
    @DependsOn("liquibase")
    public CurrencyConstants currencyConstantsInitializer(CurrencyRepository currencyRepository) {
        KWANZAS = currencyRepository.findOne(1L);
        REAL = currencyRepository.findOne(2L);
        EURO = currencyRepository.findOne(3L);
        DOLAR = currencyRepository.findOne(4L);
        return new CurrencyConstants();
    }


}
