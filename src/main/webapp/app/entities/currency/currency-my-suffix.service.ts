import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CurrencyMySuffix } from './currency-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CurrencyMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/currencies';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(currency: CurrencyMySuffix): Observable<CurrencyMySuffix> {
        const copy = this.convert(currency);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(currency: CurrencyMySuffix): Observable<CurrencyMySuffix> {
        const copy = this.convert(currency);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<CurrencyMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CurrencyMySuffix.
     */
    private convertItemFromServer(json: any): CurrencyMySuffix {
        const entity: CurrencyMySuffix = Object.assign(new CurrencyMySuffix(), json);
        entity.creationDate = this.dateUtils
            .convertLocalDateFromServer(json.creationDate);
        entity.updateDate = this.dateUtils
            .convertLocalDateFromServer(json.updateDate);
        entity.deleteDate = this.dateUtils
            .convertLocalDateFromServer(json.deleteDate);
        return entity;
    }

    /**
     * Convert a CurrencyMySuffix to a JSON which can be sent to the server.
     */
    private convert(currency: CurrencyMySuffix): CurrencyMySuffix {
        const copy: CurrencyMySuffix = Object.assign({}, currency);
        copy.creationDate = this.dateUtils
            .convertLocalDateToServer(currency.creationDate);
        copy.updateDate = this.dateUtils
            .convertLocalDateToServer(currency.updateDate);
        copy.deleteDate = this.dateUtils
            .convertLocalDateToServer(currency.deleteDate);
        return copy;
    }
}
