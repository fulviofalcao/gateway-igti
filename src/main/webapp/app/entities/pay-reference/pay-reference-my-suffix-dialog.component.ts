import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PayReferenceMySuffix } from './pay-reference-my-suffix.model';
import { PayReferenceMySuffixPopupService } from './pay-reference-my-suffix-popup.service';
import { PayReferenceMySuffixService } from './pay-reference-my-suffix.service';
import { SituationMySuffix, SituationMySuffixService } from '../situation';
import { CurrencyMySuffix, CurrencyMySuffixService } from '../currency';
import { BankMySuffix, BankMySuffixService } from '../bank';
import { PaymentOriginMySuffix, PaymentOriginMySuffixService } from '../payment-origin';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-pay-reference-my-suffix-dialog',
    templateUrl: './pay-reference-my-suffix-dialog.component.html'
})
export class PayReferenceMySuffixDialogComponent implements OnInit {

    payReference: PayReferenceMySuffix;
    isSaving: boolean;

    situations: SituationMySuffix[];

    currencies: CurrencyMySuffix[];

    banks: BankMySuffix[];

    paymentorigins: PaymentOriginMySuffix[];
    paymentLimitDateDp: any;
    paymentDateDp: any;
    conciliationDateDp: any;
    integrationDateDp: any;
    conciliatedSituationDateDp: any;
    creationDateDp: any;
    updateDateDp: any;
    deleteDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private payReferenceService: PayReferenceMySuffixService,
        private situationService: SituationMySuffixService,
        private currencyService: CurrencyMySuffixService,
        private bankService: BankMySuffixService,
        private paymentOriginService: PaymentOriginMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.situationService.query()
            .subscribe((res: ResponseWrapper) => { this.situations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.currencyService.query()
            .subscribe((res: ResponseWrapper) => { this.currencies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.bankService.query()
            .subscribe((res: ResponseWrapper) => { this.banks = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentOriginService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentorigins = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.payReference.id !== undefined) {
            this.subscribeToSaveResponse(
                this.payReferenceService.update(this.payReference));
        } else {
            this.subscribeToSaveResponse(
                this.payReferenceService.create(this.payReference));
        }
    }

    private subscribeToSaveResponse(result: Observable<PayReferenceMySuffix>) {
        result.subscribe((res: PayReferenceMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PayReferenceMySuffix) {
        this.eventManager.broadcast({ name: 'payReferenceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSituationById(index: number, item: SituationMySuffix) {
        return item.id;
    }

    trackCurrencyById(index: number, item: CurrencyMySuffix) {
        return item.id;
    }

    trackBankById(index: number, item: BankMySuffix) {
        return item.id;
    }

    trackPaymentOriginById(index: number, item: PaymentOriginMySuffix) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-pay-reference-my-suffix-popup',
    template: ''
})
export class PayReferenceMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private payReferencePopupService: PayReferenceMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.payReferencePopupService
                    .open(PayReferenceMySuffixDialogComponent as Component, params['id']);
            } else {
                this.payReferencePopupService
                    .open(PayReferenceMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
