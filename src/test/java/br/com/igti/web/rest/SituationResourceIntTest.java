package br.com.igti.web.rest;

import br.com.igti.GatewayApp;

import br.com.igti.domain.Situation;
import br.com.igti.repository.SituationRepository;
import br.com.igti.service.SituationService;
import br.com.igti.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import static br.com.igti.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SituationResource REST controller.
 *
 * @see SituationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class SituationResourceIntTest {

    private static final String DEFAULT_CODIGO = "AAAAA";
    private static final String UPDATED_CODIGO = "BBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDateTime DEFAULT_CREATION_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_CREATION_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_UPDATE_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_UPDATE_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_DELETE_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_DELETE_DATE = LocalDateTime.now(ZoneId.systemDefault());

    @Autowired
    private SituationRepository situationRepository;

    @Autowired
    private SituationService situationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSituationMockMvc;

    private Situation situation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SituationResource situationResource = new SituationResource(situationService);
        this.restSituationMockMvc = MockMvcBuilders.standaloneSetup(situationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Situation createEntity(EntityManager em) {
        Situation situation = new Situation()
            .codigo(DEFAULT_CODIGO)
            .description(DEFAULT_DESCRIPTION)
            .creationDate(DEFAULT_CREATION_DATE)
            .updateDate(DEFAULT_UPDATE_DATE)
            .deleteDate(DEFAULT_DELETE_DATE);
        return situation;
    }

    @Before
    public void initTest() {
        situation = createEntity(em);
    }

    @Test
    @Transactional
    public void createSituation() throws Exception {
        int databaseSizeBeforeCreate = situationRepository.findAll().size();

        // Create the Situation
        restSituationMockMvc.perform(post("/api/situations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(situation)))
            .andExpect(status().isCreated());

        // Validate the Situation in the database
        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeCreate + 1);
        Situation testSituation = situationList.get(situationList.size() - 1);
        assertThat(testSituation.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testSituation.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSituation.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testSituation.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testSituation.getDeleteDate()).isEqualTo(DEFAULT_DELETE_DATE);
    }

    @Test
    @Transactional
    public void createSituationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = situationRepository.findAll().size();

        // Create the Situation with an existing ID
        situation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSituationMockMvc.perform(post("/api/situations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(situation)))
            .andExpect(status().isBadRequest());

        // Validate the Situation in the database
        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodigoIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationRepository.findAll().size();
        // set the field null
        situation.setCodigo(null);

        // Create the Situation, which fails.

        restSituationMockMvc.perform(post("/api/situations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(situation)))
            .andExpect(status().isBadRequest());

        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationRepository.findAll().size();
        // set the field null
        situation.setDescription(null);

        // Create the Situation, which fails.

        restSituationMockMvc.perform(post("/api/situations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(situation)))
            .andExpect(status().isBadRequest());

        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationRepository.findAll().size();
        // set the field null
        situation.setCreationDate(null);

        // Create the Situation, which fails.

        restSituationMockMvc.perform(post("/api/situations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(situation)))
            .andExpect(status().isBadRequest());

        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSituations() throws Exception {
        // Initialize the database
        situationRepository.saveAndFlush(situation);

        // Get all the situationList
        restSituationMockMvc.perform(get("/api/situations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(situation.getId().intValue())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].deleteDate").value(hasItem(DEFAULT_DELETE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getSituation() throws Exception {
        // Initialize the database
        situationRepository.saveAndFlush(situation);

        // Get the situation
        restSituationMockMvc.perform(get("/api/situations/{id}", situation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(situation.getId().intValue()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.deleteDate").value(DEFAULT_DELETE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSituation() throws Exception {
        // Get the situation
        restSituationMockMvc.perform(get("/api/situations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSituation() throws Exception {
        // Initialize the database
        situationService.save(situation);

        int databaseSizeBeforeUpdate = situationRepository.findAll().size();

        // Update the situation
        Situation updatedSituation = situationRepository.findOne(situation.getId());
        updatedSituation
            .codigo(UPDATED_CODIGO)
            .description(UPDATED_DESCRIPTION)
            .creationDate(UPDATED_CREATION_DATE)
            .updateDate(UPDATED_UPDATE_DATE)
            .deleteDate(UPDATED_DELETE_DATE);

        restSituationMockMvc.perform(put("/api/situations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSituation)))
            .andExpect(status().isOk());

        // Validate the Situation in the database
        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeUpdate);
        Situation testSituation = situationList.get(situationList.size() - 1);
        assertThat(testSituation.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testSituation.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSituation.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testSituation.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testSituation.getDeleteDate()).isEqualTo(UPDATED_DELETE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSituation() throws Exception {
        int databaseSizeBeforeUpdate = situationRepository.findAll().size();

        // Create the Situation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSituationMockMvc.perform(put("/api/situations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(situation)))
            .andExpect(status().isCreated());

        // Validate the Situation in the database
        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSituation() throws Exception {
        // Initialize the database
        situationService.save(situation);

        int databaseSizeBeforeDelete = situationRepository.findAll().size();

        // Get the situation
        restSituationMockMvc.perform(delete("/api/situations/{id}", situation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Situation> situationList = situationRepository.findAll();
        assertThat(situationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Situation.class);
        Situation situation1 = new Situation();
        situation1.setId(1L);
        Situation situation2 = new Situation();
        situation2.setId(situation1.getId());
        assertThat(situation1).isEqualTo(situation2);
        situation2.setId(2L);
        assertThat(situation1).isNotEqualTo(situation2);
        situation1.setId(null);
        assertThat(situation1).isNotEqualTo(situation2);
    }
}
