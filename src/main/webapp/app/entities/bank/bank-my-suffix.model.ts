import { BaseEntity } from './../../shared';

export class BankMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public acronymsBank?: string,
        public creationDate?: any,
        public updateDate?: any,
        public deleteDate?: any,
    ) {
    }
}
