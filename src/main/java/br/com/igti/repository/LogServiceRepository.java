package br.com.igti.repository;

import br.com.igti.domain.LogService;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the LogService entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LogServiceRepository extends JpaRepository<LogService, Long> {

}
