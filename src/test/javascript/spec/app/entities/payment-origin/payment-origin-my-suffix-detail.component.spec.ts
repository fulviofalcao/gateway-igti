/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PaymentOriginMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/payment-origin/payment-origin-my-suffix-detail.component';
import { PaymentOriginMySuffixService } from '../../../../../../main/webapp/app/entities/payment-origin/payment-origin-my-suffix.service';
import { PaymentOriginMySuffix } from '../../../../../../main/webapp/app/entities/payment-origin/payment-origin-my-suffix.model';

describe('Component Tests', () => {

    describe('PaymentOriginMySuffix Management Detail Component', () => {
        let comp: PaymentOriginMySuffixDetailComponent;
        let fixture: ComponentFixture<PaymentOriginMySuffixDetailComponent>;
        let service: PaymentOriginMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [PaymentOriginMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PaymentOriginMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(PaymentOriginMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PaymentOriginMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentOriginMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PaymentOriginMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.paymentOrigin).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
