package br.com.igti.service.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtil {
    public static LocalDateTime stringToLocalDateTime(String date, String format) {
        LocalDateTime result = null;

        if (date != null) {
            result = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(format));
        }

        return result;
    }

    public static LocalDateTime stringToLocalDate(String date, String format) {
        LocalDateTime result = null;

        if (date != null) {
            result =  LocalDate.parse(date, DateTimeFormatter.ofPattern(format)).atStartOfDay();
        }

        return result;
    }

    public static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDateTime();
    }

    public static String dateToString(LocalDateTime  date) {
        return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
}
