package br.com.igti.web.rest;

import br.com.igti.GatewayApp;

import br.com.igti.domain.PaymentOrigin;
import br.com.igti.repository.PaymentOriginRepository;
import br.com.igti.service.PaymentOriginService;
import br.com.igti.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import static br.com.igti.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PaymentOriginResource REST controller.
 *
 * @see PaymentOriginResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GatewayApp.class)
public class PaymentOriginResourceIntTest {

    private static final String DEFAULT_CODIGO = "AAAAA";
    private static final String UPDATED_CODIGO = "BBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDateTime DEFAULT_CREATION_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_CREATION_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_UPDATE_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_UPDATE_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final LocalDateTime DEFAULT_DELETE_DATE = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.ofTotalSeconds(0));
    private static final LocalDateTime UPDATED_DELETE_DATE = LocalDateTime.now(ZoneId.systemDefault());

    @Autowired
    private PaymentOriginRepository paymentOriginRepository;

    @Autowired
    private PaymentOriginService paymentOriginService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPaymentOriginMockMvc;

    private PaymentOrigin paymentOrigin;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PaymentOriginResource paymentOriginResource = new PaymentOriginResource(paymentOriginService);
        this.restPaymentOriginMockMvc = MockMvcBuilders.standaloneSetup(paymentOriginResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentOrigin createEntity(EntityManager em) {
        PaymentOrigin paymentOrigin = new PaymentOrigin()
            .codigo(DEFAULT_CODIGO)
            .description(DEFAULT_DESCRIPTION)
            .creationDate(DEFAULT_CREATION_DATE)
            .updateDate(DEFAULT_UPDATE_DATE)
            .deleteDate(DEFAULT_DELETE_DATE);
        return paymentOrigin;
    }

    @Before
    public void initTest() {
        paymentOrigin = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaymentOrigin() throws Exception {
        int databaseSizeBeforeCreate = paymentOriginRepository.findAll().size();

        // Create the PaymentOrigin
        restPaymentOriginMockMvc.perform(post("/api/payment-origins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentOrigin)))
            .andExpect(status().isCreated());

        // Validate the PaymentOrigin in the database
        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentOrigin testPaymentOrigin = paymentOriginList.get(paymentOriginList.size() - 1);
        assertThat(testPaymentOrigin.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testPaymentOrigin.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPaymentOrigin.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testPaymentOrigin.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
        assertThat(testPaymentOrigin.getDeleteDate()).isEqualTo(DEFAULT_DELETE_DATE);
    }

    @Test
    @Transactional
    public void createPaymentOriginWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentOriginRepository.findAll().size();

        // Create the PaymentOrigin with an existing ID
        paymentOrigin.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentOriginMockMvc.perform(post("/api/payment-origins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentOrigin)))
            .andExpect(status().isBadRequest());

        // Validate the PaymentOrigin in the database
        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodigoIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentOriginRepository.findAll().size();
        // set the field null
        paymentOrigin.setCodigo(null);

        // Create the PaymentOrigin, which fails.

        restPaymentOriginMockMvc.perform(post("/api/payment-origins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentOrigin)))
            .andExpect(status().isBadRequest());

        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentOriginRepository.findAll().size();
        // set the field null
        paymentOrigin.setDescription(null);

        // Create the PaymentOrigin, which fails.

        restPaymentOriginMockMvc.perform(post("/api/payment-origins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentOrigin)))
            .andExpect(status().isBadRequest());

        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentOriginRepository.findAll().size();
        // set the field null
        paymentOrigin.setCreationDate(null);

        // Create the PaymentOrigin, which fails.

        restPaymentOriginMockMvc.perform(post("/api/payment-origins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentOrigin)))
            .andExpect(status().isBadRequest());

        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPaymentOrigins() throws Exception {
        // Initialize the database
        paymentOriginRepository.saveAndFlush(paymentOrigin);

        // Get all the paymentOriginList
        restPaymentOriginMockMvc.perform(get("/api/payment-origins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentOrigin.getId().intValue())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].deleteDate").value(hasItem(DEFAULT_DELETE_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPaymentOrigin() throws Exception {
        // Initialize the database
        paymentOriginRepository.saveAndFlush(paymentOrigin);

        // Get the paymentOrigin
        restPaymentOriginMockMvc.perform(get("/api/payment-origins/{id}", paymentOrigin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paymentOrigin.getId().intValue()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.deleteDate").value(DEFAULT_DELETE_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPaymentOrigin() throws Exception {
        // Get the paymentOrigin
        restPaymentOriginMockMvc.perform(get("/api/payment-origins/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaymentOrigin() throws Exception {
        // Initialize the database
        paymentOriginService.save(paymentOrigin);

        int databaseSizeBeforeUpdate = paymentOriginRepository.findAll().size();

        // Update the paymentOrigin
        PaymentOrigin updatedPaymentOrigin = paymentOriginRepository.findOne(paymentOrigin.getId());
        updatedPaymentOrigin
            .codigo(UPDATED_CODIGO)
            .description(UPDATED_DESCRIPTION)
            .creationDate(UPDATED_CREATION_DATE)
            .updateDate(UPDATED_UPDATE_DATE)
            .deleteDate(UPDATED_DELETE_DATE);

        restPaymentOriginMockMvc.perform(put("/api/payment-origins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPaymentOrigin)))
            .andExpect(status().isOk());

        // Validate the PaymentOrigin in the database
        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeUpdate);
        PaymentOrigin testPaymentOrigin = paymentOriginList.get(paymentOriginList.size() - 1);
        assertThat(testPaymentOrigin.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testPaymentOrigin.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPaymentOrigin.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testPaymentOrigin.getUpdateDate()).isEqualTo(UPDATED_UPDATE_DATE);
        assertThat(testPaymentOrigin.getDeleteDate()).isEqualTo(UPDATED_DELETE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPaymentOrigin() throws Exception {
        int databaseSizeBeforeUpdate = paymentOriginRepository.findAll().size();

        // Create the PaymentOrigin

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPaymentOriginMockMvc.perform(put("/api/payment-origins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(paymentOrigin)))
            .andExpect(status().isCreated());

        // Validate the PaymentOrigin in the database
        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePaymentOrigin() throws Exception {
        // Initialize the database
        paymentOriginService.save(paymentOrigin);

        int databaseSizeBeforeDelete = paymentOriginRepository.findAll().size();

        // Get the paymentOrigin
        restPaymentOriginMockMvc.perform(delete("/api/payment-origins/{id}", paymentOrigin.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PaymentOrigin> paymentOriginList = paymentOriginRepository.findAll();
        assertThat(paymentOriginList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentOrigin.class);
        PaymentOrigin paymentOrigin1 = new PaymentOrigin();
        paymentOrigin1.setId(1L);
        PaymentOrigin paymentOrigin2 = new PaymentOrigin();
        paymentOrigin2.setId(paymentOrigin1.getId());
        assertThat(paymentOrigin1).isEqualTo(paymentOrigin2);
        paymentOrigin2.setId(2L);
        assertThat(paymentOrigin1).isNotEqualTo(paymentOrigin2);
        paymentOrigin1.setId(null);
        assertThat(paymentOrigin1).isNotEqualTo(paymentOrigin2);
    }
}
