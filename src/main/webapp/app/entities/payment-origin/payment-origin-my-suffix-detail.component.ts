import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentOriginMySuffix } from './payment-origin-my-suffix.model';
import { PaymentOriginMySuffixService } from './payment-origin-my-suffix.service';

@Component({
    selector: 'jhi-payment-origin-my-suffix-detail',
    templateUrl: './payment-origin-my-suffix-detail.component.html'
})
export class PaymentOriginMySuffixDetailComponent implements OnInit, OnDestroy {

    paymentOrigin: PaymentOriginMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private paymentOriginService: PaymentOriginMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPaymentOrigins();
    }

    load(id) {
        this.paymentOriginService.find(id).subscribe((paymentOrigin) => {
            this.paymentOrigin = paymentOrigin;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPaymentOrigins() {
        this.eventSubscriber = this.eventManager.subscribe(
            'paymentOriginListModification',
            (response) => this.load(this.paymentOrigin.id)
        );
    }
}
