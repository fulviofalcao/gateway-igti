package br.com.igti.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.igti.domain.ServiceRequest;
import br.com.igti.service.ServiceRequestService;
import br.com.igti.web.rest.errors.BadRequestAlertException;
import br.com.igti.web.rest.util.HeaderUtil;
import br.com.igti.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ServiceRequest.
 */
@RestController
@RequestMapping("/api")
public class ServiceRequestResource {

    private final Logger log = LoggerFactory.getLogger(ServiceRequestResource.class);

    private static final String ENTITY_NAME = "serviceRequest";

    private final ServiceRequestService serviceRequestService;

    public ServiceRequestResource(ServiceRequestService serviceRequestService) {
        this.serviceRequestService = serviceRequestService;
    }

    /**
     * POST  /service-requests : Create a new serviceRequest.
     *
     * @param serviceRequest the serviceRequest to create
     * @return the ResponseEntity with status 201 (Created) and with body the new serviceRequest, or with status 400 (Bad Request) if the serviceRequest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/service-requests")
    @Timed
    public ResponseEntity<ServiceRequest> createServiceRequest(@Valid @RequestBody ServiceRequest serviceRequest) throws URISyntaxException {
        log.debug("REST request to save ServiceRequest : {}", serviceRequest);
        if (serviceRequest.getId() != null) {
            throw new BadRequestAlertException("A new serviceRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ServiceRequest result = serviceRequestService.save(serviceRequest);
        return ResponseEntity.created(new URI("/api/service-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /service-requests : Updates an existing serviceRequest.
     *
     * @param serviceRequest the serviceRequest to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated serviceRequest,
     * or with status 400 (Bad Request) if the serviceRequest is not valid,
     * or with status 500 (Internal Server Error) if the serviceRequest couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/service-requests")
    @Timed
    public ResponseEntity<ServiceRequest> updateServiceRequest(@Valid @RequestBody ServiceRequest serviceRequest) throws URISyntaxException {
        log.debug("REST request to update ServiceRequest : {}", serviceRequest);
        if (serviceRequest.getId() == null) {
            return createServiceRequest(serviceRequest);
        }
        ServiceRequest result = serviceRequestService.save(serviceRequest);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceRequest.getId().toString()))
            .body(result);
    }

    /**
     * GET  /service-requests : get all the serviceRequests.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of serviceRequests in body
     */
    @GetMapping("/service-requests")
    @Timed
    public ResponseEntity<List<ServiceRequest>> getAllServiceRequests(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ServiceRequests");
        Page<ServiceRequest> page = serviceRequestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/service-requests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /service-requests/:id : get the "id" serviceRequest.
     *
     * @param id the id of the serviceRequest to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the serviceRequest, or with status 404 (Not Found)
     */
    @GetMapping("/service-requests/{id}")
    @Timed
    public ResponseEntity<ServiceRequest> getServiceRequest(@PathVariable Long id) {
        log.debug("REST request to get ServiceRequest : {}", id);
        ServiceRequest serviceRequest = serviceRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceRequest));
    }

    /**
     * DELETE  /service-requests/:id : delete the "id" serviceRequest.
     *
     * @param id the id of the serviceRequest to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/service-requests/{id}")
    @Timed
    public ResponseEntity<Void> deleteServiceRequest(@PathVariable Long id) {
        log.debug("REST request to delete ServiceRequest : {}", id);
        serviceRequestService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
