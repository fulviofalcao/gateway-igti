import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BankMySuffix } from './bank-my-suffix.model';
import { BankMySuffixService } from './bank-my-suffix.service';

@Injectable()
export class BankMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private bankService: BankMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bankService.find(id).subscribe((bank) => {
                    if (bank.creationDate) {
                        bank.creationDate = {
                            year: bank.creationDate.getFullYear(),
                            month: bank.creationDate.getMonth() + 1,
                            day: bank.creationDate.getDate()
                        };
                    }
                    if (bank.updateDate) {
                        bank.updateDate = {
                            year: bank.updateDate.getFullYear(),
                            month: bank.updateDate.getMonth() + 1,
                            day: bank.updateDate.getDate()
                        };
                    }
                    if (bank.deleteDate) {
                        bank.deleteDate = {
                            year: bank.deleteDate.getFullYear(),
                            month: bank.deleteDate.getMonth() + 1,
                            day: bank.deleteDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.bankModalRef(component, bank);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.bankModalRef(component, new BankMySuffix());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bankModalRef(component: Component, bank: BankMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bank = bank;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
