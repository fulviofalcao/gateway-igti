import { BaseEntity } from './../../shared';

export class TaxMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public taxNumber?: string,
        public taxDescription?: string,
        public amount?: number,
        public payReference?: BaseEntity,
    ) {
    }
}
