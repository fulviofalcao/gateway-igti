import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { ServiceRequestMySuffix } from './service-request-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ServiceRequestMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/service-requests';

    constructor(private http: Http) { }

    create(serviceRequest: ServiceRequestMySuffix): Observable<ServiceRequestMySuffix> {
        const copy = this.convert(serviceRequest);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(serviceRequest: ServiceRequestMySuffix): Observable<ServiceRequestMySuffix> {
        const copy = this.convert(serviceRequest);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ServiceRequestMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ServiceRequestMySuffix.
     */
    private convertItemFromServer(json: any): ServiceRequestMySuffix {
        const entity: ServiceRequestMySuffix = Object.assign(new ServiceRequestMySuffix(), json);
        return entity;
    }

    /**
     * Convert a ServiceRequestMySuffix to a JSON which can be sent to the server.
     */
    private convert(serviceRequest: ServiceRequestMySuffix): ServiceRequestMySuffix {
        const copy: ServiceRequestMySuffix = Object.assign({}, serviceRequest);
        return copy;
    }
}
