import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LogServiceMySuffix } from './log-service-my-suffix.model';
import { LogServiceMySuffixPopupService } from './log-service-my-suffix-popup.service';
import { LogServiceMySuffixService } from './log-service-my-suffix.service';

@Component({
    selector: 'jhi-log-service-my-suffix-delete-dialog',
    templateUrl: './log-service-my-suffix-delete-dialog.component.html'
})
export class LogServiceMySuffixDeleteDialogComponent {

    logService: LogServiceMySuffix;

    constructor(
        private logServiceService: LogServiceMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.logServiceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'logServiceListModification',
                content: 'Deleted an logService'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-log-service-my-suffix-delete-popup',
    template: ''
})
export class LogServiceMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private logServicePopupService: LogServiceMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.logServicePopupService
                .open(LogServiceMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
