import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PaymentOriginMySuffix } from './payment-origin-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentOriginMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/payment-origins';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(paymentOrigin: PaymentOriginMySuffix): Observable<PaymentOriginMySuffix> {
        const copy = this.convert(paymentOrigin);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(paymentOrigin: PaymentOriginMySuffix): Observable<PaymentOriginMySuffix> {
        const copy = this.convert(paymentOrigin);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PaymentOriginMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PaymentOriginMySuffix.
     */
    private convertItemFromServer(json: any): PaymentOriginMySuffix {
        const entity: PaymentOriginMySuffix = Object.assign(new PaymentOriginMySuffix(), json);
        entity.creationDate = this.dateUtils
            .convertLocalDateFromServer(json.creationDate);
        entity.updateDate = this.dateUtils
            .convertLocalDateFromServer(json.updateDate);
        entity.deleteDate = this.dateUtils
            .convertLocalDateFromServer(json.deleteDate);
        return entity;
    }

    /**
     * Convert a PaymentOriginMySuffix to a JSON which can be sent to the server.
     */
    private convert(paymentOrigin: PaymentOriginMySuffix): PaymentOriginMySuffix {
        const copy: PaymentOriginMySuffix = Object.assign({}, paymentOrigin);
        copy.creationDate = this.dateUtils
            .convertLocalDateToServer(paymentOrigin.creationDate);
        copy.updateDate = this.dateUtils
            .convertLocalDateToServer(paymentOrigin.updateDate);
        copy.deleteDate = this.dateUtils
            .convertLocalDateToServer(paymentOrigin.deleteDate);
        return copy;
    }
}
