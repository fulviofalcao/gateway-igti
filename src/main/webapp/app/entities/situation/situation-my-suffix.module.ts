import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    SituationMySuffixService,
    SituationMySuffixPopupService,
    SituationMySuffixComponent,
    SituationMySuffixDetailComponent,
    SituationMySuffixDialogComponent,
    SituationMySuffixPopupComponent,
    SituationMySuffixDeletePopupComponent,
    SituationMySuffixDeleteDialogComponent,
    situationRoute,
    situationPopupRoute,
    SituationMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...situationRoute,
    ...situationPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SituationMySuffixComponent,
        SituationMySuffixDetailComponent,
        SituationMySuffixDialogComponent,
        SituationMySuffixDeleteDialogComponent,
        SituationMySuffixPopupComponent,
        SituationMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        SituationMySuffixComponent,
        SituationMySuffixDialogComponent,
        SituationMySuffixPopupComponent,
        SituationMySuffixDeleteDialogComponent,
        SituationMySuffixDeletePopupComponent,
    ],
    providers: [
        SituationMySuffixService,
        SituationMySuffixPopupService,
        SituationMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewaySituationMySuffixModule {}
