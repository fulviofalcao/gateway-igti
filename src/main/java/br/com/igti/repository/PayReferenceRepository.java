package br.com.igti.repository;

import br.com.igti.domain.PayReference;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the PayReference entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PayReferenceRepository extends JpaRepository<PayReference, Long> {
    PayReference findByReferenceNumber(String referenceNumber);

    @Query(value ="SELECT * FROM PAY_REFERENCE WHERE SITUATION_ID = 2 AND CONCILIATED_SITUATION IS NULL AND PAYMENT_DATE < SYSDATE - 24/24" , nativeQuery = true)
    List<PayReference> findBySituationAndConciliatedSituationAndPaymentDate();
}
