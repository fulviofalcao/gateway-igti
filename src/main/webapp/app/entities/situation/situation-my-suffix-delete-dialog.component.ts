import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SituationMySuffix } from './situation-my-suffix.model';
import { SituationMySuffixPopupService } from './situation-my-suffix-popup.service';
import { SituationMySuffixService } from './situation-my-suffix.service';

@Component({
    selector: 'jhi-situation-my-suffix-delete-dialog',
    templateUrl: './situation-my-suffix-delete-dialog.component.html'
})
export class SituationMySuffixDeleteDialogComponent {

    situation: SituationMySuffix;

    constructor(
        private situationService: SituationMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.situationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'situationListModification',
                content: 'Deleted an situation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-situation-my-suffix-delete-popup',
    template: ''
})
export class SituationMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private situationPopupService: SituationMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.situationPopupService
                .open(SituationMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
