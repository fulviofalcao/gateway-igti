import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SituationMySuffix } from './situation-my-suffix.model';
import { SituationMySuffixService } from './situation-my-suffix.service';

@Injectable()
export class SituationMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private situationService: SituationMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.situationService.find(id).subscribe((situation) => {
                    if (situation.creationDate) {
                        situation.creationDate = {
                            year: situation.creationDate.getFullYear(),
                            month: situation.creationDate.getMonth() + 1,
                            day: situation.creationDate.getDate()
                        };
                    }
                    if (situation.updateDate) {
                        situation.updateDate = {
                            year: situation.updateDate.getFullYear(),
                            month: situation.updateDate.getMonth() + 1,
                            day: situation.updateDate.getDate()
                        };
                    }
                    if (situation.deleteDate) {
                        situation.deleteDate = {
                            year: situation.deleteDate.getFullYear(),
                            month: situation.deleteDate.getMonth() + 1,
                            day: situation.deleteDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.situationModalRef(component, situation);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.situationModalRef(component, new SituationMySuffix());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    situationModalRef(component: Component, situation: SituationMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.situation = situation;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
