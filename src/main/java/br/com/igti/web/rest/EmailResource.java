package br.com.igti.web.rest;

import br.com.igti.service.MailService;
import br.com.igti.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class EmailResource {

    private final Logger log = LoggerFactory.getLogger(EmailResource.class);
    private final MailService mailService;

    private static final String ENTITY_NAME = "PaymentDone";

    public EmailResource(MailService mailService) {
        this.mailService = mailService;
    }

    @GetMapping("/sendEmail/{email:.+}")
    @Timed
    public ResponseEntity<String> getPayReference(@PathVariable String email) throws URISyntaxException {
        log.debug("REST request to Send Email : {}", email);
        String response = mailService.sendPaymentDoneEmail(email);
        return ResponseEntity.created(new URI("/api/sendEmail/" + email))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, email))
            .body(response);
    }
}
