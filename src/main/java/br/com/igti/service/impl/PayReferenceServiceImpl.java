package br.com.igti.service.impl;

import br.com.igti.domain.Currency;
import br.com.igti.repository.PaymentOriginRepository;
import br.com.igti.service.LogServiceService;
import br.com.igti.service.PayReferenceService;
import br.com.igti.domain.PayReference;
import br.com.igti.repository.PayReferenceRepository;
import br.com.igti.service.constants.*;
import br.com.igti.service.util.DateUtil;
import br.com.igti.service.vo.CancelPaymentReference;
import br.com.igti.service.vo.CreateReference;
import br.com.igti.service.vo.PaymentReference;
import br.com.igti.service.vo.UpdatePayReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static br.com.igti.service.validation.PaymentReferenceValidationRules.*;

/**
 * Service Implementation for managing PayReference.
 */
@Service
@Transactional
public class PayReferenceServiceImpl implements PayReferenceService{

    private final Logger log = LoggerFactory.getLogger(PayReferenceServiceImpl.class);

    private final PayReferenceRepository payReferenceRepository;

    private final PaymentOriginRepository paymentOriginRepository;

    private final LogServiceService logServiceService;

    public PayReferenceServiceImpl(PayReferenceRepository payReferenceRepository, PaymentOriginRepository paymentOriginRepository, LogServiceService logServiceService) {
        this.payReferenceRepository = payReferenceRepository;
        this.paymentOriginRepository = paymentOriginRepository;
        this.logServiceService = logServiceService;
    }

    /**
     * Save a payReference.
     *
     * @param payReference the entity to save
     * @return the persisted entity
     */
    @Override
    public PayReference save(PayReference payReference) {
        log.debug("Request to save PayReference : {}", payReference);
        return payReferenceRepository.save(payReference);
    }

    /**
     *  Get all the payReferences.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PayReference> findAll(Pageable pageable) {
        log.debug("Request to get all PayReferences");
        return payReferenceRepository.findAll(pageable);
    }

    /**
     *  Get one payReference by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PayReference findOne(Long id) {
        log.debug("Request to get PayReference : {}", id);
        return payReferenceRepository.findOne(id);
    }

    /**
     *  Delete the  payReference by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PayReference : {}", id);
        payReferenceRepository.delete(id);
    }

    @Override
    public PayReference findOnePayReference(String referenceNumber) {
        log.debug("Request to get ReferenceNumber : {}", referenceNumber);
        return payReferenceRepository.findByReferenceNumber(referenceNumber);
    }

    @Override
    public void paymentReferenceValidations(PayReference payReference, PaymentReference payment){
        setLogServiceService(logServiceService);
        validatePaymentReferenceExistence(Optional.ofNullable(payReference), payment);
        validateAmount(payReference, payment.getAmount());
        validatePaymentOrigin(Optional.ofNullable(paymentOriginRepository.findByCodigo(payment.getPaymentOrigin().toUpperCase())), payment);
        validatePaymentReferenceIsAlreadyPaid(payReference);
        validatePaymentReferenceIsCancelled(payReference);
        validatePaymentReferenceExpiration(payReference);
        validateAuthenticationNumber(payment.getAuthenticationNumber());
    }

    @Override
    public void persistConfirmationPayment(PayReference payReference, PaymentReference paymentReference) {
        payReference
            .situation(SituationConstants.PAID)
            .paymentDate(LocalDateTime.now())
            .entityPaymentStatus(EntityPaymentStatusConstants.FL_PAID_AUTHORIZED)
            .paymentOrigin(paymentOriginRepository.findByCodigo(paymentReference.getPaymentOrigin()))
            .authenticationNumber(paymentReference.getAuthenticationNumber())
            .updateDate(LocalDateTime.now());
        payReferenceRepository.save(payReference);
    }

    @Override
    public void paymentCancelReferenceValidations(PayReference payReference, CancelPaymentReference cancelPaymentReference) {
        setLogServiceService(logServiceService);
        validatePaymentReferenceExistence(Optional.ofNullable(payReference), cancelPaymentReference);
        validateAuthenticationNumber(payReference, cancelPaymentReference);
        validateSituationPayReference(payReference);
    }

    @Override
    public void persistCancelPayment(PayReference payReference, CancelPaymentReference cancelPaymentReference){
        payReference
            .situation(SituationConstants.GENERATED)
            .paymentDate(null)
            .entityPaymentStatus(EntityPaymentStatusConstants.FL_PAID_GERADO)
            .paymentOrigin(null)
            .authenticationNumber(null)
            .updateDate(LocalDateTime.now());
        payReferenceRepository.save(payReference);
    }

    @Override
    public void paymentUpdateReferenceValidations(PayReference payReference, UpdatePayReference updatePayReference) {
        setLogServiceService(logServiceService);
        validatePaymentReferenceExistence(Optional.ofNullable(payReference), updatePayReference);
        validatePaymentReferenceLimitDate(DateUtil.stringToLocalDateTime(updatePayReference.getPaymentLimitDate(), Formats.DEFAULT_DATE_TIME_FORMAT));
        validatePaymentReferenceTaxNumber(updatePayReference.getTaxNumber());
        validatePaymentReferenceTaxDescription(updatePayReference.getTaxDescription());
        validatePaymentReferenceAmount(new BigDecimal(updatePayReference.getAmount()));
    }

    @Override
    public void persistUpdatePayReference(PayReference payReference, UpdatePayReference updatePayReference) {
        payReference
            .paymentLimitDate(DateUtil.stringToLocalDateTime(updatePayReference.getPaymentLimitDate(), Formats.DEFAULT_DATE_TIME_FORMAT))
            .taxNumber(updatePayReference.getTaxNumber())
            .taxDescription(updatePayReference.getTaxDescription())
            .amount(new BigDecimal(updatePayReference.getAmount()))
            .updateDate(LocalDateTime.now());
        payReferenceRepository.save(payReference);
    }

    @Override
    public void createReferenceValidations(PayReference payReference, CreateReference createReference) {
        setLogServiceService(logServiceService);
        validateReferenceExistence(Optional.ofNullable(createReference));
        validateCpf(createReference.getCpf());
        validateTaxpayerName(createReference.getTaxpayerName());
        validatePaymentLimitDate(DateUtil.stringToLocalDateTime(createReference.getPaymentLimitDate(), Formats.DEFAULT_DATE_TIME_FORMAT));
        validateTaxNumber(createReference.getTaxNumber());
        validateTaxDescription(createReference.getTaxDescription());
        validateAmount(new BigDecimal(createReference.getAmount()));
        validateCurrency(createReference.getCurrency());
        validateBank(createReference.getBank());
    }

    @Override
    public PayReference persistCreateReferencePayment(CreateReference createReference) {
        PayReference payReference = new PayReference();
                 payReference
                .referenceNumber(createReference.getReferenceNumber())
                .cpf(createReference.getCpf())
                .taxpayerName(createReference.getTaxpayerName())
                .paymentLimitDate(DateUtil.stringToLocalDateTime(createReference.getPaymentLimitDate(), Formats.DEFAULT_DATE_TIME_FORMAT))
                .taxNumber(createReference.getTaxNumber())
                .taxDescription(createReference.getTaxDescription())
                .amount(new BigDecimal(createReference.getAmount()))
                .currency(CurrencyConstants.KWANZAS)
                .bank(BankConstants.BAI)
                .entityPaymentStatus(EntityPaymentStatusConstants.FL_PAID_GERADO)
                .situation(SituationConstants.GENERATED)
                .creationDate(LocalDateTime.now());
        return payReferenceRepository.save(payReference);
    }

    @Override
    public void paymentDeleteReferenceValidations(PayReference payReference, String reference) {
        setLogServiceService(logServiceService);
        validateDeleteReferenceExistence(Optional.ofNullable(payReference), reference);
        validateDeleteReferenceIsAlreadyPaid(payReference);
    }

    @Override
    public void persistDeletePayReference(PayReference payReference) {
        payReference
            .situation(SituationConstants.DELETED)
            .deleteDate(LocalDateTime.now())
            .updateDate(LocalDateTime.now());
        payReferenceRepository.save(payReference);
    }

    @Override
    public void cancelUnverifiedPayment() {
        for (PayReference payReference : payReferenceRepository.findBySituationAndConciliatedSituationAndPaymentDate()) {
            payReference
                .situation(SituationConstants.GENERATED)
                .authenticationNumber(null)
                .paymentOrigin(null)
                .paymentDate(null)
                .entityPaymentStatus(EntityPaymentStatusConstants.FL_PAID_GERADO)
                .updateDate(LocalDateTime.now());
            payReferenceRepository.save(payReference);
            logServiceService.saveLog(payReference, "statusCode: 201 -» unverifiedPayment",
                        ServiceRequestConstants.JOB_PAYMENT_NOT_CONFIRMATION, ServiceResponseConstants.CANCELLED);
        }
    }
}
