/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SituationMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/situation/situation-my-suffix-detail.component';
import { SituationMySuffixService } from '../../../../../../main/webapp/app/entities/situation/situation-my-suffix.service';
import { SituationMySuffix } from '../../../../../../main/webapp/app/entities/situation/situation-my-suffix.model';

describe('Component Tests', () => {

    describe('SituationMySuffix Management Detail Component', () => {
        let comp: SituationMySuffixDetailComponent;
        let fixture: ComponentFixture<SituationMySuffixDetailComponent>;
        let service: SituationMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [SituationMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SituationMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(SituationMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SituationMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SituationMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SituationMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.situation).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
