import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ServiceResponseMySuffix } from './service-response-my-suffix.model';
import { ServiceResponseMySuffixService } from './service-response-my-suffix.service';

@Component({
    selector: 'jhi-service-response-my-suffix-detail',
    templateUrl: './service-response-my-suffix-detail.component.html'
})
export class ServiceResponseMySuffixDetailComponent implements OnInit, OnDestroy {

    serviceResponse: ServiceResponseMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private serviceResponseService: ServiceResponseMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInServiceResponses();
    }

    load(id) {
        this.serviceResponseService.find(id).subscribe((serviceResponse) => {
            this.serviceResponse = serviceResponse;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInServiceResponses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'serviceResponseListModification',
            (response) => this.load(this.serviceResponse.id)
        );
    }
}
