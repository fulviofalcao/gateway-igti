package br.com.igti.service.vo;

public class CreateReference {

    private String referenceNumber;
    private String cpf;
    private String taxpayerName;
    private String paymentLimitDate;
    private String taxNumber;
    private String taxDescription;
    private String amount;
    private String currency;
    private String bank;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public CreateReference referenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        return this;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getCpf() {
        return cpf;
    }

    public CreateReference cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTaxpayerName() {
        return taxpayerName;
    }

    public CreateReference taxpayerName(String taxpayerName) {
        this.taxpayerName = taxpayerName;
        return this;
    }

    public void setTaxpayerName(String taxpayerName) {
        this.taxpayerName = taxpayerName;
    }

    public String getPaymentLimitDate() {
        return paymentLimitDate;
    }

    public CreateReference paymentLimitDate(String paymentLimitDate) {
        this.paymentLimitDate = paymentLimitDate;
        return this;
    }

    public void setPaymentLimitDate(String paymentLimitDate) {
        this.paymentLimitDate = paymentLimitDate;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public CreateReference taxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
        return this;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getTaxDescription() {
        return taxDescription;
    }

    public CreateReference taxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
        return this;
    }

    public void setTaxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
    }

    public String getAmount() {
        return amount;
    }

    public CreateReference amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public CreateReference currency(String currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBank() {
        return bank;
    }

    public CreateReference bank(String bank) {
        this.bank = bank;
        return this;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }
}
