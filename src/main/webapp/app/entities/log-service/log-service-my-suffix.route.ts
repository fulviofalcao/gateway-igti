import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LogServiceMySuffixComponent } from './log-service-my-suffix.component';
import { LogServiceMySuffixDetailComponent } from './log-service-my-suffix-detail.component';
import { LogServiceMySuffixPopupComponent } from './log-service-my-suffix-dialog.component';
import { LogServiceMySuffixDeletePopupComponent } from './log-service-my-suffix-delete-dialog.component';

@Injectable()
export class LogServiceMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const logServiceRoute: Routes = [
    {
        path: 'log-service-my-suffix',
        component: LogServiceMySuffixComponent,
        resolve: {
            'pagingParams': LogServiceMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.logService.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'log-service-my-suffix/:id',
        component: LogServiceMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.logService.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const logServicePopupRoute: Routes = [
    {
        path: 'log-service-my-suffix-new',
        component: LogServiceMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.logService.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'log-service-my-suffix/:id/edit',
        component: LogServiceMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.logService.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'log-service-my-suffix/:id/delete',
        component: LogServiceMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.logService.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
