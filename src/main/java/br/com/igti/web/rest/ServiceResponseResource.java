package br.com.igti.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.igti.domain.ServiceResponse;
import br.com.igti.service.ServiceResponseService;
import br.com.igti.web.rest.errors.BadRequestAlertException;
import br.com.igti.web.rest.util.HeaderUtil;
import br.com.igti.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ServiceResponse.
 */
@RestController
@RequestMapping("/api")
public class ServiceResponseResource {

    private final Logger log = LoggerFactory.getLogger(ServiceResponseResource.class);

    private static final String ENTITY_NAME = "serviceResponse";

    private final ServiceResponseService serviceResponseService;

    public ServiceResponseResource(ServiceResponseService serviceResponseService) {
        this.serviceResponseService = serviceResponseService;
    }

    /**
     * POST  /service-responses : Create a new serviceResponse.
     *
     * @param serviceResponse the serviceResponse to create
     * @return the ResponseEntity with status 201 (Created) and with body the new serviceResponse, or with status 400 (Bad Request) if the serviceResponse has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/service-responses")
    @Timed
    public ResponseEntity<ServiceResponse> createServiceResponse(@Valid @RequestBody ServiceResponse serviceResponse) throws URISyntaxException {
        log.debug("REST request to save ServiceResponse : {}", serviceResponse);
        if (serviceResponse.getId() != null) {
            throw new BadRequestAlertException("A new serviceResponse cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ServiceResponse result = serviceResponseService.save(serviceResponse);
        return ResponseEntity.created(new URI("/api/service-responses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /service-responses : Updates an existing serviceResponse.
     *
     * @param serviceResponse the serviceResponse to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated serviceResponse,
     * or with status 400 (Bad Request) if the serviceResponse is not valid,
     * or with status 500 (Internal Server Error) if the serviceResponse couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/service-responses")
    @Timed
    public ResponseEntity<ServiceResponse> updateServiceResponse(@Valid @RequestBody ServiceResponse serviceResponse) throws URISyntaxException {
        log.debug("REST request to update ServiceResponse : {}", serviceResponse);
        if (serviceResponse.getId() == null) {
            return createServiceResponse(serviceResponse);
        }
        ServiceResponse result = serviceResponseService.save(serviceResponse);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceResponse.getId().toString()))
            .body(result);
    }

    /**
     * GET  /service-responses : get all the serviceResponses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of serviceResponses in body
     */
    @GetMapping("/service-responses")
    @Timed
    public ResponseEntity<List<ServiceResponse>> getAllServiceResponses(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ServiceResponses");
        Page<ServiceResponse> page = serviceResponseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/service-responses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /service-responses/:id : get the "id" serviceResponse.
     *
     * @param id the id of the serviceResponse to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the serviceResponse, or with status 404 (Not Found)
     */
    @GetMapping("/service-responses/{id}")
    @Timed
    public ResponseEntity<ServiceResponse> getServiceResponse(@PathVariable Long id) {
        log.debug("REST request to get ServiceResponse : {}", id);
        ServiceResponse serviceResponse = serviceResponseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceResponse));
    }

    /**
     * DELETE  /service-responses/:id : delete the "id" serviceResponse.
     *
     * @param id the id of the serviceResponse to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/service-responses/{id}")
    @Timed
    public ResponseEntity<Void> deleteServiceResponse(@PathVariable Long id) {
        log.debug("REST request to delete ServiceResponse : {}", id);
        serviceResponseService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
