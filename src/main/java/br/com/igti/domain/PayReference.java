package br.com.igti.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A PayReference.
 */
@Entity
@Table(name = "pay_reference")
public class PayReference implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 15)
    @Column(name = "reference_number", length = 15, nullable = false)
    private String referenceNumber;

    @NotNull
    @Size(max = 15)
    @Column(name = "cpf", length = 15, nullable = false)
    private String cpf;

    @NotNull
    @Size(max = 100)
    @Column(name = "taxpayer_name", length = 100, nullable = false)
    private String taxpayerName;

    @NotNull
    @Column(name = "payment_limit_date", nullable = false)
    private LocalDateTime paymentLimitDate;

    @NotNull
    @Size(max = 5)
    @Column(name = "tax_number", length = 5, nullable = false)
    private String taxNumber;

    @NotNull
    @Size(max = 100)
    @Column(name = "tax_description", length = 100, nullable = false)
    private String taxDescription;

    @NotNull
    @Column(name = "amount", precision=10, scale=2, nullable = false)
    private BigDecimal amount;

    @NotNull
    @Size(max = 1)
    @Column(name = "entity_payment_status", length = 1, nullable = false)
    private String entityPaymentStatus;

    @Column(name = "payment_date")
    private LocalDateTime paymentDate;

    @Size(max = 60)
    @Column(name = "authentication_number", length = 60)
    private String authenticationNumber;

    @Column(name = "conciliation_date")
    private LocalDateTime conciliationDate;

    @Column(name = "integration_date")
    private LocalDateTime integrationDate;

    @Size(max = 1)
    @Column(name = "conciliated_situation", length = 1)
    private String conciliatedSituation;

    @Column(name = "conciliated_situation_date")
    private LocalDateTime conciliatedSituationDate;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private LocalDateTime creationDate;

    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "delete_date")
    private LocalDateTime deleteDate;

    @ManyToOne
    private Situation situation;

    @ManyToOne
    private Currency currency;

    @ManyToOne
    private Bank bank;

    @ManyToOne
    private PaymentOrigin paymentOrigin;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public PayReference referenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        return this;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getCpf() {
        return cpf;
    }

    public PayReference cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTaxpayerName() {
        return taxpayerName;
    }

    public PayReference taxpayerName(String taxpayerName) {
        this.taxpayerName = taxpayerName;
        return this;
    }

    public void setTaxpayerName(String taxpayerName) {
        this.taxpayerName = taxpayerName;
    }

    public LocalDateTime getPaymentLimitDate() {
        return paymentLimitDate;
    }

    public PayReference paymentLimitDate(LocalDateTime paymentLimitDate) {
        this.paymentLimitDate = paymentLimitDate;
        return this;
    }

    public void setPaymentLimitDate(LocalDateTime paymentLimitDate) {
        this.paymentLimitDate = paymentLimitDate;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public PayReference taxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
        return this;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getTaxDescription() {
        return taxDescription;
    }

    public PayReference taxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
        return this;
    }

    public void setTaxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public PayReference amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getEntityPaymentStatus() {
        return entityPaymentStatus;
    }

    public PayReference entityPaymentStatus(String entityPaymentStatus) {
        this.entityPaymentStatus = entityPaymentStatus;
        return this;
    }

    public void setEntityPaymentStatus(String entityPaymentStatus) {
        this.entityPaymentStatus = entityPaymentStatus;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public PayReference paymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
        return this;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getAuthenticationNumber() {
        return authenticationNumber;
    }

    public PayReference authenticationNumber(String authenticationNumber) {
        this.authenticationNumber = authenticationNumber;
        return this;
    }

    public void setAuthenticationNumber(String authenticationNumber) {
        this.authenticationNumber = authenticationNumber;
    }

    public LocalDateTime getConciliationDate() {
        return conciliationDate;
    }

    public PayReference conciliationDate(LocalDateTime conciliationDate) {
        this.conciliationDate = conciliationDate;
        return this;
    }

    public void setConciliationDate(LocalDateTime conciliationDate) {
        this.conciliationDate = conciliationDate;
    }

    public LocalDateTime getIntegrationDate() {
        return integrationDate;
    }

    public PayReference integrationDate(LocalDateTime integrationDate) {
        this.integrationDate = integrationDate;
        return this;
    }

    public void setIntegrationDate(LocalDateTime integrationDate) {
        this.integrationDate = integrationDate;
    }

    public String getConciliatedSituation() {
        return conciliatedSituation;
    }

    public PayReference conciliatedSituation(String conciliatedSituation) {
        this.conciliatedSituation = conciliatedSituation;
        return this;
    }

    public void setConciliatedSituation(String conciliatedSituation) {
        this.conciliatedSituation = conciliatedSituation;
    }

    public LocalDateTime getConciliatedSituationDate() {
        return conciliatedSituationDate;
    }

    public PayReference conciliatedSituationDate(LocalDateTime conciliatedSituationDate) {
        this.conciliatedSituationDate = conciliatedSituationDate;
        return this;
    }

    public void setConciliatedSituationDate(LocalDateTime conciliatedSituationDate) {
        this.conciliatedSituationDate = conciliatedSituationDate;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public PayReference creationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public PayReference updateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public LocalDateTime getDeleteDate() {
        return deleteDate;
    }

    public PayReference deleteDate(LocalDateTime deleteDate) {
        this.deleteDate = deleteDate;
        return this;
    }

    public void setDeleteDate(LocalDateTime deleteDate) {
        this.deleteDate = deleteDate;
    }

    public Situation getSituation() {
        return situation;
    }

    public PayReference situation(Situation situation) {
        this.situation = situation;
        return this;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }

    public Currency getCurrency() {
        return currency;
    }

    public PayReference currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Bank getBank() {
        return bank;
    }

    public PayReference bank(Bank bank) {
        this.bank = bank;
        return this;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public PaymentOrigin getPaymentOrigin() {
        return paymentOrigin;
    }

    public PayReference paymentOrigin(PaymentOrigin paymentOrigin) {
        this.paymentOrigin = paymentOrigin;
        return this;
    }

    public void setPaymentOrigin(PaymentOrigin paymentOrigin) {
        this.paymentOrigin = paymentOrigin;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PayReference payReference = (PayReference) o;
        if (payReference.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), payReference.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PayReference{" +
            "id=" + getId() +
            ", referenceNumber='" + getReferenceNumber() + "'" +
            ", cpf='" + getCpf() + "'" +
            ", taxpayerName='" + getTaxpayerName() + "'" +
            ", paymentLimitDate='" + getPaymentLimitDate() + "'" +
            ", taxNumber='" + getTaxNumber() + "'" +
            ", taxDescription='" + getTaxDescription() + "'" +
            ", amount='" + getAmount() + "'" +
            ", entityPaymentStatus='" + getEntityPaymentStatus() + "'" +
            ", paymentDate='" + getPaymentDate() + "'" +
            ", authenticationNumber='" + getAuthenticationNumber() + "'" +
            ", conciliationDate='" + getConciliationDate() + "'" +
            ", integrationDate='" + getIntegrationDate() + "'" +
            ", conciliatedSituation='" + getConciliatedSituation() + "'" +
            ", conciliatedSituationDate='" + getConciliatedSituationDate() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", updateDate='" + getUpdateDate() + "'" +
            ", deleteDate='" + getDeleteDate() + "'" +
            "}";
    }
}
