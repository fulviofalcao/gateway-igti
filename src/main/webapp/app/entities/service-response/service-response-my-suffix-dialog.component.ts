import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ServiceResponseMySuffix } from './service-response-my-suffix.model';
import { ServiceResponseMySuffixPopupService } from './service-response-my-suffix-popup.service';
import { ServiceResponseMySuffixService } from './service-response-my-suffix.service';

@Component({
    selector: 'jhi-service-response-my-suffix-dialog',
    templateUrl: './service-response-my-suffix-dialog.component.html'
})
export class ServiceResponseMySuffixDialogComponent implements OnInit {

    serviceResponse: ServiceResponseMySuffix;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private serviceResponseService: ServiceResponseMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.serviceResponse.id !== undefined) {
            this.subscribeToSaveResponse(
                this.serviceResponseService.update(this.serviceResponse));
        } else {
            this.subscribeToSaveResponse(
                this.serviceResponseService.create(this.serviceResponse));
        }
    }

    private subscribeToSaveResponse(result: Observable<ServiceResponseMySuffix>) {
        result.subscribe((res: ServiceResponseMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ServiceResponseMySuffix) {
        this.eventManager.broadcast({ name: 'serviceResponseListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-service-response-my-suffix-popup',
    template: ''
})
export class ServiceResponseMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private serviceResponsePopupService: ServiceResponseMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.serviceResponsePopupService
                    .open(ServiceResponseMySuffixDialogComponent as Component, params['id']);
            } else {
                this.serviceResponsePopupService
                    .open(ServiceResponseMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
