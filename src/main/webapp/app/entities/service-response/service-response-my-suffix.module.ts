import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    ServiceResponseMySuffixService,
    ServiceResponseMySuffixPopupService,
    ServiceResponseMySuffixComponent,
    ServiceResponseMySuffixDetailComponent,
    ServiceResponseMySuffixDialogComponent,
    ServiceResponseMySuffixPopupComponent,
    ServiceResponseMySuffixDeletePopupComponent,
    ServiceResponseMySuffixDeleteDialogComponent,
    serviceResponseRoute,
    serviceResponsePopupRoute,
    ServiceResponseMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...serviceResponseRoute,
    ...serviceResponsePopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ServiceResponseMySuffixComponent,
        ServiceResponseMySuffixDetailComponent,
        ServiceResponseMySuffixDialogComponent,
        ServiceResponseMySuffixDeleteDialogComponent,
        ServiceResponseMySuffixPopupComponent,
        ServiceResponseMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        ServiceResponseMySuffixComponent,
        ServiceResponseMySuffixDialogComponent,
        ServiceResponseMySuffixPopupComponent,
        ServiceResponseMySuffixDeleteDialogComponent,
        ServiceResponseMySuffixDeletePopupComponent,
    ],
    providers: [
        ServiceResponseMySuffixService,
        ServiceResponseMySuffixPopupService,
        ServiceResponseMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayServiceResponseMySuffixModule {}
