import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    PayReferenceMySuffixService,
    PayReferenceMySuffixPopupService,
    PayReferenceMySuffixComponent,
    PayReferenceMySuffixDetailComponent,
    PayReferenceMySuffixDialogComponent,
    PayReferenceMySuffixPopupComponent,
    PayReferenceMySuffixDeletePopupComponent,
    PayReferenceMySuffixDeleteDialogComponent,
    payReferenceRoute,
    payReferencePopupRoute,
    PayReferenceMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...payReferenceRoute,
    ...payReferencePopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PayReferenceMySuffixComponent,
        PayReferenceMySuffixDetailComponent,
        PayReferenceMySuffixDialogComponent,
        PayReferenceMySuffixDeleteDialogComponent,
        PayReferenceMySuffixPopupComponent,
        PayReferenceMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        PayReferenceMySuffixComponent,
        PayReferenceMySuffixDialogComponent,
        PayReferenceMySuffixPopupComponent,
        PayReferenceMySuffixDeleteDialogComponent,
        PayReferenceMySuffixDeletePopupComponent,
    ],
    providers: [
        PayReferenceMySuffixService,
        PayReferenceMySuffixPopupService,
        PayReferenceMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayPayReferenceMySuffixModule {}
