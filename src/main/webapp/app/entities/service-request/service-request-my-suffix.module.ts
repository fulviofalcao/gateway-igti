import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    ServiceRequestMySuffixService,
    ServiceRequestMySuffixPopupService,
    ServiceRequestMySuffixComponent,
    ServiceRequestMySuffixDetailComponent,
    ServiceRequestMySuffixDialogComponent,
    ServiceRequestMySuffixPopupComponent,
    ServiceRequestMySuffixDeletePopupComponent,
    ServiceRequestMySuffixDeleteDialogComponent,
    serviceRequestRoute,
    serviceRequestPopupRoute,
    ServiceRequestMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...serviceRequestRoute,
    ...serviceRequestPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ServiceRequestMySuffixComponent,
        ServiceRequestMySuffixDetailComponent,
        ServiceRequestMySuffixDialogComponent,
        ServiceRequestMySuffixDeleteDialogComponent,
        ServiceRequestMySuffixPopupComponent,
        ServiceRequestMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        ServiceRequestMySuffixComponent,
        ServiceRequestMySuffixDialogComponent,
        ServiceRequestMySuffixPopupComponent,
        ServiceRequestMySuffixDeleteDialogComponent,
        ServiceRequestMySuffixDeletePopupComponent,
    ],
    providers: [
        ServiceRequestMySuffixService,
        ServiceRequestMySuffixPopupService,
        ServiceRequestMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayServiceRequestMySuffixModule {}
