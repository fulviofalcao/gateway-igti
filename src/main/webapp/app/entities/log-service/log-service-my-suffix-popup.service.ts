import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LogServiceMySuffix } from './log-service-my-suffix.model';
import { LogServiceMySuffixService } from './log-service-my-suffix.service';

@Injectable()
export class LogServiceMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private logServiceService: LogServiceMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.logServiceService.find(id).subscribe((logService) => {
                    if (logService.logDate) {
                        logService.logDate = {
                            year: logService.logDate.getFullYear(),
                            month: logService.logDate.getMonth() + 1,
                            day: logService.logDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.logServiceModalRef(component, logService);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.logServiceModalRef(component, new LogServiceMySuffix());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    logServiceModalRef(component: Component, logService: LogServiceMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.logService = logService;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
