import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ServiceRequestMySuffix } from './service-request-my-suffix.model';
import { ServiceRequestMySuffixPopupService } from './service-request-my-suffix-popup.service';
import { ServiceRequestMySuffixService } from './service-request-my-suffix.service';

@Component({
    selector: 'jhi-service-request-my-suffix-dialog',
    templateUrl: './service-request-my-suffix-dialog.component.html'
})
export class ServiceRequestMySuffixDialogComponent implements OnInit {

    serviceRequest: ServiceRequestMySuffix;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private serviceRequestService: ServiceRequestMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.serviceRequest.id !== undefined) {
            this.subscribeToSaveResponse(
                this.serviceRequestService.update(this.serviceRequest));
        } else {
            this.subscribeToSaveResponse(
                this.serviceRequestService.create(this.serviceRequest));
        }
    }

    private subscribeToSaveResponse(result: Observable<ServiceRequestMySuffix>) {
        result.subscribe((res: ServiceRequestMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ServiceRequestMySuffix) {
        this.eventManager.broadcast({ name: 'serviceRequestListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-service-request-my-suffix-popup',
    template: ''
})
export class ServiceRequestMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private serviceRequestPopupService: ServiceRequestMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.serviceRequestPopupService
                    .open(ServiceRequestMySuffixDialogComponent as Component, params['id']);
            } else {
                this.serviceRequestPopupService
                    .open(ServiceRequestMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
