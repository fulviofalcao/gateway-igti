package br.com.igti.service.vo;

public class UpdatePayReference {

    private String referenceNumber;
    private String paymentLimitDate;
    private String taxNumber;
    private String taxDescription;
    private String amount;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public UpdatePayReference referenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        return this;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getPaymentLimitDate() {
        return paymentLimitDate;
    }

    public UpdatePayReference paymentLimitDate(String paymentLimitDate) {
        this.paymentLimitDate = paymentLimitDate;
        return this;
    }

    public void setPaymentLimitDate(String paymentLimitDate) {
        this.paymentLimitDate = paymentLimitDate;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public UpdatePayReference taxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
        return this;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getTaxDescription() {
        return taxDescription;
    }

    public UpdatePayReference taxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
        return this;
    }

    public void setTaxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
    }

    public String getAmount() {
        return amount;
    }

    public UpdatePayReference amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


}
