package br.com.igti.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.igti.domain.PaymentOrigin;
import br.com.igti.service.PaymentOriginService;
import br.com.igti.web.rest.errors.BadRequestAlertException;
import br.com.igti.web.rest.util.HeaderUtil;
import br.com.igti.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PaymentOrigin.
 */
@RestController
@RequestMapping("/api")
public class PaymentOriginResource {

    private final Logger log = LoggerFactory.getLogger(PaymentOriginResource.class);

    private static final String ENTITY_NAME = "paymentOrigin";

    private final PaymentOriginService paymentOriginService;

    public PaymentOriginResource(PaymentOriginService paymentOriginService) {
        this.paymentOriginService = paymentOriginService;
    }

    /**
     * POST  /payment-origins : Create a new paymentOrigin.
     *
     * @param paymentOrigin the paymentOrigin to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paymentOrigin, or with status 400 (Bad Request) if the paymentOrigin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment-origins")
    @Timed
    public ResponseEntity<PaymentOrigin> createPaymentOrigin(@Valid @RequestBody PaymentOrigin paymentOrigin) throws URISyntaxException {
        log.debug("REST request to save PaymentOrigin : {}", paymentOrigin);
        if (paymentOrigin.getId() != null) {
            throw new BadRequestAlertException("A new paymentOrigin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentOrigin result = paymentOriginService.save(paymentOrigin);
        return ResponseEntity.created(new URI("/api/payment-origins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /payment-origins : Updates an existing paymentOrigin.
     *
     * @param paymentOrigin the paymentOrigin to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paymentOrigin,
     * or with status 400 (Bad Request) if the paymentOrigin is not valid,
     * or with status 500 (Internal Server Error) if the paymentOrigin couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payment-origins")
    @Timed
    public ResponseEntity<PaymentOrigin> updatePaymentOrigin(@Valid @RequestBody PaymentOrigin paymentOrigin) throws URISyntaxException {
        log.debug("REST request to update PaymentOrigin : {}", paymentOrigin);
        if (paymentOrigin.getId() == null) {
            return createPaymentOrigin(paymentOrigin);
        }
        PaymentOrigin result = paymentOriginService.save(paymentOrigin);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentOrigin.getId().toString()))
            .body(result);
    }

    /**
     * GET  /payment-origins : get all the paymentOrigins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of paymentOrigins in body
     */
    @GetMapping("/payment-origins")
    @Timed
    public ResponseEntity<List<PaymentOrigin>> getAllPaymentOrigins(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PaymentOrigins");
        Page<PaymentOrigin> page = paymentOriginService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-origins");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /payment-origins/:id : get the "id" paymentOrigin.
     *
     * @param id the id of the paymentOrigin to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paymentOrigin, or with status 404 (Not Found)
     */
    @GetMapping("/payment-origins/{id}")
    @Timed
    public ResponseEntity<PaymentOrigin> getPaymentOrigin(@PathVariable Long id) {
        log.debug("REST request to get PaymentOrigin : {}", id);
        PaymentOrigin paymentOrigin = paymentOriginService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentOrigin));
    }

    /**
     * DELETE  /payment-origins/:id : delete the "id" paymentOrigin.
     *
     * @param id the id of the paymentOrigin to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payment-origins/{id}")
    @Timed
    public ResponseEntity<Void> deletePaymentOrigin(@PathVariable Long id) {
        log.debug("REST request to delete PaymentOrigin : {}", id);
        paymentOriginService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
