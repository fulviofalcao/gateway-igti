package br.com.igti.service.impl;

import br.com.igti.service.PaymentOriginService;
import br.com.igti.domain.PaymentOrigin;
import br.com.igti.repository.PaymentOriginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PaymentOrigin.
 */
@Service
@Transactional
public class PaymentOriginServiceImpl implements PaymentOriginService{

    private final Logger log = LoggerFactory.getLogger(PaymentOriginServiceImpl.class);

    private final PaymentOriginRepository paymentOriginRepository;

    public PaymentOriginServiceImpl(PaymentOriginRepository paymentOriginRepository) {
        this.paymentOriginRepository = paymentOriginRepository;
    }

    /**
     * Save a paymentOrigin.
     *
     * @param paymentOrigin the entity to save
     * @return the persisted entity
     */
    @Override
    public PaymentOrigin save(PaymentOrigin paymentOrigin) {
        log.debug("Request to save PaymentOrigin : {}", paymentOrigin);
        return paymentOriginRepository.save(paymentOrigin);
    }

    /**
     *  Get all the paymentOrigins.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PaymentOrigin> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentOrigins");
        return paymentOriginRepository.findAll(pageable);
    }

    /**
     *  Get one paymentOrigin by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PaymentOrigin findOne(Long id) {
        log.debug("Request to get PaymentOrigin : {}", id);
        return paymentOriginRepository.findOne(id);
    }

    /**
     *  Delete the  paymentOrigin by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PaymentOrigin : {}", id);
        paymentOriginRepository.delete(id);
    }
}
