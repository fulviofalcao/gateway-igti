import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentOriginMySuffix } from './payment-origin-my-suffix.model';
import { PaymentOriginMySuffixPopupService } from './payment-origin-my-suffix-popup.service';
import { PaymentOriginMySuffixService } from './payment-origin-my-suffix.service';

@Component({
    selector: 'jhi-payment-origin-my-suffix-dialog',
    templateUrl: './payment-origin-my-suffix-dialog.component.html'
})
export class PaymentOriginMySuffixDialogComponent implements OnInit {

    paymentOrigin: PaymentOriginMySuffix;
    isSaving: boolean;
    creationDateDp: any;
    updateDateDp: any;
    deleteDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private paymentOriginService: PaymentOriginMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentOrigin.id !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentOriginService.update(this.paymentOrigin));
        } else {
            this.subscribeToSaveResponse(
                this.paymentOriginService.create(this.paymentOrigin));
        }
    }

    private subscribeToSaveResponse(result: Observable<PaymentOriginMySuffix>) {
        result.subscribe((res: PaymentOriginMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PaymentOriginMySuffix) {
        this.eventManager.broadcast({ name: 'paymentOriginListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-payment-origin-my-suffix-popup',
    template: ''
})
export class PaymentOriginMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentOriginPopupService: PaymentOriginMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentOriginPopupService
                    .open(PaymentOriginMySuffixDialogComponent as Component, params['id']);
            } else {
                this.paymentOriginPopupService
                    .open(PaymentOriginMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
