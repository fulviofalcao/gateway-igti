package br.com.igti.repository;

import br.com.igti.domain.PaymentOrigin;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PaymentOrigin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentOriginRepository extends JpaRepository<PaymentOrigin, Long> {
    PaymentOrigin findByCodigo(String code);
}
