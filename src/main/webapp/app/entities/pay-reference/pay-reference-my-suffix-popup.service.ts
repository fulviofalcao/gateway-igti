import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PayReferenceMySuffix } from './pay-reference-my-suffix.model';
import { PayReferenceMySuffixService } from './pay-reference-my-suffix.service';

@Injectable()
export class PayReferenceMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private payReferenceService: PayReferenceMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.payReferenceService.find(id).subscribe((payReference) => {
                    if (payReference.paymentLimitDate) {
                        payReference.paymentLimitDate = {
                            year: payReference.paymentLimitDate.getFullYear(),
                            month: payReference.paymentLimitDate.getMonth() + 1,
                            day: payReference.paymentLimitDate.getDate()
                        };
                    }
                    if (payReference.paymentDate) {
                        payReference.paymentDate = {
                            year: payReference.paymentDate.getFullYear(),
                            month: payReference.paymentDate.getMonth() + 1,
                            day: payReference.paymentDate.getDate()
                        };
                    }
                    if (payReference.conciliationDate) {
                        payReference.conciliationDate = {
                            year: payReference.conciliationDate.getFullYear(),
                            month: payReference.conciliationDate.getMonth() + 1,
                            day: payReference.conciliationDate.getDate()
                        };
                    }
                    if (payReference.integrationDate) {
                        payReference.integrationDate = {
                            year: payReference.integrationDate.getFullYear(),
                            month: payReference.integrationDate.getMonth() + 1,
                            day: payReference.integrationDate.getDate()
                        };
                    }
                    if (payReference.conciliatedSituationDate) {
                        payReference.conciliatedSituationDate = {
                            year: payReference.conciliatedSituationDate.getFullYear(),
                            month: payReference.conciliatedSituationDate.getMonth() + 1,
                            day: payReference.conciliatedSituationDate.getDate()
                        };
                    }
                    if (payReference.creationDate) {
                        payReference.creationDate = {
                            year: payReference.creationDate.getFullYear(),
                            month: payReference.creationDate.getMonth() + 1,
                            day: payReference.creationDate.getDate()
                        };
                    }
                    if (payReference.updateDate) {
                        payReference.updateDate = {
                            year: payReference.updateDate.getFullYear(),
                            month: payReference.updateDate.getMonth() + 1,
                            day: payReference.updateDate.getDate()
                        };
                    }
                    if (payReference.deleteDate) {
                        payReference.deleteDate = {
                            year: payReference.deleteDate.getFullYear(),
                            month: payReference.deleteDate.getMonth() + 1,
                            day: payReference.deleteDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.payReferenceModalRef(component, payReference);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.payReferenceModalRef(component, new PayReferenceMySuffix());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    payReferenceModalRef(component: Component, payReference: PayReferenceMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.payReference = payReference;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
