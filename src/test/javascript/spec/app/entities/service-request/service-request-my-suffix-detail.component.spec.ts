/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ServiceRequestMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/service-request/service-request-my-suffix-detail.component';
import { ServiceRequestMySuffixService } from '../../../../../../main/webapp/app/entities/service-request/service-request-my-suffix.service';
import { ServiceRequestMySuffix } from '../../../../../../main/webapp/app/entities/service-request/service-request-my-suffix.model';

describe('Component Tests', () => {

    describe('ServiceRequestMySuffix Management Detail Component', () => {
        let comp: ServiceRequestMySuffixDetailComponent;
        let fixture: ComponentFixture<ServiceRequestMySuffixDetailComponent>;
        let service: ServiceRequestMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ServiceRequestMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ServiceRequestMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(ServiceRequestMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceRequestMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceRequestMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ServiceRequestMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.serviceRequest).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
