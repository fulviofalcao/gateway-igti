package br.com.igti.service.impl;

import br.com.igti.domain.ServiceRequest;
import br.com.igti.domain.ServiceResponse;
import br.com.igti.service.LogServiceService;
import br.com.igti.domain.LogService;
import br.com.igti.repository.LogServiceRepository;
import br.com.igti.service.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;


/**
 * Service Implementation for managing LogService.
 */
@Service
@Transactional
public class LogServiceServiceImpl implements LogServiceService{

    private final Logger log = LoggerFactory.getLogger(LogServiceServiceImpl.class);

    private final LogServiceRepository logServiceRepository;

    public LogServiceServiceImpl(LogServiceRepository logServiceRepository) {
        this.logServiceRepository = logServiceRepository;
    }

    /**
     * Save a logService.
     *
     * @param logService the entity to save
     * @return the persisted entity
     */
    @Override
    public LogService save(LogService logService) {
        log.debug("Request to save LogService : {}", logService);
        return logServiceRepository.save(logService);
    }

    /**
     *  Get all the logServices.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LogService> findAll(Pageable pageable) {
        log.debug("Request to get all LogServices");
        return logServiceRepository.findAll(pageable);
    }

    /**
     *  Get one logService by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LogService findOne(Long id) {
        log.debug("Request to get LogService : {}", id);
        return logServiceRepository.findOne(id);
    }

    /**
     *  Delete the  logService by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LogService : {}", id);
        logServiceRepository.delete(id);
    }

    @Override
    @Async
    public void saveLog(Object imputObject, Object outputObject, ServiceRequest serviceRequest, ServiceResponse serviceResponse) {
        LogService serviceLog = null;
        try {
            serviceLog = new LogService()
                .imput(StringUtil.objectAttributes(imputObject))
                .output((String)outputObject)
                .logDate(LocalDateTime.now())
                .request(serviceRequest)
                .response(serviceResponse);
        } catch (IllegalAccessException e) {
            log.error("Error converting object: {} to string", imputObject);
        }
        logServiceRepository.save(serviceLog);
    }
}
