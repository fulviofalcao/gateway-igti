import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { BankMySuffix } from './bank-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BankMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/banks';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(bank: BankMySuffix): Observable<BankMySuffix> {
        const copy = this.convert(bank);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(bank: BankMySuffix): Observable<BankMySuffix> {
        const copy = this.convert(bank);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<BankMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to BankMySuffix.
     */
    private convertItemFromServer(json: any): BankMySuffix {
        const entity: BankMySuffix = Object.assign(new BankMySuffix(), json);
        entity.creationDate = this.dateUtils
            .convertLocalDateFromServer(json.creationDate);
        entity.updateDate = this.dateUtils
            .convertLocalDateFromServer(json.updateDate);
        entity.deleteDate = this.dateUtils
            .convertLocalDateFromServer(json.deleteDate);
        return entity;
    }

    /**
     * Convert a BankMySuffix to a JSON which can be sent to the server.
     */
    private convert(bank: BankMySuffix): BankMySuffix {
        const copy: BankMySuffix = Object.assign({}, bank);
        copy.creationDate = this.dateUtils
            .convertLocalDateToServer(bank.creationDate);
        copy.updateDate = this.dateUtils
            .convertLocalDateToServer(bank.updateDate);
        copy.deleteDate = this.dateUtils
            .convertLocalDateToServer(bank.deleteDate);
        return copy;
    }
}
