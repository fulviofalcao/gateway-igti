package br.com.igti.service;

import br.com.igti.domain.Currency;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Currency.
 */
public interface CurrencyService {

    /**
     * Save a currency.
     *
     * @param currency the entity to save
     * @return the persisted entity
     */
    Currency save(Currency currency);

    /**
     *  Get all the currencies.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Currency> findAll(Pageable pageable);

    /**
     *  Get the "id" currency.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Currency findOne(Long id);

    /**
     *  Delete the "id" currency.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
