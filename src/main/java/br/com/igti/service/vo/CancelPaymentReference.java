package br.com.igti.service.vo;

public class CancelPaymentReference {

    private String referenceNumber;
    private String authenticationNumber;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public CancelPaymentReference referenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        return this;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getAuthenticationNumber() {
        return authenticationNumber;
    }

    public CancelPaymentReference authenticationNumber(String authenticationNumber) {
        this.authenticationNumber = authenticationNumber;
        return this;
    }

    public void setAuthenticationNumber(String authenticationNumber) {
        this.authenticationNumber = authenticationNumber;
    }
}
