/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TaxMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/tax/tax-my-suffix-detail.component';
import { TaxMySuffixService } from '../../../../../../main/webapp/app/entities/tax/tax-my-suffix.service';
import { TaxMySuffix } from '../../../../../../main/webapp/app/entities/tax/tax-my-suffix.model';

describe('Component Tests', () => {

    describe('TaxMySuffix Management Detail Component', () => {
        let comp: TaxMySuffixDetailComponent;
        let fixture: ComponentFixture<TaxMySuffixDetailComponent>;
        let service: TaxMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [TaxMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TaxMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(TaxMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaxMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaxMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TaxMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tax).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
