import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TaxMySuffix } from './tax-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TaxMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/taxes';

    constructor(private http: Http) { }

    create(tax: TaxMySuffix): Observable<TaxMySuffix> {
        const copy = this.convert(tax);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(tax: TaxMySuffix): Observable<TaxMySuffix> {
        const copy = this.convert(tax);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TaxMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TaxMySuffix.
     */
    private convertItemFromServer(json: any): TaxMySuffix {
        const entity: TaxMySuffix = Object.assign(new TaxMySuffix(), json);
        return entity;
    }

    /**
     * Convert a TaxMySuffix to a JSON which can be sent to the server.
     */
    private convert(tax: TaxMySuffix): TaxMySuffix {
        const copy: TaxMySuffix = Object.assign({}, tax);
        return copy;
    }
}
