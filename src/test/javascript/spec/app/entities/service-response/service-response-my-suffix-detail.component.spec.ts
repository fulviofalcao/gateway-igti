/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ServiceResponseMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/service-response/service-response-my-suffix-detail.component';
import { ServiceResponseMySuffixService } from '../../../../../../main/webapp/app/entities/service-response/service-response-my-suffix.service';
import { ServiceResponseMySuffix } from '../../../../../../main/webapp/app/entities/service-response/service-response-my-suffix.model';

describe('Component Tests', () => {

    describe('ServiceResponseMySuffix Management Detail Component', () => {
        let comp: ServiceResponseMySuffixDetailComponent;
        let fixture: ComponentFixture<ServiceResponseMySuffixDetailComponent>;
        let service: ServiceResponseMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [ServiceResponseMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ServiceResponseMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(ServiceResponseMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceResponseMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceResponseMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ServiceResponseMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.serviceResponse).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
