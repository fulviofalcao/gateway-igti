import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CurrencyMySuffix } from './currency-my-suffix.model';
import { CurrencyMySuffixPopupService } from './currency-my-suffix-popup.service';
import { CurrencyMySuffixService } from './currency-my-suffix.service';

@Component({
    selector: 'jhi-currency-my-suffix-delete-dialog',
    templateUrl: './currency-my-suffix-delete-dialog.component.html'
})
export class CurrencyMySuffixDeleteDialogComponent {

    currency: CurrencyMySuffix;

    constructor(
        private currencyService: CurrencyMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.currencyService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'currencyListModification',
                content: 'Deleted an currency'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-currency-my-suffix-delete-popup',
    template: ''
})
export class CurrencyMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private currencyPopupService: CurrencyMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.currencyPopupService
                .open(CurrencyMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
