package br.com.igti.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A LogService.
 */
@Entity
@Table(name = "log_service")
public class LogService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 4000)
    @Column(name = "imput", length = 4000, nullable = false)
    private String imput;

    @Size(max = 4000)
    @Column(name = "output", length = 4000)
    private String output;

    @NotNull
    @Column(name = "log_date", nullable = false)
    private LocalDateTime logDate;

    @ManyToOne
    private ServiceRequest request;

    @ManyToOne
    private ServiceResponse response;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImput() {
        return imput;
    }

    public LogService imput(String imput) {
        this.imput = imput;
        return this;
    }

    public void setImput(String imput) {
        this.imput = imput;
    }

    public String getOutput() {
        return output;
    }

    public LogService output(String output) {
        this.output = output;
        return this;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public LocalDateTime getLogDate() {
        return logDate;
    }

    public LogService logDate(LocalDateTime logDate) {
        this.logDate = logDate;
        return this;
    }

    public void setLogDate(LocalDateTime logDate) {
        this.logDate = logDate;
    }

    public ServiceRequest getRequest() {
        return request;
    }

    public LogService request(ServiceRequest serviceRequest) {
        this.request = serviceRequest;
        return this;
    }

    public void setRequest(ServiceRequest serviceRequest) {
        this.request = serviceRequest;
    }

    public ServiceResponse getResponse() {
        return response;
    }

    public LogService response(ServiceResponse serviceResponse) {
        this.response = serviceResponse;
        return this;
    }

    public void setResponse(ServiceResponse serviceResponse) {
        this.response = serviceResponse;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LogService logService = (LogService) o;
        if (logService.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), logService.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LogService{" +
            "id=" + getId() +
            ", imput='" + getImput() + "'" +
            ", output='" + getOutput() + "'" +
            ", logDate='" + getLogDate() + "'" +
            "}";
    }
}
