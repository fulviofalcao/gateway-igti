package br.com.igti.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ServiceResponse.
 */
@Entity
@Table(name = "service_response")
public class ServiceResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "description", length = 50, nullable = false)
    private String description;

    @NotNull
    @Size(max = 5)
    @Column(name = "response_code", length = 10, nullable = false)
    private String responseCode;

    @NotNull
    @Size(max = 5)
    @Column(name = "return_code", length = 10, nullable = false)
    private String returnCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public ServiceResponse description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public ServiceResponse responseCode(String responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public ServiceResponse returnCode(String returnCode) {
        this.returnCode = returnCode;
        return this;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceResponse serviceResponse = (ServiceResponse) o;
        if (serviceResponse.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), serviceResponse.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ServiceResponse{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", responseCode='" + getResponseCode() + "'" +
            ", returnCode='" + getReturnCode() + "'" +
            "}";
    }
}
