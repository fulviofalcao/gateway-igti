import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ServiceRequestMySuffixComponent } from './service-request-my-suffix.component';
import { ServiceRequestMySuffixDetailComponent } from './service-request-my-suffix-detail.component';
import { ServiceRequestMySuffixPopupComponent } from './service-request-my-suffix-dialog.component';
import { ServiceRequestMySuffixDeletePopupComponent } from './service-request-my-suffix-delete-dialog.component';

@Injectable()
export class ServiceRequestMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const serviceRequestRoute: Routes = [
    {
        path: 'service-request-my-suffix',
        component: ServiceRequestMySuffixComponent,
        resolve: {
            'pagingParams': ServiceRequestMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'service-request-my-suffix/:id',
        component: ServiceRequestMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const serviceRequestPopupRoute: Routes = [
    {
        path: 'service-request-my-suffix-new',
        component: ServiceRequestMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-request-my-suffix/:id/edit',
        component: ServiceRequestMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-request-my-suffix/:id/delete',
        component: ServiceRequestMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.serviceRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
