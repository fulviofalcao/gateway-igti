import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TaxMySuffix } from './tax-my-suffix.model';
import { TaxMySuffixPopupService } from './tax-my-suffix-popup.service';
import { TaxMySuffixService } from './tax-my-suffix.service';

@Component({
    selector: 'jhi-tax-my-suffix-delete-dialog',
    templateUrl: './tax-my-suffix-delete-dialog.component.html'
})
export class TaxMySuffixDeleteDialogComponent {

    tax: TaxMySuffix;

    constructor(
        private taxService: TaxMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.taxService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'taxListModification',
                content: 'Deleted an tax'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tax-my-suffix-delete-popup',
    template: ''
})
export class TaxMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxPopupService: TaxMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.taxPopupService
                .open(TaxMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
