import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    CurrencyMySuffixService,
    CurrencyMySuffixPopupService,
    CurrencyMySuffixComponent,
    CurrencyMySuffixDetailComponent,
    CurrencyMySuffixDialogComponent,
    CurrencyMySuffixPopupComponent,
    CurrencyMySuffixDeletePopupComponent,
    CurrencyMySuffixDeleteDialogComponent,
    currencyRoute,
    currencyPopupRoute,
    CurrencyMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...currencyRoute,
    ...currencyPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CurrencyMySuffixComponent,
        CurrencyMySuffixDetailComponent,
        CurrencyMySuffixDialogComponent,
        CurrencyMySuffixDeleteDialogComponent,
        CurrencyMySuffixPopupComponent,
        CurrencyMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        CurrencyMySuffixComponent,
        CurrencyMySuffixDialogComponent,
        CurrencyMySuffixPopupComponent,
        CurrencyMySuffixDeleteDialogComponent,
        CurrencyMySuffixDeletePopupComponent,
    ],
    providers: [
        CurrencyMySuffixService,
        CurrencyMySuffixPopupService,
        CurrencyMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayCurrencyMySuffixModule {}
