package br.com.igti.service.constants;

import br.com.igti.domain.PaymentOrigin;
import br.com.igti.repository.PaymentOriginRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class PaymentOriginConstants {
    public static PaymentOrigin SPTR;
    public static PaymentOrigin SIMER;
    public static PaymentOrigin MCX;
    public static PaymentOrigin STC;
    public static PaymentOrigin SCC;
    public static PaymentOrigin SDD;
    public static PaymentOrigin SIGMA;

    @Bean
    @DependsOn("liquibase")
    public PaymentOriginConstants PaymentOriginConstantsInitializer(PaymentOriginRepository paymentOriginRepository ) {
        SPTR = paymentOriginRepository.findOne(1L);
        SIMER = paymentOriginRepository.findOne(2L);
        MCX = paymentOriginRepository.findOne(3L);
        STC = paymentOriginRepository.findOne(4L);
        SCC = paymentOriginRepository.findOne(5L);
        SDD = paymentOriginRepository.findOne(6L);
        SIGMA = paymentOriginRepository.findOne(7L);
        return new PaymentOriginConstants();
    }
}
