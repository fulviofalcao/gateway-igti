package br.com.igti.service.constants;

import br.com.igti.domain.ServiceResponse;
import br.com.igti.repository.ServiceResponseRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class ServiceResponseConstants {
    public static ServiceResponse VALID_REFERENCE_NUMBER;
    public static ServiceResponse PAID;
    public static ServiceResponse CANCELLED;
    public static ServiceResponse INVALID_ENTITY;
    public static ServiceResponse INVALID_REFERENCE;
    public static ServiceResponse INVALID_AMOUNT;
    public static ServiceResponse REFERENCE_NOT_AVAILABLE_FOR_PAYMENT;
    public static ServiceResponse EXPIRED_REFERENCE;
    public static ServiceResponse REFERENCE_ALREADY_PAID;
    public static ServiceResponse REFERENCE_ALREADY_CANCELLED;
    public static ServiceResponse DIFFERENT_PAYMENT_DATE;
    public static ServiceResponse REFERENCE_NOT_PAID;
    public static ServiceResponse DIFFERENT_TRANSACTION_DATE;
    public static ServiceResponse INVALID_ERROR_TYPE;
    public static ServiceResponse REFERENCE_ALREADY_CONCILIATED;
    public static ServiceResponse INVALID_ORIGEN_PAID;
    public static ServiceResponse INTERNAL_SERVER_ERROR;
    public static ServiceResponse UPDATE_REFERENCE;
    public static ServiceResponse UPDATE_NOT_REFERENCE;
    public static ServiceResponse CREATE_REFERENCE;
    public static ServiceResponse CREATE_NOT_REFERENCE;
    public static ServiceResponse REFERENCE_DELETE;
    public static ServiceResponse REFERENCE_NOT_DELETE;
    public static ServiceResponse JOB_START;
    public static ServiceResponse JOB_END;

    @Bean
    @DependsOn("liquibase")
    public ServiceResponseConstants ServiceResponseConstantsInitializer(ServiceResponseRepository serviceResponseRepository) {
        VALID_REFERENCE_NUMBER = serviceResponseRepository.findOne(1L);
        PAID = serviceResponseRepository.findOne(2L);
        CANCELLED = serviceResponseRepository.findOne(3L);

        INVALID_ENTITY = serviceResponseRepository.findOne(10L);
        INVALID_REFERENCE = serviceResponseRepository.findOne(11L);
        INVALID_AMOUNT = serviceResponseRepository.findOne(12L);
        REFERENCE_NOT_AVAILABLE_FOR_PAYMENT = serviceResponseRepository.findOne(13L);
        EXPIRED_REFERENCE = serviceResponseRepository.findOne(14L);
        REFERENCE_ALREADY_PAID = serviceResponseRepository.findOne(15L);
        REFERENCE_ALREADY_CANCELLED = serviceResponseRepository.findOne(16L);
        DIFFERENT_PAYMENT_DATE = serviceResponseRepository.findOne(17L);
        REFERENCE_NOT_PAID = serviceResponseRepository.findOne(18L);
        DIFFERENT_TRANSACTION_DATE = serviceResponseRepository.findOne(19L);
        INVALID_ERROR_TYPE = serviceResponseRepository.findOne(20L);
        REFERENCE_ALREADY_CONCILIATED = serviceResponseRepository.findOne(21L);
        INVALID_ORIGEN_PAID = serviceResponseRepository.findOne(22L);
        UPDATE_REFERENCE = serviceResponseRepository.findOne(23L);
        UPDATE_NOT_REFERENCE = serviceResponseRepository.findOne(24L);
        CREATE_REFERENCE = serviceResponseRepository.findOne(25L);
        CREATE_NOT_REFERENCE = serviceResponseRepository.findOne(26L);
        REFERENCE_DELETE = serviceResponseRepository.findOne(27L);
        REFERENCE_NOT_DELETE = serviceResponseRepository.findOne(28L);
        JOB_START = serviceResponseRepository.findOne(29L);
        JOB_END = serviceResponseRepository.findOne(230L);
        INTERNAL_SERVER_ERROR = serviceResponseRepository.findOne(500L);
        return new ServiceResponseConstants();
    }
}
