/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { GatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CurrencyMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/currency/currency-my-suffix-detail.component';
import { CurrencyMySuffixService } from '../../../../../../main/webapp/app/entities/currency/currency-my-suffix.service';
import { CurrencyMySuffix } from '../../../../../../main/webapp/app/entities/currency/currency-my-suffix.model';

describe('Component Tests', () => {

    describe('CurrencyMySuffix Management Detail Component', () => {
        let comp: CurrencyMySuffixDetailComponent;
        let fixture: ComponentFixture<CurrencyMySuffixDetailComponent>;
        let service: CurrencyMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [CurrencyMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CurrencyMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(CurrencyMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CurrencyMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CurrencyMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CurrencyMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.currency).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
