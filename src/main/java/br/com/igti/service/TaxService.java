package br.com.igti.service;

import br.com.igti.domain.Tax;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Tax.
 */
public interface TaxService {

    /**
     * Save a tax.
     *
     * @param tax the entity to save
     * @return the persisted entity
     */
    Tax save(Tax tax);

    /**
     *  Get all the taxes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Tax> findAll(Pageable pageable);

    /**
     *  Get the "id" tax.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Tax findOne(Long id);

    /**
     *  Delete the "id" tax.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
