import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PayReferenceMySuffix } from './pay-reference-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PayReferenceMySuffixService {

    private resourceUrl = SERVER_API_URL + 'api/pay-references';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(payReference: PayReferenceMySuffix): Observable<PayReferenceMySuffix> {
        const copy = this.convert(payReference);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(payReference: PayReferenceMySuffix): Observable<PayReferenceMySuffix> {
        const copy = this.convert(payReference);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PayReferenceMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PayReferenceMySuffix.
     */
    private convertItemFromServer(json: any): PayReferenceMySuffix {
        const entity: PayReferenceMySuffix = Object.assign(new PayReferenceMySuffix(), json);
        entity.paymentLimitDate = this.dateUtils
            .convertLocalDateFromServer(json.paymentLimitDate);
        entity.paymentDate = this.dateUtils
            .convertLocalDateFromServer(json.paymentDate);
        entity.conciliationDate = this.dateUtils
            .convertLocalDateFromServer(json.conciliationDate);
        entity.integrationDate = this.dateUtils
            .convertLocalDateFromServer(json.integrationDate);
        entity.conciliatedSituationDate = this.dateUtils
            .convertLocalDateFromServer(json.conciliatedSituationDate);
        entity.creationDate = this.dateUtils
            .convertLocalDateFromServer(json.creationDate);
        entity.updateDate = this.dateUtils
            .convertLocalDateFromServer(json.updateDate);
        entity.deleteDate = this.dateUtils
            .convertLocalDateFromServer(json.deleteDate);
        return entity;
    }

    /**
     * Convert a PayReferenceMySuffix to a JSON which can be sent to the server.
     */
    private convert(payReference: PayReferenceMySuffix): PayReferenceMySuffix {
        const copy: PayReferenceMySuffix = Object.assign({}, payReference);
        copy.paymentLimitDate = this.dateUtils
            .convertLocalDateToServer(payReference.paymentLimitDate);
        copy.paymentDate = this.dateUtils
            .convertLocalDateToServer(payReference.paymentDate);
        copy.conciliationDate = this.dateUtils
            .convertLocalDateToServer(payReference.conciliationDate);
        copy.integrationDate = this.dateUtils
            .convertLocalDateToServer(payReference.integrationDate);
        copy.conciliatedSituationDate = this.dateUtils
            .convertLocalDateToServer(payReference.conciliatedSituationDate);
        copy.creationDate = this.dateUtils
            .convertLocalDateToServer(payReference.creationDate);
        copy.updateDate = this.dateUtils
            .convertLocalDateToServer(payReference.updateDate);
        copy.deleteDate = this.dateUtils
            .convertLocalDateToServer(payReference.deleteDate);
        return copy;
    }
}
