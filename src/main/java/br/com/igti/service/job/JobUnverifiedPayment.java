package br.com.igti.service.job;

import br.com.igti.service.LogServiceService;
import br.com.igti.service.PayReferenceService;
import br.com.igti.service.constants.ServiceRequestConstants;
import br.com.igti.service.constants.ServiceResponseConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;

@Component
@Service
@EnableScheduling
public class JobUnverifiedPayment {
    private final Logger log = LoggerFactory.getLogger(JobUnverifiedPayment.class);

    @Autowired
    private PayReferenceService payReferenceService;
    @Autowired
    private LogServiceService logServiceService;

    /**
     * @Scheduled(cron = "[Seconds] [Minutes] [Hours] [Day of month] [Month] [Day of week]")
     * @Scheduled(cron = "0 0 0/30 * * *")
     * @Scheduled(cron = "0 0/1 * * * ?")
     */
//    @Scheduled(cron = "0 0/1 * * * ?")
    public void UnverifiedPayment() {
        try {
            log.debug("START - Job Unverified Payment Reference : {}", new Date());
            logServiceService.saveLog(ServiceResponseConstants.JOB_START, "statusCode: start -» unverifiedPayment",
                        ServiceRequestConstants.JOB_PAYMENT_NOT_CONFIRMATION, ServiceResponseConstants.JOB_START);

            payReferenceService.cancelUnverifiedPayment();

            logServiceService.saveLog(ServiceResponseConstants.JOB_END, "statusCode: end -» unverifiedPayment",
                ServiceRequestConstants.JOB_PAYMENT_NOT_CONFIRMATION, ServiceResponseConstants.JOB_END);

            log.debug("END - Job Unverified Payment Reference : {}", new Date());
        } catch (Exception e) {
            logServiceService.saveLog(e.getMessage(), "statusCode: end -» unverifiedPayment",
                ServiceRequestConstants.JOB_PAYMENT_NOT_CONFIRMATION, ServiceResponseConstants.JOB_END);
            log.error("Job interrompido devido a um erro inesperado: {}", e.getMessage());
        }
    }

}
