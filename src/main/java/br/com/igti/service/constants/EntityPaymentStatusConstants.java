package br.com.igti.service.constants;

public class EntityPaymentStatusConstants {
    public static final String FL_PAID_GERADO = "G";
    public static final String FL_PAID_AUTHORIZED = "A";
    public static final String FL_PAID_NO_AUTHORIZED = "N";
}
