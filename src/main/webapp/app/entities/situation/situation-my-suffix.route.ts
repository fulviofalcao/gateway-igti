import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SituationMySuffixComponent } from './situation-my-suffix.component';
import { SituationMySuffixDetailComponent } from './situation-my-suffix-detail.component';
import { SituationMySuffixPopupComponent } from './situation-my-suffix-dialog.component';
import { SituationMySuffixDeletePopupComponent } from './situation-my-suffix-delete-dialog.component';

@Injectable()
export class SituationMySuffixResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const situationRoute: Routes = [
    {
        path: 'situation-my-suffix',
        component: SituationMySuffixComponent,
        resolve: {
            'pagingParams': SituationMySuffixResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.situation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'situation-my-suffix/:id',
        component: SituationMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.situation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const situationPopupRoute: Routes = [
    {
        path: 'situation-my-suffix-new',
        component: SituationMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.situation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'situation-my-suffix/:id/edit',
        component: SituationMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.situation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'situation-my-suffix/:id/delete',
        component: SituationMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.situation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
