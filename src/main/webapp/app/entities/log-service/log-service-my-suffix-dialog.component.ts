import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LogServiceMySuffix } from './log-service-my-suffix.model';
import { LogServiceMySuffixPopupService } from './log-service-my-suffix-popup.service';
import { LogServiceMySuffixService } from './log-service-my-suffix.service';
import { ServiceRequestMySuffix, ServiceRequestMySuffixService } from '../service-request';
import { ServiceResponseMySuffix, ServiceResponseMySuffixService } from '../service-response';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-log-service-my-suffix-dialog',
    templateUrl: './log-service-my-suffix-dialog.component.html'
})
export class LogServiceMySuffixDialogComponent implements OnInit {

    logService: LogServiceMySuffix;
    isSaving: boolean;

    servicerequests: ServiceRequestMySuffix[];

    serviceresponses: ServiceResponseMySuffix[];
    logDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private logServiceService: LogServiceMySuffixService,
        private serviceRequestService: ServiceRequestMySuffixService,
        private serviceResponseService: ServiceResponseMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.serviceRequestService.query()
            .subscribe((res: ResponseWrapper) => { this.servicerequests = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.serviceResponseService.query()
            .subscribe((res: ResponseWrapper) => { this.serviceresponses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.logService.id !== undefined) {
            this.subscribeToSaveResponse(
                this.logServiceService.update(this.logService));
        } else {
            this.subscribeToSaveResponse(
                this.logServiceService.create(this.logService));
        }
    }

    private subscribeToSaveResponse(result: Observable<LogServiceMySuffix>) {
        result.subscribe((res: LogServiceMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: LogServiceMySuffix) {
        this.eventManager.broadcast({ name: 'logServiceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackServiceRequestById(index: number, item: ServiceRequestMySuffix) {
        return item.id;
    }

    trackServiceResponseById(index: number, item: ServiceResponseMySuffix) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-log-service-my-suffix-popup',
    template: ''
})
export class LogServiceMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private logServicePopupService: LogServiceMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.logServicePopupService
                    .open(LogServiceMySuffixDialogComponent as Component, params['id']);
            } else {
                this.logServicePopupService
                    .open(LogServiceMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
