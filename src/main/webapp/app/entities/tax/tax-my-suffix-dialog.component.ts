import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TaxMySuffix } from './tax-my-suffix.model';
import { TaxMySuffixPopupService } from './tax-my-suffix-popup.service';
import { TaxMySuffixService } from './tax-my-suffix.service';
import { PayReferenceMySuffix, PayReferenceMySuffixService } from '../pay-reference';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-tax-my-suffix-dialog',
    templateUrl: './tax-my-suffix-dialog.component.html'
})
export class TaxMySuffixDialogComponent implements OnInit {

    tax: TaxMySuffix;
    isSaving: boolean;

    payreferences: PayReferenceMySuffix[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private taxService: TaxMySuffixService,
        private payReferenceService: PayReferenceMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.payReferenceService.query()
            .subscribe((res: ResponseWrapper) => { this.payreferences = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tax.id !== undefined) {
            this.subscribeToSaveResponse(
                this.taxService.update(this.tax));
        } else {
            this.subscribeToSaveResponse(
                this.taxService.create(this.tax));
        }
    }

    private subscribeToSaveResponse(result: Observable<TaxMySuffix>) {
        result.subscribe((res: TaxMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TaxMySuffix) {
        this.eventManager.broadcast({ name: 'taxListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPayReferenceById(index: number, item: PayReferenceMySuffix) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-tax-my-suffix-popup',
    template: ''
})
export class TaxMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taxPopupService: TaxMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.taxPopupService
                    .open(TaxMySuffixDialogComponent as Component, params['id']);
            } else {
                this.taxPopupService
                    .open(TaxMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
