import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    MessageMySuffixService,
    MessageMySuffixPopupService,
    MessageMySuffixComponent,
    MessageMySuffixDetailComponent,
    MessageMySuffixDialogComponent,
    MessageMySuffixPopupComponent,
    MessageMySuffixDeletePopupComponent,
    MessageMySuffixDeleteDialogComponent,
    messageRoute,
    messagePopupRoute,
    MessageMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...messageRoute,
    ...messagePopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MessageMySuffixComponent,
        MessageMySuffixDetailComponent,
        MessageMySuffixDialogComponent,
        MessageMySuffixDeleteDialogComponent,
        MessageMySuffixPopupComponent,
        MessageMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        MessageMySuffixComponent,
        MessageMySuffixDialogComponent,
        MessageMySuffixPopupComponent,
        MessageMySuffixDeleteDialogComponent,
        MessageMySuffixDeletePopupComponent,
    ],
    providers: [
        MessageMySuffixService,
        MessageMySuffixPopupService,
        MessageMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayMessageMySuffixModule {}
