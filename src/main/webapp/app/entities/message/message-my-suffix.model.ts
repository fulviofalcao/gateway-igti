import { BaseEntity } from './../../shared';

export class MessageMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public messageCode?: number,
        public descriptionPt?: string,
        public descriptionEn?: string,
        public creationDate?: any,
        public updateDate?: any,
        public deleteDate?: any,
    ) {
    }
}
