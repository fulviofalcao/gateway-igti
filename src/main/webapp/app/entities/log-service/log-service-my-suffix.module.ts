import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    LogServiceMySuffixService,
    LogServiceMySuffixPopupService,
    LogServiceMySuffixComponent,
    LogServiceMySuffixDetailComponent,
    LogServiceMySuffixDialogComponent,
    LogServiceMySuffixPopupComponent,
    LogServiceMySuffixDeletePopupComponent,
    LogServiceMySuffixDeleteDialogComponent,
    logServiceRoute,
    logServicePopupRoute,
    LogServiceMySuffixResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...logServiceRoute,
    ...logServicePopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LogServiceMySuffixComponent,
        LogServiceMySuffixDetailComponent,
        LogServiceMySuffixDialogComponent,
        LogServiceMySuffixDeleteDialogComponent,
        LogServiceMySuffixPopupComponent,
        LogServiceMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        LogServiceMySuffixComponent,
        LogServiceMySuffixDialogComponent,
        LogServiceMySuffixPopupComponent,
        LogServiceMySuffixDeleteDialogComponent,
        LogServiceMySuffixDeletePopupComponent,
    ],
    providers: [
        LogServiceMySuffixService,
        LogServiceMySuffixPopupService,
        LogServiceMySuffixResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayLogServiceMySuffixModule {}
