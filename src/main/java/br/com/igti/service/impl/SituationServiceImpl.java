package br.com.igti.service.impl;

import br.com.igti.service.SituationService;
import br.com.igti.domain.Situation;
import br.com.igti.repository.SituationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Situation.
 */
@Service
@Transactional
public class SituationServiceImpl implements SituationService{

    private final Logger log = LoggerFactory.getLogger(SituationServiceImpl.class);

    private final SituationRepository situationRepository;

    public SituationServiceImpl(SituationRepository situationRepository) {
        this.situationRepository = situationRepository;
    }

    /**
     * Save a situation.
     *
     * @param situation the entity to save
     * @return the persisted entity
     */
    @Override
    public Situation save(Situation situation) {
        log.debug("Request to save Situation : {}", situation);
        return situationRepository.save(situation);
    }

    /**
     *  Get all the situations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Situation> findAll(Pageable pageable) {
        log.debug("Request to get all Situations");
        return situationRepository.findAll(pageable);
    }

    /**
     *  Get one situation by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Situation findOne(Long id) {
        log.debug("Request to get Situation : {}", id);
        return situationRepository.findOne(id);
    }

    /**
     *  Delete the  situation by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Situation : {}", id);
        situationRepository.delete(id);
    }
}
