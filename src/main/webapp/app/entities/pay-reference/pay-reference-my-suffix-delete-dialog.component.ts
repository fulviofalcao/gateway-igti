import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PayReferenceMySuffix } from './pay-reference-my-suffix.model';
import { PayReferenceMySuffixPopupService } from './pay-reference-my-suffix-popup.service';
import { PayReferenceMySuffixService } from './pay-reference-my-suffix.service';

@Component({
    selector: 'jhi-pay-reference-my-suffix-delete-dialog',
    templateUrl: './pay-reference-my-suffix-delete-dialog.component.html'
})
export class PayReferenceMySuffixDeleteDialogComponent {

    payReference: PayReferenceMySuffix;

    constructor(
        private payReferenceService: PayReferenceMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.payReferenceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'payReferenceListModification',
                content: 'Deleted an payReference'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pay-reference-my-suffix-delete-popup',
    template: ''
})
export class PayReferenceMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private payReferencePopupService: PayReferenceMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.payReferencePopupService
                .open(PayReferenceMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
